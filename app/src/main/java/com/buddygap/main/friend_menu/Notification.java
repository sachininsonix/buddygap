package com.buddygap.main.friend_menu;

import android.Manifest;
import android.app.DownloadManager;
import android.content.Context;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.buddygap.main.R;
import com.buddygap.main.utils.BaseActivity;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import java.io.File;

/**
 * Created by insonix on 21/1/16.
 */
public class Notification extends BaseActivity {
    ImageView imgView;
     TextView messge,imgtitle;
    ImageLoader imageLoader;
    DisplayImageOptions options;
    ImageView back_btn;
    LinearLayout back_layout;
    String Imgurl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.notification);
        initUI();

        imageLoader= ImageLoader.getInstance();
        imageLoader.init(ImageLoaderConfiguration.createDefault(Notification.this));
        options=new DisplayImageOptions.Builder().showStubImage(R.drawable.images).showImageForEmptyUri(R.drawable.noimage).cacheOnDisc().cacheInMemory().build();

        Bundle extras = getIntent().getExtras();
        try {

        }catch (Exception e){
            e.printStackTrace();
        }
        String title = extras.getString("title");
        String Message = extras.getString("msg");
        Imgurl = extras.getString("icon");

        if(Imgurl.equals("")){
            imgView.setVisibility(View.GONE);
        }
        if (weHavePermissionToReadContacts()) {
            downloadFile(Imgurl);

        } else {
            requestReadContactsPermissionFirst();
        }

        imageLoader.displayImage(Imgurl, imgView, options);

        imgtitle.setText(title);
        messge.setText(Message);
        back_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent intent1=new Intent(Best_friendz.this,Bestfrdz_list.class);
//                startActivity(intent1);
               onBackPressed();

            }
        });

    }
    @Override
    public void initUI() {
        imgView =(ImageView)findViewById(R.id.notifyimage);
        messge=(TextView)findViewById(R.id.message_content);
        imgtitle=(TextView)findViewById(R.id.title_content);
        back_btn=(ImageView)findViewById(R.id.back_btn);

        back_layout=(LinearLayout)findViewById(R.id.back_layout);

    }
    @Override
    public void onBackPressed() {
//        super.onBackPressed();
//        Intent intent=new Intent(Notification.this,E_cards.class);
//        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TOP);
//        startActivity(intent);
        finish();
    }
    public void downloadFile(String uRl) {
        File direct = new File(Environment.getExternalStorageDirectory()
                + "/BuddyGap Images");

        if (!direct.exists()) {
            direct.mkdirs();
        }

        DownloadManager mgr = (DownloadManager)getApplicationContext().getSystemService(Context.DOWNLOAD_SERVICE);

        Uri downloadUri = Uri.parse(uRl);
        DownloadManager.Request request = new DownloadManager.Request(
                downloadUri);

        request.setAllowedNetworkTypes(
                DownloadManager.Request.NETWORK_WIFI
                        | DownloadManager.Request.NETWORK_MOBILE)
                .setAllowedOverRoaming(false).setTitle("BuddyGap")
                .setDescription("Downloading Images to gallery")
                .setDestinationInExternalPublicDir("/BuddyGap Images", "fileName.jpg");

        mgr.enqueue(request);

    }
    private boolean weHavePermissionToReadContacts() {
        return ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED;
    }



    private void requestReadContactsPermissionFirst() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                ) {
            Toast.makeText(this, "We need permission so you can access your Storage.", Toast.LENGTH_LONG).show();
            requestForResultContactsPermission();
        } else {
            requestForResultContactsPermission();
        }
    }

    private void requestForResultContactsPermission() {
        ActivityCompat.requestPermissions(this, new String[]{ Manifest.permission.WRITE_EXTERNAL_STORAGE}, 123);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 123
                && grantResults.length > 0
                && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

            downloadFile(Imgurl);
        } else {
            Toast.makeText(this, "Permission Denied", Toast.LENGTH_SHORT).show();
        }
    }
       }


