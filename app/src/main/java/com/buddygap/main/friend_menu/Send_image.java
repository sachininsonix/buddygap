package com.buddygap.main.friend_menu;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.buddygap.main.R;
import com.buddygap.main.iconstant.IConstant;
import com.buddygap.main.utils.AppController;
import com.buddygap.main.utils.BaseActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class Send_image extends BaseActivity implements IConstant {
    ImageView gallery_image;
    TextView cancel,send,textName;
    byte b[];
    Bitmap bitmap;
    String url_profile,phone,file,name;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_send_image);
        initUI();
        Intent intent=getIntent();
        try{

        b=intent.getByteArrayExtra("ecard_gallery");
        phone=intent.getStringExtra("number");
        name=intent.getStringExtra("name");
            if (!name.equals("No Name")) {
                textName.setText("You wants to send E-card to "+ name);
            }else{
                textName.setText("You wants to send E-card to "+ phone);
            }

        bitmap= BitmapFactory.decodeByteArray(b, 0, b.length);
        gallery_image.setImageBitmap(bitmap);}
        catch (Exception e){

        }
        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                json_object();
            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finish();
            }
        });

    }

    @Override
    public void initUI() {
        gallery_image=(ImageView)findViewById(R.id.gallery_image);
        cancel=(TextView)findViewById(R.id.cancel);
        send=(TextView)findViewById(R.id.send);
        textName=(TextView)findViewById(R.id.textName);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_send_image, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    void json_object() {

        url_profile = Base_url + "send_noti.php?";

        progress_show();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url_profile, new Response.Listener<String>() {
            @Override
            public void onResponse(String s) {

                try {
                    JSONObject jsonObject = new JSONObject(s);
                    String status = jsonObject.getString("status");
                    String message = jsonObject.getString("message");
                    if (status.equals("true")) {
                        Toast.makeText(Send_image.this,"Ecard sent", Toast.LENGTH_SHORT).show();
//                        JSONObject jsonObject1 = jsonObject.getJSONObject("response");


                    }
                    else {
                        Toast.makeText(Send_image.this, message, Toast.LENGTH_SHORT).show();
                    }
                    Log.d("response", jsonObject.toString());
                    finish();
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                progress_dismiss();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                progress_dismiss();
                volleyError.printStackTrace();
                Log.d("Error","error1"+volleyError);

            }


        }) {
            @Override
            protected Map<String, String> getParams(){
                Map<String, String> params = new HashMap<String, String>();
                params.put("tag", "SHOW_FILE");
                if(pref_getvalue_str("user_id", "") != null) {
                    params.put("user_id", pref_getvalue_str("user_id", ""));
                }
                if(phone != null) {
                    Log.d("number",phone);
                    params.put("phone",phone);
                }
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                if (bitmap == null) {

                } else {
                    bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
                    byte data[] = byteArrayOutputStream.toByteArray();
                    file = Base64.encodeToString(data, Base64.DEFAULT);
                }
                if (file == null) {

                } else {
                    params.put("profile_name", "" + new Date().getTime() + ".png");
                    params.put("profile_file", file);
                }
                return params;
            };
//            @Override
//            protected Map<String, String> getParams() throws AuthFailureError {
//
//                Map<String, String> params = new HashMap<>();
//
//                params.put("user_id",pref_getvalue_str("user_id", "").toString().trim());
//                params.put("phone", number);
//                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
//                if (bitmap == null) {
//
//                } else {
//                    bitmap.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream);
//                    byte data[] = byteArrayOutputStream.toByteArray();
//                    file = Base64.encodeToString(data, Base64.DEFAULT);
//
//                }
//
//                if (file == null) {
//
//                } else {
//                    params.put("profile_name", "" + new Date().getTime() + ".png");
//                    params.put("profile_file", file);
//                }
//
//                return params;
//
//            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(stringRequest);
    }
}
