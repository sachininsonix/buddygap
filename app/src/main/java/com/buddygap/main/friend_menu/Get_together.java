package com.buddygap.main.friend_menu;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.buddygap.main.home.Incoming_outgoing_calls;
import com.buddygap.main.iconstant.IConstant;
import com.buddygap.main.lastmonth_list.Lastmonth_menu_friendz;
import com.buddygap.main.utils.AppController;
import com.buddygap.main.utils.GPSTracker;
import com.buddygap.main.utils.PlaceArrayAdapter;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.buddygap.main.R;

public class Get_together extends FragmentActivity implements IConstant,GoogleApiClient.OnConnectionFailedListener,
        GoogleApiClient.ConnectionCallbacks  {
LinearLayout back_layout,gettogather_appbar,lay_search;
    ProgressDialog progressDialog;
    AutoCompleteTextView autocompletebox;
    PlaceArrayAdapter placeArrayAdapter;
    private static final LatLngBounds BOUNDS_MOUNTAIN_VIEW = new LatLngBounds(
            new LatLng(28.70, -127.50), new LatLng(48.85, -55.19));
    List<Address> address;
    Double lat,lon;
    String url,friend;
    private static final int GOOGLE_API_CLIENT_ID = 0;
    GoogleApiClient mGoogleApiClient;
    ImageView search;
    ListView listview;
    GPSTracker gpsTracker;
    ArrayList<HashMap<String,String>> arrayList;
    static String contactno,name,invite,this_monthcheck;
    ConnectivityManager connMgr;
    NetworkInfo   networkInfo;
    TextView friends_text;
    AlertDialog.Builder alertdialog;
     SharedPreferences sharedPreferences;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_get_together);
        sharedPreferences=getSharedPreferences("first_name", Context.MODE_PRIVATE);
        gpsTracker=new GPSTracker(Get_together.this);
        address=new ArrayList();
        connMgr = (ConnectivityManager)
                getSystemService(Context.CONNECTIVITY_SERVICE);
        networkInfo = connMgr.getActiveNetworkInfo();
        alertdialog=new AlertDialog.Builder(this);

        // initialize view's id
        listview=(ListView)findViewById(R.id.listview);
        friends_text=(TextView)findViewById(R.id.friends);
        gettogather_appbar=(LinearLayout)findViewById(R.id.gettogather_appbar);
        lay_search=(LinearLayout)findViewById(R.id.lay_search);
        search=(ImageView)findViewById(R.id.search);
        back_layout=(LinearLayout)findViewById(R.id.back_layout);
        autocompletebox=(AutoCompleteTextView)findViewById(R.id.autocompletebox);

        progressDialog=new ProgressDialog(Get_together.this);

        try{
        contactno=getIntent().getStringExtra("number");
        friend=getIntent().getStringExtra("friends");
        invite=getIntent().getStringExtra("invite");
        name=getIntent().getStringExtra("name");
        this_monthcheck=getIntent().getStringExtra("this_monthcheck");
            if (sharedPreferences.getString("gettogather", "").toString().equals("")) {
                friends_text.setText("Let's Meet");}
            else {
                friends_text.setText(sharedPreferences.getString("gettogather",""));
            }

        }
        catch (Exception e){

        }

        Window window = getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);


        if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.KITKAT) {

            if (friend.equals(Incoming_outgoing_calls.best_frdz)){
                gettogather_appbar.setBackgroundResource(R.color.best_background_color);
              //  window.setStatusBarColor(getResources().getColor(R.color.best_background_color));
            }
            else if(friend.equals(Incoming_outgoing_calls.igr_frdz)){
                gettogather_appbar.setBackgroundResource(R.color.ignore_background_color);
             //   window.setStatusBarColor(getResources().getColor(R.color.ignore_background_color));

            }
            else if(friend.equals(Incoming_outgoing_calls.annoy_frdz)){
                gettogather_appbar.setBackgroundResource(R.color.annoy_background_color);
              //  window.setStatusBarColor(getResources().getColor(R.color.annoy_background_color));
            }
            else {
                gettogather_appbar.setBackgroundResource(R.color.lousy_background_color);
             //   window.setStatusBarColor(getResources().getColor(R.color.lousy_background_color));
            }
        }
        else{

            if (friend.equals(Incoming_outgoing_calls.best_frdz)){
                gettogather_appbar.setBackgroundResource(R.color.best_background_color);
                window.setStatusBarColor(getResources().getColor(R.color.best_background_color));
            }
            else if(friend.equals(Incoming_outgoing_calls.igr_frdz)){
                gettogather_appbar.setBackgroundResource(R.color.ignore_background_color);
                window.setStatusBarColor(getResources().getColor(R.color.ignore_background_color));

            }
            else if(friend.equals(Incoming_outgoing_calls.annoy_frdz)){
                gettogather_appbar.setBackgroundResource(R.color.annoy_background_color);
                window.setStatusBarColor(getResources().getColor(R.color.annoy_background_color));
            }
            else {
                gettogather_appbar.setBackgroundResource(R.color.lousy_background_color);
                window.setStatusBarColor(getResources().getColor(R.color.lousy_background_color));
            }
        }



        if (gpsTracker.canGetLocation()) {
                    if (gpsTracker.canGetLocation) {
                lat = gpsTracker.getLatitude();
               lon = gpsTracker.getLongitude();
                    }
        }
        else {
            gpsTracker.showSettingsAlert();
        }

              url=Base_url+"file.php?lat="+lat+"&lon="+lon;
        try{
        json_object();}
        catch (Exception e){

        }

        arrayList=new ArrayList<>();

        placeArrayAdapter = new PlaceArrayAdapter(this, android.R.layout.simple_list_item_1,
                BOUNDS_MOUNTAIN_VIEW, null);
        mGoogleApiClient = new GoogleApiClient.Builder(Get_together.this)
                .addApi(Places.GEO_DATA_API)
                .enableAutoManage(this, GOOGLE_API_CLIENT_ID, this)
                .addConnectionCallbacks(this)
                .build();



        if (networkInfo != null && networkInfo.isConnected()){
        autocompletebox.setAdapter(placeArrayAdapter);
            autocompletebox.setThreshold(1);}
        else{
            alertdialog.setMessage("Not able to connect to BuddyGap. Please check your network connection and try again.")
                    .setTitle("Error")
                    .setCancelable(false)
                    .setNegativeButton("Ok", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            //  Action for 'NO' Button
                            dialog.cancel();
                        }
                    });
            AlertDialog alert = alertdialog.create();
            alert.show();
        }

        autocompletebox.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Geocoder geocoder = new Geocoder(Get_together.this);
                String areaname = String.valueOf(parent.getItemAtPosition(position));
                try {
                    address = geocoder.getFromLocationName(areaname, 1);
                    if (address.size() > 0) {
                        lat = address.get(0).getLatitude();
                        lon = address.get(0).getLongitude();
                        Log.d("lat", String.valueOf(lat));
                        Log.d("lon", String.valueOf(lon));
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });

        lay_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                autocompletebox_validation();
                InputMethodManager inputManager = (InputMethodManager)
                        getSystemService(Context.INPUT_METHOD_SERVICE);

                inputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(),
                        InputMethodManager.HIDE_NOT_ALWAYS);

            }
        });
search.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {

        autocompletebox_validation();
        InputMethodManager inputManager = (InputMethodManager)
                getSystemService(Context.INPUT_METHOD_SERVICE);
try{
        inputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(),
                InputMethodManager.HIDE_NOT_ALWAYS);}
catch (Exception e){

}

    }
});
        back_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent intent=new Intent(Get_together.this,Best_friendz.class);
//                startActivity(intent);
//                Get_together.this.finish();
                if (this_monthcheck.equals("0")){
                    Intent intent=new Intent(Get_together.this,Best_friendz.class);
                    intent.putExtra("friends",friend);
                    intent.putExtra("number",contactno);
                    intent.putExtra("invite",invite);
                    intent.putExtra("name",name);
                    startActivity(intent);
                    finish();}
                else{
                    Intent intent=new Intent(Get_together.this,Lastmonth_menu_friendz.class);
                    intent.putExtra("friends",friend);
                    intent.putExtra("number",contactno);
                    intent.putExtra("invite",invite);
                    intent.putExtra("name",name);
                    startActivity(intent);
                    finish();
                }

            }
        });
    }
void autocompletebox_validation(){

    if(autocompletebox.getText().toString().equals("")){
        autocompletebox.setError("Please Enter Location Name");
    }
    else {
        arrayList = new ArrayList<>();
        url = Base_url+"file.php?lat=" + lat + "&lon=" + lon;
        //  url="http://insonix.com/design.insonix.com/bgap/file.php?lat="+lat+"&lon="+lon;
        Log.d("url", url);
      try{
        json_object();}
      catch (Exception e){

      }
    }


}
void json_object(){
    progressDialog.setMessage("Loading...");
    progressDialog.setCancelable(false);
    progressDialog.show();
    JsonObjectRequest jsonObjectRequest=new JsonObjectRequest(Request.Method.GET,url, null, new Response.Listener<JSONObject>() {
        @Override
        public void onResponse(JSONObject jsonObject) {
            try {
                String status=jsonObject.getString("status");
                if (status.equals("ok")){
                    JSONArray jsonArray=jsonObject.getJSONArray("response");
                    for (int i=0;i<jsonArray.length();i++){
                        HashMap<String,String> hashMap=new HashMap<>();
                    JSONObject jsonObject1=jsonArray.getJSONObject(i);
                        String name=jsonObject1.getString("name");
                        hashMap.put("name",name);
                        String address=jsonObject1.getString("address");
                        hashMap.put("address",address);
                        arrayList.add(hashMap);
                    }
                    Resturant adapter=new Resturant(Get_together.this,arrayList);
                    listview.setAdapter(adapter);
                }
                progressDialog.dismiss();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }, new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError volleyError) {
            progressDialog.dismiss();
        }
    });
    AppController.getInstance().addToRequestQueue(jsonObjectRequest);
}


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    @Override
    public void onBackPressed() {
   //     super.onBackPressed();
//        Intent intent=new Intent(Get_together.this,Best_friendz.class);
//        startActivity(intent);
//        Get_together.this.finish();
        if (this_monthcheck.equals("0")){
            Intent intent=new Intent(Get_together.this,Best_friendz.class);
            intent.putExtra("friends",friend);
            intent.putExtra("number",contactno);
            intent.putExtra("invite",invite);
            intent.putExtra("name",name);
            startActivity(intent);
            finish();}
        else{
            Intent intent=new Intent(Get_together.this,Lastmonth_menu_friendz.class);
            intent.putExtra("friends",friend);
            intent.putExtra("number",contactno);
            intent.putExtra("invite",invite);
            intent.putExtra("name",name);
            startActivity(intent);
            finish();
        }
    }
    @Override
    public void onConnected(Bundle bundle) {
        placeArrayAdapter.setGoogleApiClient(mGoogleApiClient);
        Log.d("google_api","connected");
    }
    @Override
    public void onConnectionSuspended(int i) {

    }
    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }
}

class Resturant extends BaseAdapter{
Activity  activity;
    ArrayList<HashMap<String,String>> arraylist;
    public Resturant(Activity activity, ArrayList<HashMap<String, String>> arrayList) {
        this.activity=activity;
        arraylist=arrayList;
    }

    @Override
    public int getCount() {
        return arraylist.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater=LayoutInflater.from(activity);
        convertView=inflater.inflate(R.layout.single_resturant,null);
        TextView name=(TextView)convertView.findViewById(R.id.name);
        TextView address=(TextView)convertView.findViewById(R.id.address);
        name.setText(arraylist.get(position).get("name"));
        address.setText(arraylist.get(position).get("address"));

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                     if (isNetworkAvailable() == false) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(activity);
                    builder.setMessage("Not able to connect to BuddyGap. Please check your network connection and try again.")
                            .setTitle("Error")
                            .setCancelable(false)
                            .setNegativeButton("Ok", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    //  Action for 'NO' Button
                                    dialog.cancel();
                                }
                            });
                    AlertDialog alert = builder.create();
                    alert.show();
                }
else{
                PackageManager pm=activity.getPackageManager();
                try {

                    Intent waIntent = new Intent(Intent.ACTION_SEND);
                    waIntent.setType("text/plain");
                    String text = arraylist.get(position).get("name")+"\n"+arraylist.get(position).get("address");

                    PackageInfo info=pm.getPackageInfo("com.whatsapp", PackageManager.GET_META_DATA);
                    //Check if package exists or not. If not then code
                    //in catch block will be called
                    waIntent.setPackage("com.whatsapp");

                    waIntent.putExtra(Intent.EXTRA_TEXT, text);
                    activity.startActivity(Intent.createChooser(waIntent, "Share with"));

                } catch (PackageManager.NameNotFoundException e) {
                    Toast.makeText(activity, "WhatsApp not Installed", Toast.LENGTH_SHORT)
                            .show();
                }
            }}
        });
        return convertView;
    }
    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager = (ConnectivityManager) activity.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
}