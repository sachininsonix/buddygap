package com.buddygap.main.friend_menu;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.AlertDialog;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.buddygap.main.R;
import com.buddygap.main.home.Incoming_outgoing_calls;
import com.buddygap.main.iconstant.IConstant;
import com.buddygap.main.lastmonth_list.Lastmonth_menu_friendz;
import com.buddygap.main.this_monthlist.Send_invite;
import com.buddygap.main.utils.AppController;
import com.buddygap.main.utils.BaseActivity;
import com.buddygap.main.utils.UserPicture;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;


public class E_cards extends BaseActivity implements IConstant {

    LinearLayout back_layout, ecards_appbar, share_lay;
    GridView grid_item;
    String friend, phone, name;
    ImageLoader imageLoader;
    DisplayImageOptions options;
    TextView friends_text;
    int request_code = 1;


    Bitmap bitmap;
    String file, url_profile, invite,this_monthcheck;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_e_cards);
        request_code = 1;
        // initialize view's id
        initUI();


        try {
            friend = getIntent().getStringExtra("friends");
            phone = getIntent().getStringExtra("number");
            invite = getIntent().getStringExtra("invite");
            name = getIntent().getStringExtra("name");
            this_monthcheck = getIntent().getStringExtra("this_monthcheck");
            Log.d("name", name);
            imageLoader = ImageLoader.getInstance();
            imageLoader.init(ImageLoaderConfiguration.createDefault(E_cards.this));
            if (pref_getvalue_str("e_cards", "").toString().equals("")) {
                friends_text.setText("E_Cards");
            } else {
                friends_text.setText(pref_getvalue_str("e_cards", ""));
            }

        } catch (Exception e) {

        }

// change statusbar color
        Window window = getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);


        if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.KITKAT) {
            if (friend.equals(Incoming_outgoing_calls.best_frdz)) {
                ecards_appbar.setBackgroundResource(R.color.best_background_color);
                //   window.setStatusBarColor(getResources().getColor(R.color.best_background_color));
            } else if (friend.equals(Incoming_outgoing_calls.igr_frdz)) {
                ecards_appbar.setBackgroundResource(R.color.ignore_background_color);
                //   window.setStatusBarColor(getResources().getColor(R.color.ignore_background_color));
            } else if (friend.equals(Incoming_outgoing_calls.annoy_frdz)) {
                ecards_appbar.setBackgroundResource(R.color.annoy_background_color);
                //  window.setStatusBarColor(getResources().getColor(R.color.annoy_background_color));
            } else {
                ecards_appbar.setBackgroundResource(R.color.lousy_background_color);
                //  window.setStatusBarColor(getResources().getColor(R.color.lousy_background_color));
            }
        } else {
            if (friend.equals(Incoming_outgoing_calls.best_frdz)) {
                ecards_appbar.setBackgroundResource(R.color.best_background_color);
                window.setStatusBarColor(getResources().getColor(R.color.best_background_color));
            } else if (friend.equals(Incoming_outgoing_calls.igr_frdz)) {
                ecards_appbar.setBackgroundResource(R.color.ignore_background_color);
                window.setStatusBarColor(getResources().getColor(R.color.ignore_background_color));
            } else if (friend.equals(Incoming_outgoing_calls.annoy_frdz)) {
                ecards_appbar.setBackgroundResource(R.color.annoy_background_color);
                window.setStatusBarColor(getResources().getColor(R.color.annoy_background_color));
            } else {
                ecards_appbar.setBackgroundResource(R.color.lousy_background_color);
                window.setStatusBarColor(getResources().getColor(R.color.lousy_background_color));
            }
        }
/*if (invite.equals("2")){
    share_lay.setVisibility(View.GONE);
}*/
        share_lay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {

                    if (this_monthcheck.equals("0")){

                 if (invite.equals("1")) {
                        if (isNetworkAvailable()==false){
                            alert_dialog();
                        }
                        else{
                        Intent intent = new Intent(Intent.ACTION_PICK);
                        intent.setType("image/*");
                        startActivityForResult(intent, request_code);}
                    } else {

                        final Dialog dialog = new Dialog(E_cards.this);
                        dialog.getWindow().setTitleColor(getResources().getColor(R.color.splash_color));
                        //  dialog.setTitle("Send Invite");

                        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                        //     dialog.setCancelable(false);
                        dialog.setContentView(R.layout.dialog);
                        TextView text = (TextView) dialog.findViewById(R.id.text_dialog);
                        if (name.equals("No Name")) {
                            text.setText("This number " + phone + " has  not install BuddyGap.Please send invite for install BuddyGap");
                        } else {
                            text.setText(name + " has not install BuddyGap.Please send invite for install BuddyGap");
                        }
                        Button dialogButton = (Button) dialog.findViewById(R.id.send_invite);
                        dialogButton.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Intent intent = new Intent(E_cards.this, Send_invite.class);
                                intent.putExtra("phone_num", phone);
                                // intent.putExtra("name",array_name);
                                // intent.putExtra("number", array_number);
                                intent.putExtra("send_nam", name);
                                intent.putExtra("invite", invite);
                                intent.putExtra("frd_ranking", "3");
                                //  intent.putExtra("friends",igr_frdz);
                                intent.putExtra("friends", friend);
                                intent.putExtra("this_monthcheck", this_monthcheck);
                                startActivity(intent);
                                finish();
                                // dialog.dismiss();
                            }
                        });
                        dialog.show();


                    }
                }else{
                        if (invite.equals("1")) {
                            if (isNetworkAvailable()==false){
                                alert_dialog();
                            }
                            else{
                                Intent intent = new Intent(Intent.ACTION_PICK);
                                intent.setType("image/*");
                                startActivityForResult(intent, request_code);}
                        } else {

                            final Dialog dialog = new Dialog(E_cards.this);
                            dialog.getWindow().setTitleColor(getResources().getColor(R.color.splash_color));
                            //  dialog.setTitle("Send Invite");

                            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                            //     dialog.setCancelable(false);
                            dialog.setContentView(R.layout.dialog);
                            TextView text = (TextView) dialog.findViewById(R.id.text_dialog);
                            if (name.equals("No Name")) {
                                text.setText("This number " + phone + " has  not install BuddyGap.Please send invite for install BuddyGap");
                            } else {
                                text.setText(name + " has not install BuddyGap.Please send invite for install BuddyGap");
                            }
                            Button dialogButton = (Button) dialog.findViewById(R.id.send_invite);
                            dialogButton.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    Intent intent = new Intent(E_cards.this, Send_invite.class);
                                    intent.putExtra("phone_num", phone);
                                    // intent.putExtra("name",array_name);
                                    // intent.putExtra("number", array_number);
                                    intent.putExtra("send_nam", name);
                                    intent.putExtra("invite", invite);
                                    intent.putExtra("frd_ranking", "3");
                                    //  intent.putExtra("friends",igr_frdz);
                                    intent.putExtra("friends", friend);
                                    intent.putExtra("this_monthcheck", this_monthcheck);
                                    startActivity(intent);
                                    finish();
                                    // dialog.dismiss();
                                }
                            });
                            dialog.show();


                        }


                    }
                }
                catch (Exception e) {
                    e.printStackTrace();
                }


            }
        });
        back_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (this_monthcheck.equals("0")){
                Intent intent=new Intent(E_cards.this,Best_friendz.class);
                intent.putExtra("friends",friend);
                intent.putExtra("number",phone);
                intent.putExtra("invite",invite);
                intent.putExtra("name",name);
                startActivity(intent);
                finish();}
                else{
                    Intent intent=new Intent(E_cards.this,Lastmonth_menu_friendz.class);
                    intent.putExtra("friends",friend);
                    intent.putExtra("number",phone);
                    intent.putExtra("invite",invite);
                    intent.putExtra("name",name);
                    startActivity(intent);
                    finish();
                }

            }
        });

        try {
            ecards_adapter adapter = new ecards_adapter(E_cards.this);
            grid_item.setAdapter(adapter);
        } catch (Exception e) {

        }
    }

    @Override
    public void initUI() {
        back_layout = (LinearLayout) findViewById(R.id.back_layout);
        share_lay = (LinearLayout) findViewById(R.id.share_lay);
        ecards_appbar = (LinearLayout) findViewById(R.id.ecards_appbar);
        grid_item = (GridView) findViewById(R.id.grid_item);
        friends_text = (TextView) findViewById(R.id.friends);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);


        try {
            // if (request_code==1 && data.getData()!=null) {
            if (requestCode == request_code && resultCode == RESULT_OK
                    && null != data) {
                Uri uri = data.getData();
                Bitmap bitmap = new UserPicture(uri, getContentResolver()).getBitmap();
                //  imagee.setImageBitmap(bitmap);
                Intent intent = new Intent(E_cards.this, Send_image.class);
                // Bitmap bitmap1 = BitmapFactory.decodeResource(getResources(), R.drawable.a1);
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.PNG, 100, baos);
                byte[] b = baos.toByteArray();
                intent.putExtra("ecard_gallery", b);
                intent.putExtra("number", phone);
                intent.putExtra("name", name);
                startActivity(intent);



            } else {
                // Toast.makeText(E_cards.this,"please select image",Toast.LENGTH_LONG).show();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
        if (this_monthcheck.equals("0")){
            Intent intent=new Intent(E_cards.this,Best_friendz.class);
            intent.putExtra("friends",friend);
            intent.putExtra("number",phone);
            intent.putExtra("invite",invite);
            intent.putExtra("name",name);
            startActivity(intent);
            finish();
        }
        else{
            Intent intent=new Intent(E_cards.this,Lastmonth_menu_friendz.class);
            intent.putExtra("friends",friend);
            intent.putExtra("number",phone);
            intent.putExtra("invite",invite);
            intent.putExtra("name",name);
            startActivity(intent);
            finish();
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    void json_object() {

        url_profile = Base_url + "send_noti.php?";

        progress_show();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url_profile, new Response.Listener<String>() {
            @Override
            public void onResponse(String s) {

                try {
                    JSONObject jsonObject = new JSONObject(s);

                    String status = jsonObject.getString("status");
                    String message = jsonObject.getString("message");
                    if (status.equals("true")) {
//                        JSONObject jsonObject1 = jsonObject.getJSONObject("response");
                        Toast.makeText(E_cards.this, "Ecard sent", Toast.LENGTH_SHORT).show();

                    } else {
                        Toast.makeText(E_cards.this, message, Toast.LENGTH_SHORT).show();
                    }
                    Log.d("response", jsonObject.toString());
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                progress_dismiss();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                progress_dismiss();
                volleyError.printStackTrace();
                Log.d("Error", "error1" + volleyError);

            }


        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("tag", "SHOW_FILE");
                if (pref_getvalue_str("user_id", "") != null) {
                    params.put("user_id", pref_getvalue_str("user_id", ""));
                }
                if (phone != null) {
                    Log.d("number", phone);
                    params.put("phone", phone);
                }
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                if (bitmap == null) {

                } else {
                    bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
                    byte data[] = byteArrayOutputStream.toByteArray();
                    file = Base64.encodeToString(data, Base64.DEFAULT);
                }
                if (file == null) {

                } else {
                    params.put("profile_name", "" + new Date().getTime() + ".png");
                    params.put("profile_file", file);
                }
                return params;
            }

            ;
//            @Override
//            protected Map<String, String> getParams() throws AuthFailureError {
//
//                Map<String, String> params = new HashMap<>();
//
//                params.put("user_id",pref_getvalue_str("user_id", "").toString().trim());
//                params.put("phone", number);
//                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
//                if (bitmap == null) {
//
//                } else {
//                    bitmap.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream);
//                    byte data[] = byteArrayOutputStream.toByteArray();
//                    file = Base64.encodeToString(data, Base64.DEFAULT);
//
//                }
//
//                if (file == null) {
//
//                } else {
//                    params.put("profile_name", "" + new Date().getTime() + ".png");
//                    params.put("profile_file", file);
//                }
//
//                return params;
//
//            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(stringRequest);
    }

    class ecards_adapter extends BaseAdapter {
        int img[] = {R.drawable.e1, R.drawable.e5, R.drawable.e3, R.drawable.e4};
        Activity activity;


        public ecards_adapter(Activity activity) {
            this.activity = activity;
        }

        @Override
        public int getCount() {
            return img.length;
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = LayoutInflater.from(activity);
            convertView = inflater.inflate(R.layout.single_ecard, null);
            ImageView image_card = (ImageView) convertView.findViewById(R.id.image_card);
            image_card.setImageResource(img[position]);
            final int p = position;
            for (File file : Environment.getExternalStorageDirectory().listFiles()) {
                if (file.isFile() && file.getName().startsWith("temporary_file")) file.delete();
            }
            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //shareImageWhatsApp(p);





                 /*       AlertDialog.Builder builder = new AlertDialog.Builder(activity);

                        builder.setTitle("Confirm");
                        builder.setMessage("Are you want to share image using BuddyGap ?");

                        builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {

                            public void onClick(DialogInterface dialog, int which) {
                                // Do nothing but close the dialog
                                share_gmail(p);
                            }

                        });

                        builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                                dialog.dismiss();
                            }
                        });

                        AlertDialog alert = builder.create();
                        alert.show();*/

if(this_monthcheck.equals("0"))     {
    if (invite.equals("1")) {
                        if (isNetworkAvailable() == false) {
                            alert_dialog();
                        } else {
                            share_gmail(p);
                        }
                    } else {


                        final Dialog dialog = new Dialog(E_cards.this);
                        dialog.getWindow().setTitleColor(getResources().getColor(R.color.splash_color));
                        //  dialog.setTitle("Send Invite");

                        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                        //     dialog.setCancelable(false);
                        dialog.setContentView(R.layout.dialog);
                        TextView text = (TextView) dialog.findViewById(R.id.text_dialog);
                        if (name.equals("No Name")) {
                            text.setText("This number " + phone + " has  not install BuddyGap.Please send invite for install BuddyGap");
                        } else {
                            text.setText(name + " has not install BuddyGap.Please send invite for install BuddyGap");
                        }
                        Button dialogButton = (Button) dialog.findViewById(R.id.send_invite);
                        dialogButton.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Intent intent = new Intent(E_cards.this, Send_invite.class);
                                intent.putExtra("phone_num", phone);
                                // intent.putExtra("name",array_name);
                                // intent.putExtra("number", array_number);
                                intent.putExtra("send_nam", name);
                                intent.putExtra("invite", invite);
                                intent.putExtra("frd_ranking", "3");
                                //  intent.putExtra("friends",igr_frdz);
                                intent.putExtra("friends", friend);
                                intent.putExtra("this_monthcheck", this_monthcheck);
                                startActivity(intent);
                                finish();
                                // dialog.dismiss();
                            }
                        });
                        dialog.show();


                    }
                }
                else{ if (invite.equals("1")) {
    if (isNetworkAvailable() == false) {
        alert_dialog();
    } else {
        share_gmail(p);
    }
} else {


    final Dialog dialog = new Dialog(E_cards.this);
    dialog.getWindow().setTitleColor(getResources().getColor(R.color.splash_color));
    //  dialog.setTitle("Send Invite");

    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
    //     dialog.setCancelable(false);
    dialog.setContentView(R.layout.dialog);
    TextView text = (TextView) dialog.findViewById(R.id.text_dialog);
    if (name.equals("No Name")) {
        text.setText("This number " + phone + " has  not install BuddyGap.Please send invite for install BuddyGap");
    } else {
        text.setText(name + " has not install BuddyGap.Please send invite for install BuddyGap");
    }
    Button dialogButton = (Button) dialog.findViewById(R.id.send_invite);
    dialogButton.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent = new Intent(E_cards.this, Send_invite.class);
            intent.putExtra("phone_num", phone);
            // intent.putExtra("name",array_name);
            // intent.putExtra("number", array_number);
            intent.putExtra("send_nam", name);
            intent.putExtra("invite", invite);
            intent.putExtra("frd_ranking", "3");
            //  intent.putExtra("friends",igr_frdz);
            intent.putExtra("friends", friend);
            intent.putExtra("this_monthcheck", this_monthcheck);
            startActivity(intent);
            finish();
            // dialog.dismiss();
        }
    });
    dialog.show();


}
}
                }
            });
            return convertView;
        }

        void share_gmail(int p) {
            bitmap = BitmapFactory.decodeResource(getResources(), img[p]);
//            image_save_notification();

            String path = MediaStore.Images.Media.insertImage(getContentResolver(), bitmap, "title", null);
            Uri screenshotUri = Uri.parse(path);
            try {

                bitmap = new UserPicture(screenshotUri, getContentResolver()).getBitmap();
//                profile_img.setImageBitmap(bitmap);


            } catch (Exception e) {

            }
            json_object();
//            final Intent emailIntent = new Intent(Intent.ACTION_SEND);
//            emailIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//            emailIntent.putExtra(Intent.EXTRA_STREAM, screenshotUri);
//            emailIntent.setType("image/png");
//            startActivity(Intent.createChooser(emailIntent, "Send email using"));
        }


    }
//    private static File getOutputMediaFile(){
//        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(
//                Environment.DIRECTORY_PICTURES), "CameraDemo");
//
//        if (!mediaStorageDir.exists()){
//            if (!mediaStorageDir.mkdirs()){
//                return null;
//            }
//        }
//
//        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
//        return new File(mediaStorageDir.getPath() + File.separator +
//                "IMG_"+ timeStamp + ".jpg");
//    }


}




