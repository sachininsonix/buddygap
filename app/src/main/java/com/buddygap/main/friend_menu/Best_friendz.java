package com.buddygap.main.friend_menu;

import android.Manifest;
import android.content.ContentUris;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.drawable.GradientDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.CalendarContract;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.buddygap.main.R;
import com.buddygap.main.home.Incoming_outgoing_calls;
import com.buddygap.main.iconstant.IConstant;
import com.buddygap.main.utils.AppController;
import com.buddygap.main.utils.BaseActivity;
import com.buddygap.main.utils.GPSTracker;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.TimeZone;


/**
 * Created by admin on 19-04-2016.
 */
public class Best_friendz extends BaseActivity implements IConstant {
    LinearLayout get_together,e_cards,sms,call,friends_detail,reminder,direct_call;
    ImageView back_btn,call_icon;
    int hour12;
    TextView name_bestfrd,number,friends,condition,reminder_text,no_datatext;
    TextView get_togathertext,call_text,sms_text,ecards_text,name_city,temp;
    static String contact_no,friend,Str_Name;
    LinearLayout back_layout,friends_appbar,progress_bar,weather_lay;
    RelativeLayout weather_img,no_data;
    String lat,lon,invite,name;
   // SharedPreferences sharedPreferences;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.best_friendz);

        // initialize view's id

       initUI();

        GradientDrawable drawable = new GradientDrawable();
        drawable.setShape(GradientDrawable.RECTANGLE);
//        drawable.setStroke(3, Color.BLACK);
//        drawable.setCornerRadius(0);
//      //  drawable.setColor(Color.BLUE);
//        weather_lay.setBackgroundDrawable(drawable);

        try {

            if (pref_getvalue_str("gettogather", "").toString().equals("")) {
                get_togathertext.setText("Let's Meet");
          //      call_text.setText("Call");
                sms_text.setText("SMS");
                reminder_text.setText("reminder");
                ecards_text.setText("E_Cards");
            } else {
                try {
                    get_togathertext.setText(pref_getvalue_str("gettogather", ""));
                  //  call_text.setText(pref_getvalue_str("call", ""));
                    sms_text.setText(pref_getvalue_str("sms", ""));
                   reminder_text.setText(pref_getvalue_str("reminder", ""));
                    ecards_text.setText(pref_getvalue_str("e_cards", ""));
                } catch (Exception e) {
                }
            }
        }
        catch (Exception e){

        }


        final Intent intent=getIntent();
        try{
            name=intent.getStringExtra("name");
        name_bestfrd.setText(name);
        contact_no=intent.getStringExtra("number");
        number.setText(contact_no);
        friend=intent.getStringExtra("friends");
      //  rank.setText(intent.getStringExtra("rank"));
        friends.setText(friend);
            invite=intent.getStringExtra("invite");
            // lat lon send by login user to weather api
            if (name.equals("No Name")){
                Str_Name="BuddyGap not able to fetch weather update as per "+contact_no+" number's permissions to app";
            }
            else{
                Str_Name="BuddyGap not able to fetch weather update as per "+name+"'s permissions to app";
            }
            Log.d("Str_Name",Str_Name);
            if(isNetworkAvailable()==false){
                no_data.setVisibility(View.VISIBLE);
                no_datatext.setText(Str_Name);

                progress_bar.setVisibility(View.GONE);
            }
            else {
                get_latlon();
            }


        }
        catch (Exception e){
e.printStackTrace();
        }
        /* if(Friend_Ranking.frd_rank=="1"){
             rank.setText(intent.getStringExtra("rank"));
         }*/

        // chnage status bar color
        Window window = getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);




       // Log.d("res", String.valueOf(installed_list.size()));
        if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.KITKAT) {
            if (friend.equals(Incoming_outgoing_calls.best_frdz)){

            }
            else if(friend.equals(Incoming_outgoing_calls.igr_frdz)){


            }
            else if(friend.equals(Incoming_outgoing_calls.annoy_frdz)){

            }
            else {

            }}
        else{
            if (friend.equals(Incoming_outgoing_calls.best_frdz)){
                window.setStatusBarColor(getResources().getColor(R.color.best_background_color));
            }
            else if(friend.equals(Incoming_outgoing_calls.igr_frdz)){
                window.setStatusBarColor(getResources().getColor(R.color.ignore_background_color));

            }
            else if(friend.equals(Incoming_outgoing_calls.annoy_frdz)){
                window.setStatusBarColor(getResources().getColor(R.color.annoy_background_color));
            }
            else {
                window.setStatusBarColor(getResources().getColor(R.color.lousy_background_color));
            }
        }
        if(friend.equals(Incoming_outgoing_calls.best_frdz)){

         //   friends_detail.setBackgroundResource(R.color.best_color);
          get_together.setBackgroundResource(R.color.best_gettogather);
            reminder.setBackgroundResource(R.color.best_reminder);
           // call.setBackgroundResource(R.color.best_call);
            sms.setBackgroundResource(R.color.best_sms);
            e_cards.setBackgroundResource(R.color.best_ecards);
            get_togathertext.setBackgroundResource(R.color.best_gettogather);
           reminder_text.setBackgroundResource(R.color.best_reminder);
           // call_text.setBackgroundResource(R.color.best_call);
            sms_text.setBackgroundResource(R.color.best_sms);
            ecards_text.setBackgroundResource(R.color.best_ecards);

            friends_appbar.setBackgroundResource(R.color.best_background_color);

            drawable.setStroke(3, getResources().getColor(R.color.best_gettogather));
            drawable.setCornerRadius(0);
            //  drawable.setColor(Color.BLUE);
            weather_lay.setBackgroundDrawable(drawable);



        }
        else if(friend.equals(Incoming_outgoing_calls.igr_frdz)){

         //   friends_detail.setBackgroundResource(R.color.ignore_color);
            get_together.setBackgroundResource(R.color.igr_gettogather);
           reminder.setBackgroundResource(R.color.igr_reminder);
        //    call.setBackgroundResource(R.color.igr_call);
            sms.setBackgroundResource(R.color.igr_sms);
            e_cards.setBackgroundResource(R.color.igr_ecards);
            get_togathertext.setBackgroundResource(R.color.igr_gettogather);
          reminder_text.setBackgroundResource(R.color.igr_reminder);
           // call_text.setBackgroundResource(R.color.igr_call);
            sms_text.setBackgroundResource(R.color.igr_sms);
            ecards_text.setBackgroundResource(R.color.igr_ecards);

            friends_appbar.setBackgroundResource(R.color.ignore_background_color);
            drawable.setStroke(3, getResources().getColor(R.color.igr_gettogather));
            drawable.setCornerRadius(0);
            //  drawable.setColor(Color.BLUE);
            weather_lay.setBackgroundDrawable(drawable);

        }
        else if(friend.equals(Incoming_outgoing_calls.annoy_frdz)){

           // friends_detail.setBackgroundResource(R.color.annoy_color);
            get_together.setBackgroundResource(R.color.ann_gettogather);
         reminder.setBackgroundResource(R.color.ann_reminder);
         //   call.setBackgroundResource(R.color.ann_call);
            sms.setBackgroundResource(R.color.ann_sms);
            e_cards.setBackgroundResource(R.color.ann_ecards);
            get_togathertext.setBackgroundResource(R.color.ann_gettogather);
           reminder_text.setBackgroundResource(R.color.ann_reminder);
          //  call_text.setBackgroundResource(R.color.ann_call);
            sms_text.setBackgroundResource(R.color.ann_sms);
            ecards_text.setBackgroundResource(R.color.ann_ecards);

            friends_appbar.setBackgroundResource(R.color.annoy_background_color);
            drawable.setStroke(3, getResources().getColor(R.color.ann_gettogather));
            drawable.setCornerRadius(0);
            //  drawable.setColor(Color.BLUE);
            weather_lay.setBackgroundDrawable(drawable);
        }
        else {

            //friends_detail.setBackgroundResource(R.color.lousy_color);
            get_together.setBackgroundResource(R.color.lousy_gettogather);
            reminder.setBackgroundResource(R.color.lousy_reminder);
         //   call.setBackgroundResource(R.color.lousy_call);
            sms.setBackgroundResource(R.color.lousy_sms);
            e_cards.setBackgroundResource(R.color.lousy_ecards);
            get_togathertext.setBackgroundResource(R.color.lousy_gettogather);
           reminder_text.setBackgroundResource(R.color.lousy_reminder);
          //  call_text.setBackgroundResource(R.color.lousy_call);
            sms_text.setBackgroundResource(R.color.lousy_sms);
            ecards_text.setBackgroundResource(R.color.lousy_ecards);

            friends_appbar.setBackgroundResource(R.color.lousy_background_color);

            drawable.setStroke(3, getResources().getColor(R.color.lousy_gettogather));
            drawable.setCornerRadius(0);
            //  drawable.setColor(Color.BLUE);
            weather_lay.setBackgroundDrawable(drawable);
        }




        back_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent intent1=new Intent(Best_friendz.this,Bestfrdz_list.class);
//                startActivity(intent1);
                Best_friendz.this.finish();

            }
        });

        reminder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Uri.Builder builder = CalendarContract.CONTENT_URI.buildUpon();
                builder.appendPath("time");
                ContentUris.appendId(builder, Calendar.getInstance().getTimeInMillis());
                Intent intent = new Intent(Intent.ACTION_VIEW)
                        .setData(builder.build());
                startActivity(intent);
            }
        });

        sms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Uri uri = Uri.parse("smsto:"+contact_no);
                Intent it = new Intent(Intent.ACTION_SENDTO, uri);
             //   it.setPackage()
                it.putExtra("sms_body", " ");
                startActivity(it);
                //startActivity(new Intent(Intent.ACTION_VIEW, Uri.fromParts("sms", contact_no)));
//finish();
            }
        });

        friends_detail.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {
        Intent intent = new Intent(Intent.ACTION_CALL);
        intent.setData(Uri.parse("tel:"+"0"+contact_no));
        startActivity(intent);
  //      finish();
    }
});




        e_cards.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (weHavePermissionToReadContacts()) {
//            readTheContacts();


                    Intent intent = new Intent(Best_friendz.this,E_cards.class);
                    intent.putExtra("friends",friend);
                    intent.putExtra("number",contact_no);
                    intent.putExtra("invite",invite);
                    intent.putExtra("name",name);
                    intent.putExtra("this_monthcheck","0");

                    startActivity(intent);
                    finish();
                } else {
                    requestReadContactsPermissionFirst();
                }

               // Best_friendz.this.finish();
            }
        });


        get_together.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (weHavePermissionToAccessLocations()) {
//            readTheContacts();

                    if(!new GPSTracker(Best_friendz.this).canGetLocation()){
                        new GPSTracker(Best_friendz.this).showSettingsAlert();
                    }
                    if(new GPSTracker(Best_friendz.this).getLatitude()!=0 && new GPSTracker(Best_friendz.this).getLongitude()!=0 ){
                        Intent intent = new Intent(Best_friendz.this,Get_together.class);
                        intent.putExtra("number",contact_no);
                        intent.putExtra("friends",friend);
                        intent.putExtra("invite",invite);
                        intent.putExtra("name",name);
                        intent.putExtra("this_monthcheck","0");
//                        intent.putExtra("name",name);
//                        intent.putExtra("invite",invite);
                      //  intent.putExtra("invite",this);
                        startActivity(intent);
                    finish();}
                } else {
                    requestReadAccessLocationFirst();
                }


               // Best_friendz.this.finish();
            }
        });
    }

    @Override
    public void initUI() {
        get_together=(LinearLayout)findViewById(R.id.get_together_layout);

        reminder=(LinearLayout)findViewById(R.id.reminder_layout);
        direct_call=(LinearLayout)findViewById(R.id.direct_call);
        weather_lay=(LinearLayout)findViewById(R.id.weather_lay);
        friends_appbar=(LinearLayout)findViewById(R.id.friends_appbar);
        progress_bar=(LinearLayout)findViewById(R.id.progress_bar);
        no_data=(RelativeLayout)findViewById(R.id.no_data);
        friends_detail=(LinearLayout)findViewById(R.id.friends_detail);
        call_icon=(ImageView)findViewById(R.id.call_icon);
        condition=(TextView)findViewById(R.id.condition);
        sms=(LinearLayout)findViewById(R.id.sms_layout);
        back_btn=(ImageView)findViewById(R.id.back_btn);
        e_cards=(LinearLayout)findViewById(R.id.e_cards_layout);
      //  call=(LinearLayout)findViewById(R.id.call_layout);
        back_layout=(LinearLayout)findViewById(R.id.back_layout);
        name_bestfrd=(TextView)findViewById(R.id.name);
        number=(TextView)findViewById(R.id.phn_number);
        friends=(TextView)findViewById(R.id.friends);
        get_togathertext=(TextView)findViewById(R.id.get_togathertext);
       reminder_text=(TextView)findViewById(R.id.reminder_text);
       // call_text=(TextView)findViewById(R.id.call_text);
        sms_text=(TextView)findViewById(R.id.sms_text);
        ecards_text=(TextView)findViewById(R.id.ecards_text);
        weather_img=(RelativeLayout)findViewById(R.id.weather_img);
        temp=(TextView)findViewById(R.id.temp);
        name_city=(TextView)findViewById(R.id.name_city);
        no_datatext=(TextView)findViewById(R.id.no_datatext);
    }




    public  void json_object(String lati,String loni){
        //String url="http://api.openweathermap.org/data/2.5/weather?lat=-33.87&lon=151.21&APPID=ea574594b9d36ab688642d5fbeab847e";
        String url="http://api.openweathermap.org/data/2.5/weather?lat="+lati+"&lon="+loni+"&APPID=ea574594b9d36ab688642d5fbeab847e";
        Log.e("url", url);
        //  progress_show();
        JsonObjectRequest jsonObjectRequest=new JsonObjectRequest(url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject jsonObject) {
                try {
                    Log.e("jsonn", String.valueOf(jsonObject));

                    JSONObject jsonObject1=jsonObject.getJSONObject("coord");
//                    String lon=jsonObject1.getString("lon");
//                    String lat=jsonObject1.getString("lat");
//                    Log.d("lat", String.valueOf(lat));
                    JSONArray jsonArray=jsonObject.getJSONArray("weather");
                    JSONObject jsonObject11=jsonArray.getJSONObject(0);
                    String id=jsonObject11.getString("id");

                    String main=jsonObject11.getString("main");
                    Log.d("tempp", String.valueOf(main));
                    String icon=jsonObject11.getString("icon");
                    JSONObject jsonObject12=jsonObject.getJSONObject("main");
                    double temp1=jsonObject12.getInt("temp");
                    String pressure=jsonObject12.getString("pressure");
                    String humidity=jsonObject12.getString("humidity");
                    String name=jsonObject.getString("name");
                    temp1=temp1-273.15;
                    int t= (int) temp1;
                    temp.setText("Temp " + t + " " + (char) 0x00B0 + "C");
                    Log.d("tempp", String.valueOf(t));
                    Log.d("clear_hour12", "hpurs" + hour12);



                    name_city.setText(name);
                    condition.setText(main);
                    weather_img.setVisibility(View.VISIBLE);
                    if(main.equals("Clouds")){
                        progress_bar.setVisibility(View.VISIBLE);
                        weather_img.setBackgroundResource(R.drawable.cloud);
                        progress_bar.setVisibility(View.GONE);
                    }
                    else if(main.equals("Thunderstorm")){
                        progress_bar.setVisibility(View.VISIBLE);
                        weather_img.setBackgroundResource(R.drawable.thuderstrom);
                        progress_bar.setVisibility(View.GONE);
                    }
                    else if(main.equals("Clear")){
                        progress_bar.setVisibility(View.VISIBLE);
                        if(hour12 >=6 && hour12 <= 18) {
                            weather_img.setBackgroundResource(R.drawable.sun);
                        }
                        else{
                            weather_img.setBackgroundResource(R.drawable.moon);
                        }
                        progress_bar.setVisibility(View.GONE);
                    }
                    else if (main.equals("Rain")){
                        progress_bar.setVisibility(View.VISIBLE);
                        weather_img.setBackgroundResource(R.drawable.rain);
                        progress_bar.setVisibility(View.GONE);
                    }
                    else if (main.equals("Fog")){
                        progress_bar.setVisibility(View.VISIBLE);
                        weather_img.setBackgroundResource(R.drawable.fogg);
                        progress_bar.setVisibility(View.GONE);
                    }
                    else if (main.equals("Mist")){
                        progress_bar.setVisibility(View.VISIBLE);
                        weather_img.setBackgroundResource(R.drawable.mist);
                        progress_bar.setVisibility(View.GONE);

                    }
                    else if (main.equals("Snow")){
                        progress_bar.setVisibility(View.VISIBLE);
                        weather_img.setBackgroundResource(R.drawable.snow);
                        progress_bar.setVisibility(View.GONE);

                    }
                    else if (main.equals("Haze")){
                        progress_bar.setVisibility(View.VISIBLE);
                        weather_img.setBackgroundResource(R.drawable.haze);
                        progress_bar.setVisibility(View.GONE);

                    }
                    else{
                        progress_bar.setVisibility(View.VISIBLE);
                        if(hour12 >=6 && hour12 <= 18) {
                            weather_img.setBackgroundResource(R.drawable.sun);
                        }
                        else{
                            weather_img.setBackgroundResource(R.drawable.moon);
                        }
                        progress_bar.setVisibility(View.GONE);
                    }

                    Log.e("mg",lon+"lat="+lat+"id="+id+"main="+main+"icon="+icon+"temp="+temp+"pre="+pressure);
                }
                catch (JSONException e) {
                    e.printStackTrace();
                }


                Log.e("msg",jsonObject.toString());
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.e("msg","error");
                // progress_dismiss();
            }
        });
        AppController.getInstance().addToRequestQueue(jsonObjectRequest);
    }
    public  void gettime_date(String latit,String lonit){
        //String url="http://api.openweathermap.org/data/2.5/weather?lat=-33.87&lon=151.21&APPID=ea574594b9d36ab688642d5fbeab847e";
        String url="https://maps.googleapis.com/maps/api/timezone/json?location="+latit+","+lonit+"&timestamp=1374868635&sensor=false";
        Log.e("url", url);
        //  progress_show();
        JsonObjectRequest jsonObjectRequest=new JsonObjectRequest(url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject jsonObject) {
                try {
                    Log.e("jsonn", String.valueOf(jsonObject));
                    String timeZone =jsonObject.getString("timeZoneId");
                    Calendar c = Calendar.getInstance();
                    c.setTimeZone(TimeZone.getTimeZone(timeZone));

                    hour12 = c.get(Calendar.HOUR_OF_DAY);
                    Log.d("hour12","hpurs"+hour12);
                    json_object(lat, lon);

                }
                catch (JSONException e) {
                    e.printStackTrace();
                }


                Log.e("msg",jsonObject.toString());
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.e("msg","error");
                // progress_dismiss();
            }
        });
        AppController.getInstance().addToRequestQueue(jsonObjectRequest);
    }
    public  void get_latlon(){
        //String url="http://api.openweathermap.org/data/2.5/weather?lat=-33.87&lon=151.21&APPID=ea574594b9d36ab688642d5fbeab847e";
        String url=Base_url+"get_lat_lon.php?phone="+contact_no;
        //http://api.openweathermap.org/data/2.5/weather?lat=30.7105&lon=76.7033&APPID=ea574594b9d36ab688642d5fbeab847e";
        Log.e("urll", url);
        // progress_show();
        JsonObjectRequest jsonObjectRequest=new JsonObjectRequest(url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject jsonObject) {
                try {

                    String status=jsonObject.getString("status");
                    if (status.equals("false")){
                        no_data.setVisibility(View.VISIBLE);
                        no_datatext.setText(Str_Name);
                        weather_img.setVisibility(View.GONE);
                        //  progress_dismiss();
                    }

                    JSONObject jsonObject1=jsonObject.getJSONObject("response");
                    lat=jsonObject1.getString("lat");
                    lon=jsonObject1.getString("lon");
                    Log.d("latnull", lat);
                    if(lat.equals("") && lon.equals("")){
                        no_data.setVisibility(View.VISIBLE);
                        no_datatext.setText(Str_Name);
                        weather_img.setVisibility(View.GONE);

                    }
                    else {
                        gettime_date(lat, lon);


                    }
                }
                catch (JSONException e) {
                    e.printStackTrace();
                }
                Log.e("msg",jsonObject.toString());
                //  progress_dismiss();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.e("msg","error");
                //    progress_dismiss();
            }
        });
        AppController.getInstance().addToRequestQueue(jsonObjectRequest);
    }






    @Override
    public void onBackPressed() {
      //  super.onBackPressed();
        Incoming_outgoing_calls.gps_sett=0;
        Incoming_outgoing_calls.permission_deny=0;
        finish();
    }
    //Start for marshmallow Permission to access locations
    private boolean weHavePermissionToAccessLocations() {
        return ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED&&ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED;}



    private void requestReadAccessLocationFirst() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_COARSE_LOCATION)&&ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_FINE_LOCATION)) {
//            Toast.makeText(this, "We need permission so you can access your External storage", Toast.LENGTH_LONG).show();
            requestForResultLocationsPermission();
        } else {
            requestForResultLocationsPermission();
        }
    }

    private void requestForResultLocationsPermission() {
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION}, 124);
    }

    //End for marshmallow Permission to access locations
    //Start for marshmallow Permission to access External Storage
    private boolean weHavePermissionToReadContacts() {
        return ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED&&ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED;}



    private void requestReadContactsPermissionFirst() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)&&ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_EXTERNAL_STORAGE)) {
//            Toast.makeText(this, "We need permission so you can access your External storage", Toast.LENGTH_LONG).show();
            requestForResultContactsPermission();
        } else {
            requestForResultContactsPermission();
        }
    }

    private void requestForResultContactsPermission() {
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE,Manifest.permission.READ_EXTERNAL_STORAGE}, 123);
    }
    //End for marshmallow Permission to access External Storage
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 123
                && grantResults.length > 0
                && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//            Toast.makeText(this, "Permission Granted", Toast.LENGTH_SHORT).show();

            Intent intent = new Intent(Best_friendz.this,E_cards.class);
            intent.putExtra("friends",friend);
            intent.putExtra("number",contact_no);
            intent.putExtra("invite",invite);
            intent.putExtra("name",name);
            intent.putExtra("this_monthcheck","0");
            startActivity(intent);
            finish();

        }
     else if(requestCode == 124
                && grantResults.length > 0
                && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//            Toast.makeText(this, "Permission Granted", Toast.LENGTH_SHORT).show();

            if(!new GPSTracker(Best_friendz.this).canGetLocation()){
                new GPSTracker(Best_friendz.this).showSettingsAlert();
            }
            if(new GPSTracker(Best_friendz.this).getLatitude()!=0 && new GPSTracker(Best_friendz.this).getLongitude()!=0 ){
                Intent intent = new Intent(Best_friendz.this,Get_together.class);
                intent.putExtra("contactno",contact_no);
                intent.putExtra("friends",friend);
                startActivity(intent);
            finish();}
        }else {
            Toast.makeText(this, "Permission Denied", Toast.LENGTH_SHORT).show();
        }
    }




// feel notification






}




