package com.buddygap.main.AlarmService;

import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.WakefulBroadcastReceiver;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;

import com.buddygap.main.R;
import com.buddygap.main.utils.Notification_feel;

import java.util.Calendar;



/**
 * Created by klogi
 *
 *
 */
public class NotificationIntentService extends IntentService {

    private static Intent intent;
    private  int NOTIFICATION_ID = 1;
    private static final String ACTION_START = "ACTION_START";
    private static final String ACTION_DELETE = "ACTION_DELETE";
    private SharedPreferences sharedPreferences;

    public NotificationIntentService() {
        super(NotificationIntentService.class.getSimpleName());
    }

    public static Intent createIntentStartNotificationService(Context context) {

            intent = new Intent(context, NotificationIntentService.class);
            intent.setAction(ACTION_START);
        return intent;
    }

    public static Intent createIntentDeleteNotification(Context context) {
        Intent intent = new Intent(context, NotificationIntentService.class);
        intent.setAction(ACTION_DELETE);
        return intent;
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Log.d(getClass().getSimpleName(), "onHandleIntent, started handling a notification event");
        sharedPreferences=getSharedPreferences("first_name", Context.MODE_PRIVATE);
        try {
            String action = intent.getAction();
            if (ACTION_START.equals(action)) {
                Calendar calendar = Calendar.getInstance();
                if(calendar.getTimeInMillis()==NotificationEventReceiver.getTriggerAt())
                   if (!sharedPreferences.getString("user_id", "").equals("")) {
                        Toast.makeText(getApplicationContext(), "service called", Toast.LENGTH_SHORT).show();
//                Toast.makeText(getApplicationContext(),GlobalData.getValue(getApplicationContext(),"user_id") , Toast.LENGTH_SHORT).show();
                        processStartNotification();
                   }
                   }

        } finally {
            WakefulBroadcastReceiver.completeWakefulIntent(intent);
        }
    }

    private void processDeleteNotification(Intent intent) {
        // Log something?
    }

    private void processStartNotification() {
        // Do something. For example, fetch fresh data from backend to create a rich notification?

        final NotificationCompat.Builder builder = new NotificationCompat.Builder(this);
        builder.setContentTitle("Feelings of the Day")
                .setAutoCancel(true)
                .setContentText("How was your day today?")
                .setStyle(new NotificationCompat.BigTextStyle().bigText("How was your day today?"))
                .setDefaults(Notification.DEFAULT_ALL)
                .setSmallIcon(R.drawable.app_icon);

        Intent mainIntent = new Intent(this, Notification_feel.class);
        mainIntent.putExtra("frommulti"," ");
        PendingIntent pendingIntent = PendingIntent.getActivity(this,
                NOTIFICATION_ID,
                mainIntent,
                PendingIntent.FLAG_UPDATE_CURRENT);
        builder.setContentIntent(pendingIntent);
        builder.setDeleteIntent(NotificationEventReceiver.getDeleteIntent(this));

        final NotificationManager manager = (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);
        manager.notify(++NOTIFICATION_ID, builder.build());
    }
}
