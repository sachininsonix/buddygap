package com.buddygap.main.Internet_connection;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.buddygap.main.R;
import com.buddygap.main.utils.AppController;
import com.buddygap.main.utils.BaseActivity;
import com.buddygap.main.utils.Splash_screen;

import com.buddygap.main.R;
import com.google.android.gms.analytics.HitBuilders;

public class Try_again extends BaseActivity {
Button try_again;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.try_again);
        initUI();
        AppController.tracker().send(new HitBuilders.EventBuilder("ui", "open")
                .setLabel("Try_Again Activity")
                .build());



        try_again.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            Thread.sleep(1000);
                            if (isNetworkAvailable() == false) {
                               alert_dialog();

                            } else {
                                Intent intent = new Intent(Try_again.this, Splash_screen.class);
                                startActivity(intent);
                                finish();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }).start();

            }
        });
    }

    @Override
    public void initUI() {
        try_again=(Button)findViewById(R.id.try_again);
    }


}
