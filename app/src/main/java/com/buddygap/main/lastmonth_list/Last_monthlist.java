package com.buddygap.main.lastmonth_list;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.buddygap.main.R;
import com.buddygap.main.home.Incoming_outgoing_calls;
import com.buddygap.main.home.cur_month_out;
import com.buddygap.main.iconstant.IConstant;
import com.buddygap.main.searchadapter.searchfilter_lastmonthlist;
import com.buddygap.main.utils.AppController;
import com.buddygap.main.utils.BaseActivity;
import com.buddygap.main.utils.TextDrawable;
import com.buddygap.main.utils.WorldPopulation;
import com.google.android.gms.analytics.HitBuilders;

import org.json.JSONObject;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class Last_monthlist extends BaseActivity implements IConstant {
    ListView list_item, list_item1;
    LinearLayout back_layout, search, friends_list_appbar;
    static List<String> list_number, list_name;
    ArrayList<String> list, originallist, ordinal_ranklist, array_shared_install,arraylist_mood;
    public static String friend, url;
    EditText search_edit;
    last_adapter adapter;
    searchfilter_lastmonthlist arrayAdapter;
    static int a, ordinal = 0;
    TextView friends, no_msg, ranktext_color;
    int p = 0, color;
    String url_image;
    int textrank_color;
    int divide = 0, search_value = 0;
ImageView search_btn;
    ArrayList<WorldPopulation> world = new ArrayList<WorldPopulation>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_last_monthlist);
        AppController.tracker().send(new HitBuilders.EventBuilder("ui", "open")
                .setLabel("Last Month Activity")
                .build());
        // initialize view id's
        initUI();

        // arraylist
        list_name = new ArrayList<>();
        list_number = new ArrayList<>();
        arraylist_mood = new ArrayList<>();
        ordinal_ranklist = new ArrayList<>();
        array_shared_install = new ArrayList<>();
        list = new ArrayList<>();
        originallist = new ArrayList<>();

        Intent intent = getIntent();
        try {
            list_name = intent.getStringArrayListExtra("name");
            list_number = intent.getStringArrayListExtra("number");
            friend = intent.getStringExtra("friends");
            json(friend, list_number);
            for (int i = 0; i < list_name.size(); i++) {
                arraylist_mood.add("Neutral");
                if (list_name.get(i).equals("No Name")) {
                    //textView.setText(Bestfrdz_list.list_number.get(position));
                    list.add(Last_monthlist.list_number.get(i));
                } else {
                    //  textView.setText(getList().get(position));
                    list.add(list_name.get(i));
                }
            }
        } catch (Exception e) {

        }

        // statucbar color change
        Window window = getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

        friends.setText(friend);

        if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.KITKAT) {
            if (friend.equals(Incoming_outgoing_calls.best_frdz)) {
                friends_list_appbar.setBackgroundResource(R.color.best_background_color);
                //window.setStatusBarColor(getResources().getColor(R.color.best_background_color));
                p = 1;
                color = R.color.best_rank;
                textrank_color = R.color.best_background_color;
                ranktext_color.setTextColor(getResources().getColor(textrank_color));
            } else if (friend.equals(Incoming_outgoing_calls.igr_frdz)) {
                p = 1 + cur_month_out.list4.size();
                color = R.color.igr_rank;

                friends_list_appbar.setBackgroundResource(R.color.ignore_background_color);
                textrank_color = R.color.ignore_background_color;
                //   tintManager.setTintColor(Color.parseColor("#ffffff"));
                ranktext_color.setTextColor(getResources().getColor(textrank_color));
                //window.setStatusBarColor(getResources().getColor(R.color.ignore_background_color));
            } else if (friend.equals(Incoming_outgoing_calls.annoy_frdz)) {
                friends_list_appbar.setBackgroundResource(R.color.annoy_background_color);
                // window.setStatusBarColor(getResources().getColor(R.color.annoy_background_color));
                p = cur_month_out.list2.size() + 1 + cur_month_out.list4.size();
                textrank_color = R.color.annoy_background_color;
                color = R.color.ann_rank;
                ranktext_color.setTextColor(getResources().getColor(textrank_color));
            } else {
                friends_list_appbar.setBackgroundResource(R.color.lousy_background_color);
                // window.setStatusBarColor(getResources().getColor(R.color.lousy_background_color));
                p = cur_month_out.list0.size() + 1 + cur_month_out.list2.size() + cur_month_out.list4.size();
                textrank_color = R.color.lousy_background_color;
                color = R.color.lousy_rank;
                ranktext_color.setTextColor(getResources().getColor(textrank_color));
            }
        } else {
            if (friend.equals(Incoming_outgoing_calls.best_frdz)) {
                friends_list_appbar.setBackgroundResource(R.color.best_background_color);
                window.setStatusBarColor(getResources().getColor(R.color.best_background_color));
                p = 1;
                color = R.color.best_rank;
                textrank_color = R.color.best_background_color;
                ranktext_color.setTextColor(getResources().getColor(textrank_color));
            } else if (friend.equals(Incoming_outgoing_calls.igr_frdz)) {
                friends_list_appbar.setBackgroundResource(R.color.ignore_background_color);
                window.setStatusBarColor(getResources().getColor(R.color.ignore_background_color));
                p = 1 + Last_month_outgoingcall.list4.size();
                color = R.color.igr_rank;
                textrank_color = R.color.ignore_background_color;
                ranktext_color.setTextColor(getResources().getColor(textrank_color));
            } else if (friend.equals(Incoming_outgoing_calls.annoy_frdz)) {
                friends_list_appbar.setBackgroundResource(R.color.annoy_background_color);
                window.setStatusBarColor(getResources().getColor(R.color.annoy_background_color));
                p = Last_month_outgoingcall.list2.size() + 1 + Last_month_outgoingcall.list4.size();
                textrank_color = R.color.annoy_background_color;
                color = R.color.ann_rank;
                ranktext_color.setTextColor(getResources().getColor(textrank_color));
            } else {
                friends_list_appbar.setBackgroundResource(R.color.lousy_background_color);
                window.setStatusBarColor(getResources().getColor(R.color.lousy_background_color));
                p = Last_month_outgoingcall.list1.size() + 1 + Last_month_outgoingcall.list2.size() + Last_month_outgoingcall.list4.size();
                textrank_color = R.color.lousy_background_color;
                color = R.color.lousy_rank;
                ranktext_color.setTextColor(getResources().getColor(textrank_color));
            }
        }

        originallist = new ArrayList<>(list);
        divide = 0;
        for (int i = 0; i < list_number.size(); i++) {
            int sum = p + i;
            // rank_list.add(sum);

            int aa = sum;
            //  divide=0;
            // ordinal=0;
            Log.d("rank_aa", String.valueOf(aa));
            if ((aa - 1) % 10 == 0 && aa > 20) {
                divide = (aa - 1) / 10;
                ordinal = 0;
            } else if ((aa - 2) % 10 == 0 && aa > 20) {
                divide = (aa - 2) / 10;
                ordinal = 1;
            } else if ((aa - 3) % 10 == 0 && aa > 20) {
                divide = (aa - 3) / 10;
                ordinal = 2;
            }
            if (ordinal < 3 && aa > 20 && aa > divide * 10 && divide != 0) {

                if (ordinal == 0) {
                    ordinal_ranklist.add("" + divide + "1st");
                    // myViewHolder.rank.setText("" + divide + "1st");
                } else if (ordinal == 1) {
                    ordinal_ranklist.add("" + divide + "2nd");
                    // myViewHolder.rank.setText("" + divide + "2nd");
                } else if (ordinal == 2) {
                    ordinal_ranklist.add("" + divide + "3rd");
                    // myViewHolder.rank.setText("" + divide + "3rd");
                }
                ordinal = ordinal + 1;
                divide = 0;
            } else {
                // ordinal=0;
                if (aa == 1) {
                    ordinal_ranklist.add("1st");
                    // myViewHolder.rank.setText("1st");
                } else if (aa == 2) {
                    ordinal_ranklist.add("2nd");
                    // myViewHolder.rank.setText("2nd");
                } else if (aa == 3) {
                    ordinal_ranklist.add("3rd");
                    //  myViewHolder.rank.setText("3rd");
                } else {
                    ordinal_ranklist.add("" + aa + "th");
                }


            }
        }

        try {
            adapter = new last_adapter(Last_monthlist.this, originallist);
            list_item.setAdapter(adapter);
        } catch (Exception e) {

        }



        // search list
        try {
            for (int i = 0; i < list_name.size(); i++) {
                array_shared_install.add("1");
                String s1 = String.valueOf(list_name.get(i));
                String stri = s1.substring(0, 1);
                TextDrawable drawable = TextDrawable.builder()
                        .beginConfig()
                                // height in px
                        .endConfig()
                        .buildRound(stri, Color.parseColor("#1665c1"));


                url_image = "https://scontent.xx.fbcdn.net/v/t1.0-1/s200x200/375045_466002050142972_671155216_n.jpg?oh=af79baa2785248de3358ab36b949affd&oe=57EBF53A";
                WorldPopulation wp = new WorldPopulation(array_shared_install.get(i), originallist.get(i), list_name.get(i), list_number.get(i), ordinal_ranklist.get(i), drawable, url_image, arraylist_mood.get(i));
                // Binds all strings into an array
                world.add(wp);
            }

            arrayAdapter = new searchfilter_lastmonthlist(Last_monthlist.this, world);
            list_item1.setAdapter(arrayAdapter);
        } catch (Exception e) {

        }
        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                search_value=1;
                search_edit.setVisibility(View.VISIBLE);
                list_item1.setVisibility(View.VISIBLE);
                list_item.setVisibility(View.GONE);
                search_btn.setVisibility(View.INVISIBLE);
                search.setVisibility(View.INVISIBLE);
            }
        });

        search_edit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String text = search_edit.getText().toString().toLowerCase(Locale.getDefault());
                arrayAdapter.filter(text);
            }
        });

// if no friend list then exicute it
        try {
            a = list_name.size();
            Log.d("listnameee", String.valueOf(a));
            if (a == 0) {
                no_msg.setVisibility(View.VISIBLE);
                no_msg.setText("No " + friend);
                ranktext_color.setVisibility(View.INVISIBLE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        back_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                search_btn.setVisibility(View.VISIBLE);
                search.setVisibility(View.VISIBLE);
                InputMethodManager inputManager = (InputMethodManager)
                        getSystemService(Context.INPUT_METHOD_SERVICE);
                try {
                    Incoming_outgoing_calls.gps_sett=0;
                    inputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(),
                            InputMethodManager.HIDE_NOT_ALWAYS);
                } catch (Exception e) {

                }
                    if (search_value == 1) {
                        search_value = 0;
                        search_edit.setVisibility(View.GONE);
                        adapter = new last_adapter(Last_monthlist.this, originallist);
                        list_item.setAdapter(adapter);
                        list_item.setVisibility(View.VISIBLE);
                        list_item1.setVisibility(View.GONE);
                        search_edit.setText("");
                    } else {
                        Intent intent1 = new Intent(Last_monthlist.this, Incoming_outgoing_calls.class);
                        startActivity(intent1);
                        Last_monthlist.this.finish();
                    }


            }
        });


        list_item.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Log.d("",""+parent.getItemAtPosition(position));
                Intent intent=new Intent(Last_monthlist.this,Lastmonth_menu_friendz.class);
                intent.putExtra("name",list_name.get(position));
                intent.putExtra("number", list_number.get(position));
                intent.putExtra("friends",friend);
                startActivity(intent);

            }
        });

    }

    @Override
    public void initUI() {
        list_item = (ListView) findViewById(R.id.list_item);
        list_item1 = (ListView) findViewById(R.id.list_item1);
        back_layout = (LinearLayout) findViewById(R.id.back_layout);
        search = (LinearLayout) findViewById(R.id.search);
        search_edit = (EditText) findViewById(R.id.search_edit);
        friends = (TextView) findViewById(R.id.friends);
        no_msg = (TextView) findViewById(R.id.no_msg);
        search_btn = (ImageView) findViewById(R.id.search_btn);
        ranktext_color = (TextView) findViewById(R.id.ranktext_color);
        friends_list_appbar = (LinearLayout) findViewById(R.id.friends_list_appbar);
    }

    void json(String friend, List<String> list_number) {
        url = Base_url + "save_friends.php?type=" + URLEncoder.encode(friend) + "&user_id=" + pref_getvalue_str("user_id", "") + "&friends_list=" + URLEncoder.encode(list_number.toString().replace("[", "").replace("]", "").replace(", ", ",").trim());
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject jsonObject) {
                Log.d("listapi", jsonObject.toString());

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {


            }
        });
        AppController.getInstance().addToRequestQueue(jsonObjectRequest);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        // super.onBackPressed();
        search_btn.setVisibility(View.VISIBLE);
        search.setVisibility(View.VISIBLE);
        InputMethodManager inputManager = (InputMethodManager)
                getSystemService(Context.INPUT_METHOD_SERVICE);
        try {
            Incoming_outgoing_calls.gps_sett=0;
            inputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(),
                    InputMethodManager.HIDE_NOT_ALWAYS);
        } catch (Exception e) {

        }
            if (search_value == 1) {
                search_value = 0;
                search_edit.setVisibility(View.GONE);
                adapter = new last_adapter(Last_monthlist.this, originallist);
                list_item.setAdapter(adapter);
                list_item.setVisibility(View.VISIBLE);
                list_item1.setVisibility(View.GONE);
                search_edit.setText("");

            } else {
                Intent intent = new Intent(Last_monthlist.this, Incoming_outgoing_calls.class);
                startActivity(intent);
                Last_monthlist.this.finish();
            }



    }

    class last_adapter extends BaseAdapter {
        Activity activity;
        int divide = 0;
        ArrayList<String> arraylist = new ArrayList();
        ArrayList list = new ArrayList();

        public List<String> getList() {
            return Last_monthlist.list_name;

        }

        public last_adapter(Activity activity, ArrayList<String> arraylist) {

            this.activity = activity;
            this.arraylist = arraylist;
        }

        @Override
        public int getCount() {

            return arraylist.size();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = LayoutInflater.from(activity);
            convertView = inflater.inflate(R.layout.row_best, null);

            TextView textView = (TextView) convertView.findViewById(R.id.name);
            TextView rank = (TextView) convertView.findViewById(R.id.rank);

            ImageView menuimage = (ImageView) convertView.findViewById(R.id.menuimage);
            ImageView emotions = (ImageView) convertView.findViewById(R.id.emotions);
            LinearLayout rank_layout = (LinearLayout) convertView.findViewById(R.id.rank_layout);
            rank_layout.setBackgroundColor(getResources().getColor(color));
            emotions.setVisibility(View.GONE);

            rank.setText(ordinal_ranklist.get(position));
            rank.setTextColor(getResources().getColor(textrank_color));

            try {
                if (getList().get(position).equals("No Name")) {
                    textView.setText(Last_monthlist.list_number.get(position));
                    list.add(Last_monthlist.list_number.get(position));
                    menuimage.setImageResource(R.drawable.user);
                } else {
                    String s1 = String.valueOf(getList().get(position));
                    String stri = s1.substring(0, 1);
                    TextDrawable drawable = TextDrawable.builder()
                            .beginConfig()
                                    // height in px
                            .endConfig()
                            .buildRound(stri, Color.parseColor("#1665c1"));
                    menuimage.setImageDrawable(drawable);
                    textView.setText(getList().get(position));
                    list.add(getList().get(position));
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

            return convertView;
        }

    }
}