package com.buddygap.main.utils;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.os.Vibrator;
import android.support.v7.app.NotificationCompat;
import android.widget.Toast;

import com.buddygap.main.R;

/**
 * Created by admin on 15-07-2016.
 */
public class broadcast extends BroadcastReceiver {
    NotificationManager notificationManager;
    @Override
    public void onReceive(Context context, Intent intent) {

            Toast.makeText(context, "Intent Detected.", Toast.LENGTH_LONG).show();
        handleMessage(context);
            Vibrator vibrator = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);
            vibrator.vibrate(2000);
    }


    private void handleMessage(Context mContext) {

        long when = System.currentTimeMillis();
        int icon = R.drawable.app_icon;

        try {


            notificationManager = (NotificationManager) mContext
                    .getSystemService(Context.NOTIFICATION_SERVICE);
            PendingIntent contentIntent = null;

            Intent gotoIntent = new Intent();
            gotoIntent.setClassName(mContext, "com.buddygap.main.utils.Notification_feel");//Start activity when user taps on notification.
            contentIntent = PendingIntent.getActivity(mContext,
                    (int) (Math.random() * 100), gotoIntent,
                    PendingIntent.FLAG_UPDATE_CURRENT);
            NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(
                    mContext);
            Notification notification = mBuilder.setSmallIcon(icon).setTicker("Indu Demo").setWhen(0)
                    .setAutoCancel(true)
                    .setContentTitle("Moody")
                    .setStyle(new NotificationCompat.BigTextStyle().bigText("How was you day today"))
                    .setContentIntent(contentIntent)
                    .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))

                    .setContentText("How was you day today").build();
            //    .setStyle(notiStyle).build();


            notification.flags = Notification.FLAG_AUTO_CANCEL;
            // count++;
            notificationManager.notify(9001, notification);//This will generate seperate notification each time server sends.

        } catch (Throwable e) {
            e.printStackTrace();
        }
       /* }
        else{
            Log.d("eee","error");
        }*/
    }

}
