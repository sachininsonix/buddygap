package com.buddygap.main.utils;

/**
 * Created by insonix on 20/4/16.
 */

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.util.Log;
import android.widget.Button;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.buddygap.main.R;
import com.buddygap.main.iconstant.IConstant;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Timer;
import java.util.TimerTask;


/**
 * Created by insonix on 20/4/16.
 */
public class LocationService extends Service implements IConstant
{


    public SharedPreferences sharedPreferences;

    String url;
    private static final long MINIMUM_DISTANCE_CHANGE_FOR_UPDATES = 1000; // in
    // Meters
    private static final long MINIMUM_TIME_BETWEEN_UPDATES = 1000; // in
    // Milliseconds
    protected LocationManager locationManager;

    private static Timer timer = new Timer();
    private Context ctx;
    @Override
    public IBinder onBind(Intent arg0) {
        // TODO Auto-generated method stub
        return null;
    }
    public void onCreate()
    {
        super.onCreate();
        ctx = this;
        startService();
        sharedPreferences=getSharedPreferences("first_name", Context.MODE_PRIVATE);
    }
    private void startService()
    {
        timer.scheduleAtFixedRate(new mainTask(), 0, 1800000);
    }

    private class mainTask extends TimerTask
    {
        public void run()
        {
            toastHandler.sendEmptyMessage(0);
        }
    }

    public void onDestroy()
    {
        super.onDestroy();
       // Toast.makeText(this, "Service Stopped ...", Toast.LENGTH_SHORT).show();
    }

    private final Handler toastHandler = new Handler()
    {
        @Override
        public void handleMessage(Message msg)
        {
//            Intent i = new Intent(LocationService.this, HomeScreen.class);
//            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//
//            startActivity(i);


       //     Toast.makeText(LocationService.this, "GPS Service created ...", Toast.LENGTH_SHORT).show();
            sharedPreferences=getSharedPreferences("first_name", Context.MODE_PRIVATE);
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,
                MINIMUM_TIME_BETWEEN_UPDATES,
                MINIMUM_DISTANCE_CHANGE_FOR_UPDATES, new MyLocationListener());
            Location location = locationManager
                    .getLastKnownLocation(LocationManager.GPS_PROVIDER);
            Log.d("lpcationn", String.valueOf(location));
//        float latitude = (float) location.getLatitude();
//        float longitude = (float) location.getLongitude();
//        String uniqueId = getIMEI();
        if (location != null ) {
            String message = String.format(
                    "Current Location \n Longitude: %1$s \n Latitude: %2$s",
                    location.getLongitude(), location.getLatitude());

        //    Toast.makeText(LocationService.this, message, Toast.LENGTH_SHORT).show();
        }
        }
    };


    private class MyLocationListener implements LocationListener
    {
        public void onLocationChanged(Location location) {
Log.d("lonn", String.valueOf(location.getLatitude()));
            String message = String.format(
                    "New Location "+ location.getSpeed() +" \n Longitude: %1$s  \n Latitude: %2$s",
                    location.getLongitude(), location.getLatitude());
            if (!sharedPreferences.getString("user_id", "").equals("")) {
                url = Base_url + "save_lat_lon.php?user_id=" + sharedPreferences.getString("user_id", "") + "&lat=" + location.getLatitude() + "&lon=" + location.getLongitude();
                json_location(url);
            }
//            Toast.makeText(LocationService.this, message, Toast.LENGTH_SHORT)
//                    .show();
// web service
        }

        public void onStatusChanged(String s, int i, Bundle b) {
//            Toast.makeText(LocationService.this, "Provider status changed",
//                    Toast.LENGTH_SHORT).show();
        }

        public void onProviderDisabled(String s) {
//            Toast.makeText(LocationService.this,
//                    "Provider disabled by the user. GPS turned off",
//                    Toast.LENGTH_SHORT).show();
        }

        public void onProviderEnabled(String s) {
//            Toast.makeText(LocationService.this,
//                    "Provider enabled by the user. GPS turned on",
//                    Toast.LENGTH_SHORT).show();
        }
    }


    public  void json_location(String url){
        //String url="http://api.openweathermap.org/data/2.5/weather?lat=-33.87&lon=151.21&APPID=ea574594b9d36ab688642d5fbeab847e";
Log.d("urlll",url);
        JsonObjectRequest jsonObjectRequest=new JsonObjectRequest(url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject jsonObject) {
                try {
                    String json=jsonObject.getString("status");
                    Log.d("status",json);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                Log.e("msg",jsonObject.toString());
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.e("msg","error");
            }
        });
        AppController.getInstance().addToRequestQueue(jsonObjectRequest);
    }




}