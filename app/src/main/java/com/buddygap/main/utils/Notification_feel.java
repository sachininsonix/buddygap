package com.buddygap.main.utils;

import android.app.AlertDialog;
import android.app.NotificationManager;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.buddygap.main.R;
import com.buddygap.main.home.Incoming_outgoing_calls;
import com.buddygap.main.iconstant.IConstant;
import com.buddygap.main.login.Facebook_login;
import com.buddygap.main.navigation_menu.Multilanguage;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by admin on 14-07-2016.
 */
public class Notification_feel extends BaseActivity implements IConstant {

    LinearLayout smile5,smile4,smile3,smile2,smile,maincontainer,editcontainer,edit_lay;
    Button send_smile,submit_moodedit;
    View smile_view5,smile_view4,smile_view3,smile_view2,smile_view;
    TextView mood_text5,mood_text4,mood_text3,mood_text2,mood_text1,friendstext;
    String  text_mood="Excellent",backsceencheck;
    NotificationManager notificationManager;
    ImageView back_btn;
    EditText mood_edittext1,mood_edittext2,mood_edittext3,mood_edittext4,mood_edittext5;
int editor_lay=0;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feel_notification);
        initUI();
        try {
            backsceencheck = getIntent().getStringExtra("frommulti");
            mood_text1.setText(pref_getvalue_str("excelent", ""));
            mood_text2.setText(pref_getvalue_str("good", ""));
            mood_text3.setText(pref_getvalue_str("average", ""));
            mood_text4.setText(pref_getvalue_str("sad", ""));
            mood_text5.setText(pref_getvalue_str("frustrated", ""));
            friendstext.setText(pref_getvalue_str("feels_today", ""));

//            if((backsceencheck.equals("multilanguage")) || (backsceencheck.equals("incoming_outcoming"))){
//              back_btn.setVisibility(View.VISIBLE);
                back_btn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        onBackPressed();
                    }
                });
//            }
          
        } catch (Exception e) {

        }



        smile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              smile_view5.setVisibility(View.GONE);
                smile_view4.setVisibility(View.GONE);
                smile_view3.setVisibility(View.GONE);
                smile_view2.setVisibility(View.GONE);
                smile_view.setVisibility(View.VISIBLE);
                text_mood="Excellent";


            }
        });

        smile2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                smile_view5.setVisibility(View.GONE);
                smile_view4.setVisibility(View.GONE);
                smile_view3.setVisibility(View.GONE);
                smile_view.setVisibility(View.GONE);
                smile_view2.setVisibility(View.VISIBLE);
                text_mood="Good";



            }
        });

        smile3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                smile_view5.setVisibility(View.GONE);
                smile_view4.setVisibility(View.GONE);
                smile_view.setVisibility(View.GONE);
                smile_view2.setVisibility(View.GONE);
                smile_view3.setVisibility(View.VISIBLE);
                text_mood="Average";

            }
        });

        smile4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                smile_view5.setVisibility(View.GONE);
                smile_view.setVisibility(View.GONE);
                smile_view3.setVisibility(View.GONE);
                smile_view2.setVisibility(View.GONE);
                smile_view4.setVisibility(View.VISIBLE);
                text_mood="Sad";


            }
        });

        smile5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                smile_view.setVisibility(View.GONE);
                smile_view4.setVisibility(View.GONE);
                smile_view3.setVisibility(View.GONE);
                smile_view2.setVisibility(View.GONE);
                smile_view5.setVisibility(View.VISIBLE);

                text_mood="Frustrated";

            }
        });
        edit_lay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                maincontainer.setVisibility(View.GONE);
                editcontainer.setVisibility(View.VISIBLE);
                editor_lay=1;
                friendstext.setText("Customize feelings");
                mood_edittext1.setText(pref_getvalue_str("excelent", ""));
                mood_edittext2.setText(pref_getvalue_str("good", ""));
                mood_edittext3.setText(pref_getvalue_str("average", ""));
                mood_edittext4.setText(pref_getvalue_str("sad", ""));
                mood_edittext5.setText(pref_getvalue_str("frustrated", ""));
            }
        });



        send_smile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isNetworkAvailable() == false) {
                    alert_dialog();
                }
                else if  (pref_getvalue_str("user_id", "").equals("")) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(Notification_feel.this);

                    builder.setTitle("Confirm");
                    builder.setMessage("To Update your Feels for today You have to login first.Are you sure you want to proceed?");

                    builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {

                        public void onClick(DialogInterface dialog, int which) {
                            // Do nothing but close the dialog
                            Intent intent = new Intent(Notification_feel.this, Facebook_login.class);
                            startActivity(intent);
                            finish();
                        }

                    });
                    builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                    AlertDialog alert = builder.create();
                    alert.show();
                }
                else {
                    mood_save(text_mood);
                }
            }
        });

        submit_moodedit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isNetworkAvailable() == false) {
                    alert_dialog();
                }
                else if(pref_getvalue_str("user_id", "").equals("")) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(Notification_feel.this);

                    builder.setTitle("Confirm");
                    builder.setMessage("To Update your Feels for today You have to login first.Are you sure you want to proceed?");

                    builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {

                        public void onClick(DialogInterface dialog, int which) {
                            // Do nothing but close the dialog
                            Intent intent = new Intent(Notification_feel.this, Facebook_login.class);
                            startActivity(intent);
                            finish();
                        }

                    });
                    builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                    AlertDialog alert = builder.create();
                    alert.show();
                }
                else {
                    edit_textUpdate();
                }
            }
        });
    }
    @Override
    public void initUI() {
        smile5=(LinearLayout)findViewById(R.id.smile5);
        smile4=(LinearLayout)findViewById(R.id.smile4);
        smile3=(LinearLayout)findViewById(R.id.smile3);
        smile2=(LinearLayout)findViewById(R.id.smile2);
        smile=(LinearLayout)findViewById(R.id.smile);
        editcontainer=(LinearLayout)findViewById(R.id.editcontainer);
        maincontainer=(LinearLayout)findViewById(R.id.maincontainer);
        edit_lay=(LinearLayout)findViewById(R.id.edit_lay);
        send_smile=(Button)findViewById(R.id.send_smile);
        back_btn=(ImageView)findViewById(R.id.back_btn);
        smile_view5=findViewById(R.id.smile_view5);
        smile_view4=findViewById(R.id.smile_view4);
        smile_view3=findViewById(R.id.smile_view3);
        smile_view2=findViewById(R.id.smile_view2);
        smile_view=findViewById(R.id.smile_view);
        mood_text1=(TextView)findViewById(R.id.mood_text1);
        mood_text2=(TextView)findViewById(R.id.mood_text2);
        mood_text3=(TextView)findViewById(R.id.mood_text3);
        mood_text4=(TextView)findViewById(R.id.mood_text4);
        mood_text5=(TextView)findViewById(R.id.mood_text5);
        friendstext=(TextView)findViewById(R.id.friendstext);
        mood_edittext1=(EditText)findViewById(R.id.mood_edittext1);
        mood_edittext2=(EditText)findViewById(R.id.mood_edittext2);
        mood_edittext3=(EditText)findViewById(R.id.mood_edittext3);
        mood_edittext4=(EditText)findViewById(R.id.mood_edittext4);
        mood_edittext5=(EditText)findViewById(R.id.mood_edittext5);
        submit_moodedit=(Button)findViewById(R.id.submit_moodedit);
    }
    @Override
    public void onBackPressed() {
//        Intent intent=new Intent(Notification_feel.this, Incoming_outgoing_calls.class);
//        startActivity(intent);


        if(backsceencheck.equals("multilanguage")){
            Intent intent = new Intent(Notification_feel.this, Multilanguage.class);
            startActivity(intent);
            finish();
        }
        else if(editor_lay == 1){
            Intent intent = new Intent(Notification_feel.this, Notification_feel.class);
            intent.putExtra("frommulti","");
            startActivity(intent);
            overridePendingTransition(0, 0);
            finish();

        }
        else if(backsceencheck.equals("incoming_outcoming")){
            Intent intent = new Intent(Notification_feel.this, Incoming_outgoing_calls.class);
            startActivity(intent);
            finish();
        }


            else{
            Intent intent = new Intent(Notification_feel.this, Incoming_outgoing_calls.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
            finish();
            }

    }

// mood api


    void mood_save(String text){
        Log.d("mood",text);
        progress_show();
    String url=Base_url+"save_mood.php?id="+pref_getvalue_str("user_id","")+"&mood="+text;
       Log.d("url_mood", url);
        JsonObjectRequest jsonObjectRequest=new JsonObjectRequest(url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject jsonObject) {
                try {
                    String status=jsonObject.getString("status");
                    String message=jsonObject.getString("message");
                    if (status.equals("true")){
                        Log.d("mood_sattaus", status);
                        Toast.makeText(Notification_feel.this, "Your feels for day updated Sucessfully", Toast.LENGTH_LONG).show();
                      if(backsceencheck.equals("multilanguage")){
                          Intent intent = new Intent(Notification_feel.this, Multilanguage.class);
                          startActivity(intent);
                          finish();
                      }
                      else if(backsceencheck.equals("incoming_outcoming")){
                          Intent intent = new Intent(Notification_feel.this, Incoming_outgoing_calls.class);
                          startActivity(intent);
                          finish();
                      }
                        else {
                          Intent intent = new Intent(Notification_feel.this, Incoming_outgoing_calls.class);
                          intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_CLEAR_TASK);
                          startActivity(intent);
                          finish();
                      }
                      }
                    else if (status.equals("false")){
                        if (message.equals("Already submitted your feeling")){
                            Toast.makeText(Notification_feel.this, message, Toast.LENGTH_LONG).show();
                            if(backsceencheck.equals("multilanguage")){
                                Intent intent = new Intent(Notification_feel.this, Multilanguage.class);
                                startActivity(intent);
                                finish();
                            }
                            else if(backsceencheck.equals("incoming_outcoming")){
                                Intent intent = new Intent(Notification_feel.this, Incoming_outgoing_calls.class);
                                startActivity(intent);
                                finish();
                            }
                            else {
                                Intent intent = new Intent(Notification_feel.this, Incoming_outgoing_calls.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                                finish();
                            }
                        }
                        else{

                        }

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                progressDialog.dismiss();

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Toast.makeText(Notification_feel.this, "Slow Internet Connection", Toast.LENGTH_LONG).show();
            }
        });
AppController.getInstance().addToRequestQueue(jsonObjectRequest);

    }
    void edit_textUpdate(){
        progress_show();
        pref_setvalue_str("excelent", mood_edittext1.getText().toString().trim());
        pref_setvalue_str("good", mood_edittext2.getText().toString().trim());
        pref_setvalue_str("average", mood_edittext3.getText().toString().trim());
        pref_setvalue_str("sad", mood_edittext4.getText().toString().trim());
        pref_setvalue_str("frustrated", mood_edittext5.getText().toString().trim());
        progress_dismiss();
        maincontainer.setVisibility(View.VISIBLE);
        editcontainer.setVisibility(View.GONE);
        editor_lay=0;
        Intent intent = new Intent(Notification_feel.this, Notification_feel.class);
        intent.putExtra("frommulti","");
        startActivity(intent);
        overridePendingTransition(0,0);
        finish();
    }




}
