package com.buddygap.main.utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.preference.PreferenceManager;

import java.util.Set;

/**
 * Created by admin on 28-06-2016.
 */
public  abstract class BaseActivity extends Activity {
    int gps_set=0;
   public ProgressDialog progressDialog;
   public SharedPreferences sharedPreferences;
    public SharedPreferences.Editor editor;
    AlertDialog.Builder alertDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        progressDialog=new ProgressDialog(BaseActivity.this);

        sharedPreferences= PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        sharedPreferences=getSharedPreferences("first_name", Context.MODE_PRIVATE);
        editor=sharedPreferences.edit();
        alertDialog=new AlertDialog.Builder(this);

    }
    public int gps_Show(){
    gps_set=0;
        return gps_set;
}
    public int gps_hide(){
        gps_set=1;
        return gps_set;
    }

    public abstract void initUI();
    public  boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager = (ConnectivityManager) BaseActivity.this.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public void progress_show(){
        progressDialog.setMessage("Loading...");
        progressDialog.setCancelable(false);
        progressDialog.show();
    }
    public void progress_show_sending(){
        progressDialog.setMessage("Sending...");
        progressDialog.setCancelable(false);
        progressDialog.show();
    }
    public void progress_dismiss(){
        progressDialog.dismiss();
    }

    public  void pref_setvalue_str(String key,String value){
        editor.putString(key, value);
        editor.commit();
    }
    public  String pref_getvalue_str(String key,String value){
     String ss=   sharedPreferences.getString(key,"");
        return ss;

    }
    public  void pref_setvalue_int(String key,Integer value){
        editor.putInt(key,value);
        editor.commit();
    }
    public  int pref_getvalue_int(String key){
        int ss=   sharedPreferences.getInt(key,0);
        return ss;

    }

    public void pref_setvalue_strset(String key,Set value){
        editor.putStringSet(key,value);
        editor.commit();

    }
    public Set pref_getvalue_strset(String key,Set value){
      Set sets=  sharedPreferences.getStringSet(key,value);
        return  sets;
        //editor.commit();

    }














public void alert_dialog(){
    alertDialog.setMessage("Not able to connect to BuddyGap. Please check your network connection and try again.")
            .setTitle("Error")
            .setCancelable(false)
            .setNegativeButton("Ok", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    //  Action for 'NO' Button
                    dialog.cancel();
                }
            });
    AlertDialog alert = alertDialog.create();
    alert.show();
}


}
