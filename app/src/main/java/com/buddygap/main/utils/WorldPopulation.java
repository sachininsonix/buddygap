package com.buddygap.main.utils;

/**
 * Created by admin on 22-05-2016.
 */
public class WorldPopulation {
    String name;
    String name_no;
    String number;
    String rank;
    String install;
    TextDrawable img;
    String url,mood;
//    WorldPopulation(String name,String rank,int img){
//    this.name=name;
//    this.rank=rank;
//    this.img=img;
//}

    public WorldPopulation(String install, String name_no, String name, String number, String rank, TextDrawable drawable, String url_image, String mood) {
        this.name=name;
        this.rank=rank;
        this.img=drawable;
        this.name_no=name_no;
        this.number=number;
        this.install=install;
        url=url_image;
        this.mood=mood;
    }


    public String getName() {
        return name;
    }
    public String getName_no() {
        return name_no;
    }

    public String getNumber() {
        return number;
    }

//    public void setName(String name) {
//        this.name = name;
//    }

    public String getRank() {
        return rank;
    }
    public String getMood() {
        return mood;
    }
    public String getinstall() {
        return install;
    }

//    public void setRank(int rank) {
//        this.rank = rank;
//    }

    public TextDrawable getImg() {
        return img;
    }

    public String geturl() {
        return url;
    }
}
