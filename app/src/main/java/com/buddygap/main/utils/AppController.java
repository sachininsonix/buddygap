package com.buddygap.main.utils;

import android.app.Application;
import android.text.TextUtils;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;
import com.crashlytics.android.Crashlytics;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;

import java.io.File;

import io.fabric.sdk.android.Fabric;


public class AppController extends Application {
	private static GoogleAnalytics analytics;
	/**
	 * The default app tracker. The field is from onCreate callback when the application is
	 * initially created.
	 */
	private static Tracker tracker;
	public static final String TAG = AppController.class
			.getSimpleName();

	private RequestQueue mRequestQueue;
	private ImageLoader mImageLoader;

	private static AppController mInstance;
	public static GoogleAnalytics analytics() {
		return analytics;
	}
	/**
	 * The default app tracker. If this method returns null you forgot to either set
	 * android:name="&lt;this.class.name&gt;" attribute on your application element in
	 * AndroidManifest.xml or you are not setting this.tracker field in onCreate method override.
	 */
	public static Tracker tracker() {
		return tracker;
	}
	@Override
	public void onCreate() {
		super.onCreate();
		Fabric.with(this, new Crashlytics());
	//	Fabric.with(this, new Crashlytics());
		mInstance = this;
		analytics = GoogleAnalytics.getInstance(this);
// TODO: Replace the tracker-id with your app one from https://www.google.com/analytics/web/
		tracker = analytics.newTracker("UA-80051865-1");
// Provide unhandled exceptions reports. Do that first after creating the tracker
		tracker.enableExceptionReporting(true);
// Enable Remarketing, Demographics & Interests reports
// https://developers.google.com/analytics/devguides/collection/android/display-features
		tracker.enableAdvertisingIdCollection(true);
// Enable automatic activity tracking for your app
		tracker.enableAutoActivityTracking(true);
	}

	public static synchronized AppController getInstance() {
		return mInstance;
	}

	public RequestQueue getRequestQueue() {
		if (mRequestQueue == null) {
			mRequestQueue = Volley.newRequestQueue(getApplicationContext());
		}

		return mRequestQueue;
	}
	public void clearApplicationData() {

		File cacheDirectory = getCacheDir();
		File applicationDirectory = new File(cacheDirectory.getParent());
		if (applicationDirectory.exists()) {

			String[] fileNames = applicationDirectory.list();

			for (String fileName : fileNames) {

				if (!fileName.equals("lib")) {

					deleteFile(new File(applicationDirectory, fileName));

				}

			}

		}

	}

	public static boolean deleteFile(File file) {

		boolean deletedAll = true;

		if (file != null) {

			if (file.isDirectory()) {

				String[] children = file.list();

				for (int i = 0; i < children.length; i++) {

					deletedAll = deleteFile(new File(file, children[i])) && deletedAll;

				}

			} else {

				deletedAll = file.delete();

			}

		}

		return deletedAll;

	}




	public <T> void addToRequestQueue(Request<T> req, String tag) {
		// set the default tag if tag is empty
		req.setTag(TextUtils.isEmpty(tag) ? TAG : tag);
		getRequestQueue().add(req);
	}

	public <T> void addToRequestQueue(Request<T> req) {
		req.setTag(TAG);
		getRequestQueue().add(req);
	}

	public void cancelPendingRequests(Object tag) {
		if (mRequestQueue != null) {
			mRequestQueue.cancelAll(tag);
		}
	}
}
