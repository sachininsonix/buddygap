package com.buddygap.main.utils;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.buddygap.main.Internet_connection.Try_again;
import com.buddygap.main.R;
import com.buddygap.main.home.Incoming_outgoing_calls;
import com.buddygap.main.login.Facebook_login;
import com.buddygap.main.login.Gplus;
import com.buddygap.main.login.Login_process;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.gcm.GoogleCloudMessaging;

import java.io.IOException;



public class Splash_screen extends BaseActivity {

    String login;
    private String regid = null;
    protected String SENDER_ID="512682975514";
    private GoogleCloudMessaging gcm=null;
    private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        try{
            isFirstTimebest();
        login=pref_getvalue_str("login","");
        }
        catch (Exception e) {

        }


        if (Build.VERSION.SDK_INT >= 23) {
            if (

                    ContextCompat.checkSelfPermission(Splash_screen.this, android.Manifest.permission.INTERNET)!= PackageManager.PERMISSION_GRANTED

                    ) {
                ActivityCompat.requestPermissions(Splash_screen.this,
                        new String[]{

                                android.Manifest.permission.INTERNET
                        },
                        1);

            }


        }



        if (isNetworkAvailable() == false) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        Thread.sleep(300);
                        if (pref_getvalue_str("user_id","").equals("")) {
                            Intent intent = new Intent(Splash_screen.this, Try_again.class);
                            startActivity(intent);
                            finish();
                        }
                        else if (pref_getvalue_str("phone","").equals("")) {
                            Intent intent = new Intent(Splash_screen.this, Try_again.class);
                            startActivity(intent);
                            finish();
                        } else if((!pref_getvalue_str("user_id","").equals("")) && (!pref_getvalue_str("phone","").equals("")))  {
                            Intent intent = new Intent(Splash_screen.this, Incoming_outgoing_calls.class);
                            startActivity(intent);
                            finish();
                        }
                        else {
                            Intent intent = new Intent(Splash_screen.this, Try_again.class);
                            startActivity(intent);
                            finish();
                        }

                    } catch (Exception e) {

                    }
                }
            }).start();
        }else {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        Thread.sleep(3000);
                        if (pref_getvalue_str("user_id","").equals("")) {
                            Intent intent = new Intent(Splash_screen.this, Facebook_login.class);
                            startActivity(intent);
                            finish();
                        }
                        else if (pref_getvalue_str("phone","").equals("")) {
                            Intent intent = new Intent(Splash_screen.this, Login_process.class);
                            startActivity(intent);
                            finish();
                        } else if((!pref_getvalue_str("user_id","").equals("")) && (!pref_getvalue_str("phone","").equals("")) && (!pref_getvalue_str("user_id","").equals("null")) && (!pref_getvalue_str("phone","").equals("")))  {

                                Intent intent = new Intent(Splash_screen.this, Incoming_outgoing_calls.class);
                                startActivity(intent);
                                finish();

                        }
                        else {
                            Intent intent = new Intent(Splash_screen.this, Facebook_login.class);
                            startActivity(intent);
                            finish();
                        }

                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                }
            }).start();
        }
        if(checkPlayServices()){
            registerInBackground();
        }else{
        }
    }

    @Override
    public void initUI() {

    }
    public boolean isFirstTimebest()
    {

        SharedPreferences preferences = getPreferences(MODE_PRIVATE);
        boolean ranBeforebest = preferences.getBoolean("RanBeforebest", false);
        if (!ranBeforebest) {
            // first time
            sharedPreferences.edit().clear().commit();
            SharedPreferences.Editor editor = preferences.edit();
            editor.putBoolean("RanBeforebest", true);
            editor.commit();
        }
        return !ranBeforebest;
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_splash_screen, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
    @Override protected void onResume()
    {
        super.onResume();
        checkPlayServices();
    }
    private boolean checkPlayServices() {
        int resultcode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(getApplicationContext());
        if (resultcode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultcode)) {
                GooglePlayServicesUtil.getErrorDialog(resultcode, Splash_screen.this, PLAY_SERVICES_RESOLUTION_REQUEST).show();
            } else {

            }
            return false;
        }
        return true;
    }
    public void  registerInBackground(){
        new AsyncTask<Void,Void,String>(){

            protected String doInBackground(Void... params) {
                String msg = "";
                try {
                    if (gcm == null) {
                        gcm = GoogleCloudMessaging
                                .getInstance(getApplicationContext());
                    }
                    regid = gcm.register(SENDER_ID);
                    msg = "Registration ID :" + regid;
                    Log.i("Registration id", regid);
                    pref_setvalue_str("reg_id", regid);
                    editor.putString("reg_id", regid);
                    editor.commit();

                } catch (IOException ex) {
                    msg = "Error :" + ex.getMessage();
                }
                return msg;
            }

            @Override
            protected void onPostExecute(String msg) {
                if (!TextUtils.isEmpty(regid)) {
                    // storeRegIdinSharedPref(getActivity(), regId, emailID);
						/*
						 * Toast.makeText( con,
						 * "Registered with GCM Server successfully.\n\n" + msg,
						 * Toast.LENGTH_SHORT).show();
						 */
                } else {
						/*
						 * Toast.makeText( con,
						 * "Reg ID Creation Failed.\n\nEither you haven't enabled Internet or GCM server is busy right now. Make sure you enabled Internet and try registering again after some time."
						 * + msg, Toast.LENGTH_LONG).show();
						 */
                }
            }
        }.execute(null, null, null);
    }

}
