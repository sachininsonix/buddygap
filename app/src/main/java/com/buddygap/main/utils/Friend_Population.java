package com.buddygap.main.utils;

/**
 * Created by admin on 25-07-2016.
 */
public class Friend_Population {


    String name;
    String friend;
    String view_rank;
    String view_list;
    String name_no;
    String number;
    String rank;
    String install;
    TextDrawable img;
    String url,mood;
    int text,text_background;
    Boolean aBoolean;
//    WorldPopulation(String name,String rank,int img){
//    this.name=name;
//    this.rank=rank;
//    this.img=img;
//}

    public Friend_Population(String install, String name_no, String name, String number, String rank, TextDrawable drawable, String url_image, int text, int text_background, Boolean aBoolean, String view_rank, String view_list, String friend_list, String mood) {
        this.name=name;
        this.rank=rank;
        this.img=drawable;
        this.name_no=name_no;
        this.number=number;
        this.install=install;
        url=url_image;
        this.text=text;
        this.text_background=text_background;
        this.aBoolean=aBoolean;
        this.friend=friend_list;
        this.view_list=view_list;
        this.view_rank=view_rank;
        this.mood=mood;
    }


public int getText1(){
    return text;
}
    public  boolean getaBoolean(){
        return aBoolean;
    }
    public int getText_background(){
        return text_background;
    }
    public String getName() {
        return name;
    }
    public String getMood() {
        return mood;
    }
    public String getFriend_list() {
        return friend;
    }
    public String getViewlist() {
        return view_list;
    }
    public String getviewrank() {
        return view_rank;
    }





    public String getName_no() {
        return name_no;
    }

    public String getNumber() {
        return number;
    }

//    public void setName(String name) {
//        this.name = name;
//    }

    public String getRank() {
        return rank;
    }
    public String getinstall() {
        return install;
    }

//    public void setRank(int rank) {
//        this.rank = rank;
//    }

    public TextDrawable getImg() {
        return img;
    }

    public String geturl() {
        return url;
    }
}
