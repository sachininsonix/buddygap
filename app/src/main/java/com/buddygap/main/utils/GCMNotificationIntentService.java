package com.buddygap.main.utils;

import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.util.DisplayMetrics;
import android.util.Log;
import android.widget.Toast;

import com.buddygap.main.R;
import com.buddygap.main.home.Incoming_outgoing_calls;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Iterator;
import java.util.Random;
import java.util.Set;

public class GCMNotificationIntentService extends IntentService {
    // Sets an ID for the notification, so it can be updated
    //static final String MSG_KEY = "m";

    static float multiplier;
    //public static final int notifyID = 9001;
    NotificationCompat.Builder builder;
    SharedPreferences preferences;
    SharedPreferences.Editor editor;
    String getclass, newString;
    ImageLoader imageLoader;
    DisplayImageOptions options;
    Bitmap remote_picture = null, resized;

    public GCMNotificationIntentService() {
        super("GcmIntentService");

    }

    public static float getImageFactor(Resources r) {
        DisplayMetrics metrics = r.getDisplayMetrics();
        multiplier = metrics.density / 2f;
        return multiplier;
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Bundle extras = intent.getExtras();
        //extras.get("message");
        GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(this);

        String messageType = gcm.getMessageType(intent);


        if (!extras.isEmpty()) {
            if (GoogleCloudMessaging.MESSAGE_TYPE_SEND_ERROR
                    .equals(messageType)) {
                sendNotification("Send error: " + extras.toString(), "" + extras.toString(), "" + extras.toString(), "" + extras.toString(), "" + extras.toString(), "" + extras.toString());
            } else if (GoogleCloudMessaging.MESSAGE_TYPE_DELETED
                    .equals(messageType)) {
                sendNotification("Deleted messages on server: "
                        + extras.toString(), "" + extras.toString(), "" + extras.toString(), "" + extras.toString(), "" + extras.toString(), "" + extras.toString());
            } else if (GoogleCloudMessaging.MESSAGE_TYPE_MESSAGE
                    .equals(messageType)) {
//				if(preferences.getBoolean("checked",true)) {
                sendNotification(" "
                        + extras.get("title"), "" + extras.get("rank"), "" + extras.get("phone"), "" + extras.get("type"), "" + extras.get("image_url"), "" + extras.get("message"));
//				}else{
//
//				}
                Set<String> sss = extras.keySet();
                Iterator it = sss.iterator();
                while (it.hasNext()) {
                    Log.d("iteration", "it" + it.next());


                }
            }
        }
        GcmBroadcastReceiver.completeWakefulIntent(intent);
    }

    private void sendNotification(String title, String rank, String phone, String type, String image_url, String message) {
//			editor = preferences.edit();
        PendingIntent resultPendingIntent;
        Intent resultIntent;
        Random random = new Random();
        int m = random.nextInt(9999 - 1000) + 1000;
//
        Log.d("msg:", "msg" + title);
        Log.d("name:", "rank" + rank);
        Log.d("title:", "phone" + phone);


        if (type.equals("rank")) {

            resultIntent = new Intent(this, Incoming_outgoing_calls.class);
            Toast.makeText(getApplicationContext(), title, Toast.LENGTH_LONG).show();
            resultIntent.setAction(Intent.ACTION_MAIN);
            resultIntent.addCategory(Intent.CATEGORY_LAUNCHER);
            resultIntent.putExtra("title", title);
//		TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
            resultIntent.putExtra("rank", rank);
//		resultIntent.putExtra("title", title);
            resultIntent.putExtra("phone", phone);
//			resultIntent.putExtra("promocode", promocode);
//




//
            NotificationCompat.Builder mNotifyBuilder;
            NotificationManager mNotificationManager;

            mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            resultIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |
                    Intent.FLAG_ACTIVITY_NEW_TASK |Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT);
//            resultIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            resultPendingIntent = PendingIntent.getActivity(this, m,
                    resultIntent, PendingIntent.FLAG_UPDATE_CURRENT);
            mNotifyBuilder = new NotificationCompat.Builder(this)
                    .setSmallIcon(R.drawable.app_icon).setContentTitle(title);
            mNotifyBuilder.setContentIntent(resultPendingIntent);
            // Set pending intent
            // mNotifyBuilder.setContentIntent(resultPendingIntent);
            // Set Vibrate, Sound and Light
            int defaults = 0;
            defaults = defaults | Notification.DEFAULT_LIGHTS;
            defaults = defaults | Notification.DEFAULT_VIBRATE;
            defaults = defaults | Notification.DEFAULT_SOUND;
            mNotifyBuilder.setDefaults(defaults);
            // Set the content for Notification
            // mNotifyBuilder.setContentText(msg);
            // Set autocancel
            mNotifyBuilder.setAutoCancel(true);
//		mNotifyBuilder.setStyle(notiStyle);
            // Post a notification
            mNotificationManager.notify(m, mNotifyBuilder.build());
        } else {
        //Image Notification
        // Add the big picture to the style.
            NotificationCompat.BigPictureStyle notiStyle = new NotificationCompat.BigPictureStyle();
            notiStyle.setBigContentTitle(title);
            notiStyle.setSummaryText(message);
            try {
                remote_picture = BitmapFactory.decodeStream(
                        (InputStream) new URL(image_url).getContent());
                multiplier = getImageFactor(getResources());
                resized = Bitmap.createScaledBitmap(remote_picture, (int) (remote_picture.getWidth() * multiplier), (int) (remote_picture.getHeight() * multiplier), false);
            } catch (IOException e) {
                e.printStackTrace();
            }
            resultIntent = new Intent(this, com.buddygap.main.friend_menu.Notification.class);
            resultIntent.putExtra("title", title);
            resultIntent.putExtra("msg", message);
//		TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
            resultIntent.putExtra("icon", image_url);
            resultIntent.putExtra("type", type);
//


//
            NotificationCompat.Builder mNotifyBuilder;
            NotificationManager mNotificationManager;

            mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            resultPendingIntent = PendingIntent.getActivity(this, m,
                    resultIntent, PendingIntent.FLAG_ONE_SHOT);


            mNotifyBuilder = new NotificationCompat.Builder(this)
                    .setSmallIcon(R.drawable.app_icon).setContentText(title).setContentTitle(title);
            try {
                notiStyle.bigPicture(resized);
            } catch (Exception e) {

            }
            mNotifyBuilder.setStyle(notiStyle);


            mNotifyBuilder.setContentIntent(resultPendingIntent);
            resultIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);


//		mNotifyBuilder = new NotificationCompat.Builder(this)
//				.setSmallIcon(R.drawable.logo).setContentText(msg).setContentTitle("Shopping").setLargeIcon(remote_picture);

//		remoteViews.setImageViewResource(R.id.imageView2,remote_picture);

//		remoteViews.setTextViewText(R.id.textView2,msg);


            // Set pending intent
//		mNotifyBuilder.setContentIntent(resultPendingIntent);

            // Set Vibrate, Sound and Light
            int defaults = 0;
            defaults = defaults | Notification.DEFAULT_LIGHTS;
            defaults = defaults | Notification.DEFAULT_VIBRATE;
            defaults = defaults | Notification.DEFAULT_SOUND;

            mNotifyBuilder.setDefaults(defaults);
            // Set the content for Notification
            // mNotifyBuilder.setContentText(msg);
            // Set autocancel
            mNotifyBuilder.setAutoCancel(true);
//		mNotifyBuilder.setStyle(notiStyle);


            // Post a notification
//			Random random = new Random();
//			int m = random.nextInt(9999 - 1000) + 1000;
            mNotificationManager.notify(m, mNotifyBuilder.build());
        }
//		resultPendingIntent = PendingIntent.getActivity(this, 0,
//				resultIntent, PendingIntent.FLAG_ONE_SHOT);
////
//		NotificationCompat.Builder mNotifyBuilder;
//		NotificationManager mNotificationManager;
//
//		mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
//
//		mNotifyBuilder = new NotificationCompat.Builder(this)
//				.setSmallIcon(R.drawable.logo).setContentText(title).setContentTitle("BuddyGap");
//		// Set pending intent
//		mNotifyBuilder.setContentIntent(resultPendingIntent);
//
//		// Set Vibrate, Sound and Light
//		int defaults = 0;
//		defaults = defaults | Notification.DEFAULT_LIGHTS;
//		defaults = defaults | Notification.DEFAULT_VIBRATE;
//		defaults = defaults | Notification.DEFAULT_SOUND;
//
//		mNotifyBuilder.setDefaults(defaults);
//		// Set the content for Notification
//		// mNotifyBuilder.setContentText(msg);
//		// Set autocancel
//		mNotifyBuilder.setAutoCancel(true);
//		// Post a notification
//		mNotificationManager.notify(notifyID, mNotifyBuilder.build());
    }

    private Object getFragmentManager() {
        // TODO Auto-generated method stub
        return null;
    }


}