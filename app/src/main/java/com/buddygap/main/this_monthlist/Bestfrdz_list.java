package com.buddygap.main.this_monthlist;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.buddygap.main.R;
import com.buddygap.main.friend_menu.Best_friendz;
import com.buddygap.main.home.Incoming_outgoing_calls;
import com.buddygap.main.home.cur_month_out;
import com.buddygap.main.iconstant.IConstant;
import com.buddygap.main.searchadapter.productsearchfilter;
import com.buddygap.main.utils.AppController;
import com.buddygap.main.utils.BaseActivity;
import com.buddygap.main.utils.TextDrawable;
import com.buddygap.main.utils.WorldPopulation;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.ref.WeakReference;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;

import io.fabric.sdk.android.services.concurrency.AsyncTask;


public class Bestfrdz_list extends BaseActivity implements IConstant {

    ImageLoader imageLoader;
    DisplayImageOptions displayImageOptions;
    String url_image;
    ListView list_item1;
    ListView list_item;
    public static List<String> list_number, list_name;
    static int a;
    TextView friends, no_msg, ranktext_color;
    public static String friend, Appbarcolor, url;
    ImageView back_btn, search_btn;
    LinearLayout back_layout, search, friends_list_appbar;
    EditText search_edit;
    best_adapter adapter;
    ArrayList<String> list;
    ArrayList<Integer> rank_list;

    static ArrayList<HashMap<String, String>> installed_list;
    static ArrayList<String> profile_image;
    public static ArrayList<String> originallist, installed_array, array_number, array_rank, ordinal_ranklist;
    public static ArrayList<String> array_installed, array_shared_install,arraylist_mood;
    String best_frdz, igr_frdz, annoy_frdz, lousy_frdz;
    ArrayList<WorldPopulation> world = new ArrayList<WorldPopulation>();
    productsearchfilter adapter111;
    Iterator<String> listnumite;
    Iterator<Integer> listnumite1;
    StringBuilder builder;
    String str_phn,profile_pic,mood;
    int str_rnk;
    String url_status = "";
    int p = 0, divide = 0, ordinal = 0, search_value = 0;
    int color, textrank_color;
    static TextView timertext;
    final MyCounter timer = new MyCounter(20000, 1000);
    int dialogflag=0;
    Dialog dialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bestfrdz_list);
        try {
            best_frdz = pref_getvalue_str("bestfrd", "");
            igr_frdz = pref_getvalue_str("igrfrd", "");
            annoy_frdz = pref_getvalue_str("annoyfrd", "");
            lousy_frdz = pref_getvalue_str("lousyfrd", "");
        } catch (Exception e) {

        }

        initUI();
        list_name = new ArrayList<>();
        installed_list = new ArrayList<>();
        rank_list = new ArrayList<>();
        array_installed = new ArrayList<>();
        array_number = new ArrayList<>();
        profile_image = new ArrayList<>();
        array_rank = new ArrayList<>();
        installed_array = new ArrayList<>();
        ordinal_ranklist = new ArrayList<>();
        array_shared_install = new ArrayList<>();
        list_number = new ArrayList<>();
        arraylist_mood = new ArrayList<>();
        list = new ArrayList<>();

// Set the statusbar color




        Window window = getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);


        try {
            Intent intent = getIntent();
            list_name = intent.getStringArrayListExtra("name");
            list_number = intent.getStringArrayListExtra("number");
            friend = intent.getStringExtra("friends");
            Appbarcolor = intent.getStringExtra("Appbarcolor");

            friends.setText(friend);

            if(friend.equals("Best Friends")){
                if (isFirstTimebest()) {
                    // show dialog
                    dialogflag=1;
                    dialog = new Dialog(Bestfrdz_list.this,android.R.style.Theme_Holo_Dialog_NoActionBar);
                    dialog.setContentView(R.layout.show_invite_pop);
                    int width = (int)(getResources().getDisplayMetrics().widthPixels*0.90);
                    int height = (int)(getResources().getDisplayMetrics().heightPixels*0.80);

                    dialog.getWindow().setLayout(width, height);
                    dialog.getWindow().getAttributes().windowAnimations = R.style.animationdialog;
                    dialog.show();
//                    dialog.getWindow().setLayout(1050, 1300);
                    timertext = (TextView)dialog.findViewById(R.id.dialogtext);
                    timertext.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.dismiss();
                            dialogflag=0;
                        }
                    });
//                    timertext.setText("After 20 seconds, this dialog will be closed automatically!");
//                    timer.start();
//                    dialog.show();
//                    final Timer timer2 = new Timer();
//                    timer2.schedule(new TimerTask() {
//                        public void run() {
//                            dialog.dismiss();
//                            timer2.cancel(); //this will cancel the timer of the system
//                        }
//                    }, 20000); // the timer will count 20 seconds....
                }
            }
            else if(friend.equals("Ignored Friends")){
                if (isFirstTimeignored()) {
                    // show dialog
                    dialogflag=1;
                     dialog = new Dialog(Bestfrdz_list.this,android.R.style.Theme_Holo_Dialog_NoActionBar);
                    dialog.setContentView(R.layout.show_invite_pop);
                    dialog.getWindow().getAttributes().windowAnimations = R.style.animationdialog;
                    int width = (int)(getResources().getDisplayMetrics().widthPixels*0.90);
                    int height = (int)(getResources().getDisplayMetrics().heightPixels*0.80);

                    dialog.getWindow().setLayout(width, height);
                    dialog.show();
                    timertext = (TextView)dialog.findViewById(R.id.dialogtext);
                    timertext.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.dismiss();
                            dialogflag=0;
                        }
                    });
//                    timertext = (TextView)dialog.findViewById(R.id.dialog_text_view);
//                    timertext.setText("After 20 seconds, this dialog will be closed automatically!");
//                    timer.start();
//                    dialog.show();
//                    final Timer timer2 = new Timer();
//                    timer2.schedule(new TimerTask() {
//                        public void run() {
//                            dialog.dismiss();
//                            timer2.cancel(); //this will cancel the timer of the system
//                        }
//                    }, 20000); // the timer will count 20 seconds....
                }
            }
            else if(friend.equals("Annoyed Friends")){
                if (isFirstTimeannoy()) {
                    // show dialog
                    dialogflag=1;
                    dialog = new Dialog(Bestfrdz_list.this,android.R.style.Theme_Holo_Dialog_NoActionBar);
                    dialog.setContentView(R.layout.show_invite_pop);
                    dialog.getWindow().getAttributes().windowAnimations = R.style.animationdialog;
                    int width = (int)(getResources().getDisplayMetrics().widthPixels*0.90);
                    int height = (int)(getResources().getDisplayMetrics().heightPixels*0.80);

                    dialog.getWindow().setLayout(width, height);
                    dialog.show();
                    timertext = (TextView)dialog.findViewById(R.id.dialogtext);
                    timertext.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.dismiss();
                            dialogflag=0;
                        }
                    });
//                    timertext = (TextView)dialog.findViewById(R.id.dialog_text_view);
//                    timertext.setText("After 20 seconds, this dialog will be closed automatically!");
//
//                    timer.start();
//                    dialog.show();
//                    final Timer timer2 = new Timer();
//                    timer2.schedule(new TimerTask() {
//                        public void run() {
//                            dialog.dismiss();
//                            timer2.cancel(); //this will cancel the timer of the system
//                        }
//                    }, 20000); // the timer will count 20 seconds....
                }
            }
            else {
                if (isFirstTimelousy()) {
                    // show dialog
                    dialogflag=1;
                    dialog = new Dialog(Bestfrdz_list.this,android.R.style.Theme_Holo_Dialog_NoActionBar);
                    dialog.setContentView(R.layout.show_invite_pop);
                    dialog.getWindow().getAttributes().windowAnimations = R.style.animationdialog;
                    int width = (int)(getResources().getDisplayMetrics().widthPixels*0.90);
                    int height = (int)(getResources().getDisplayMetrics().heightPixels*0.80);

                    dialog.getWindow().setLayout(width, height);
                    dialog.show();
                    timertext = (TextView)dialog.findViewById(R.id.dialogtext);
                    timertext.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.dismiss();
                            dialogflag=0;
                        }
                    });
//                    timertext = (TextView)dialog.findViewById(R.id.dialog_text_view);
//                    timertext.setText("After 20 seconds, this dialog will be closed automatically!");
//                    timer.start();
//                    dialog.show();
//                    final Timer timer2 = new Timer();
//                    timer2.schedule(new TimerTask() {
//                        public void run() {
//                            dialog.dismiss();
//                            timer2.cancel(); //this will cancel the timer of the system
//                        }
//                    }, 20000); // the timer will count 20 seconds....
                }
            }

            // json(friend,list_number);

            // for less than kitkat version status bar color not change
            if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.KITKAT) {
                if (friend.equals(Incoming_outgoing_calls.best_frdz)) {
                    friends_list_appbar.setBackgroundResource(R.color.best_background_color);
                    ranktext_color.setTextColor(Color.parseColor("#ff6f00"));
                    color = R.color.best_rank;
                    textrank_color = R.color.best_background_color;
                    p = 1;
                } else if (friend.equals(Incoming_outgoing_calls.igr_frdz)) {
                    p = 1 + cur_month_out.list4.size();

                    friends_list_appbar.setBackgroundResource(R.color.ignore_background_color);
                    ranktext_color.setTextColor(Color.parseColor("#01c853"));
                    color = R.color.igr_rank;
                    textrank_color = R.color.ignore_background_color;

                } else if (friend.equals(Incoming_outgoing_calls.annoy_frdz)) {
                    friends_list_appbar.setBackgroundResource(R.color.annoy_background_color);
                    ranktext_color.setTextColor(Color.parseColor("#dd2c00"));
                    textrank_color = R.color.annoy_background_color;
                    color = R.color.ann_rank;
                    p = cur_month_out.list2.size() + 1 + cur_month_out.list4.size();
                } else {
                    friends_list_appbar.setBackgroundResource(R.color.lousy_background_color);
                    ranktext_color.setTextColor(Color.parseColor("#36474f"));
                    color = R.color.lousy_rank;
                    textrank_color = R.color.lousy_background_color;
                    p = cur_month_out.list0.size() + 1 + cur_month_out.list2.size() + cur_month_out.list4.size();
                }
            } else {
                if (friend.equals(Incoming_outgoing_calls.best_frdz)) {
                    friends_list_appbar.setBackgroundResource(R.color.best_background_color);
                    ranktext_color.setTextColor(Color.parseColor("#ff6f00"));
                    color = R.color.best_rank;
                    textrank_color = R.color.best_background_color;
                    //tintManager.setTintColor(Color.parseColor("#ffffff"));
                    window.setStatusBarColor(getResources().getColor(R.color.best_background_color));
                    p = 1;
                } else if (friend.equals(Incoming_outgoing_calls.igr_frdz)) {
                    p = 1 + cur_month_out.list4.size();
                    ranktext_color.setTextColor(Color.parseColor("#01c853"));
                    color = R.color.igr_rank;
                    textrank_color = R.color.ignore_background_color;
                    friends_list_appbar.setBackgroundResource(R.color.ignore_background_color);
                    // tintManager.setTintColor(Color.parseColor("#ffffff"));
                    window.setStatusBarColor(getResources().getColor(R.color.ignore_background_color));


                } else if (friend.equals(Incoming_outgoing_calls.annoy_frdz)) {
                    friends_list_appbar.setBackgroundResource(R.color.annoy_background_color);
                    window.setStatusBarColor(getResources().getColor(R.color.annoy_background_color));
                    ranktext_color.setTextColor(Color.parseColor("#dd2c00"));
                    color = R.color.ann_rank;
                    textrank_color = R.color.annoy_background_color;
                    p = cur_month_out.list2.size() + 1 + cur_month_out.list4.size();
                } else {
                    friends_list_appbar.setBackgroundResource(R.color.lousy_background_color);
                    ranktext_color.setTextColor(Color.parseColor("#36474f"));
                    color = R.color.lousy_rank;
                    textrank_color = R.color.lousy_background_color;
                    window.setStatusBarColor(getResources().getColor(R.color.lousy_background_color));
                    p = cur_month_out.list0.size() + 1 + cur_month_out.list2.size() + cur_month_out.list4.size();
                }
            }

        } catch (Exception e) {

        }
        // rank list into ordinal number
        try {
            for (int i = 0; i < list_number.size(); i++) {
                int sum = p + i;
                rank_list.add(sum);
                int aa = sum;
                divide = 0;
                if ((aa - 1) % 10 == 0 && aa > 20) {
                    divide = (aa - 1) / 10;
                    ordinal = 0;
                } else if ((aa - 2) % 10 == 0 && aa > 20) {
                    divide = (aa - 2) / 10;
                    ordinal = 1;
                } else if ((aa - 3) % 10 == 0 && aa > 20) {
                    divide = (aa - 3) / 10;
                    ordinal = 2;
                }
                if (ordinal < 3 && aa > 20 && aa > divide * 10 && divide != 0) {

                    if (ordinal == 0) {
                        ordinal_ranklist.add("" + divide + "1st");

                    } else if (ordinal == 1) {
                        ordinal_ranklist.add("" + divide + "2nd");
                    } else if (ordinal == 2) {
                        ordinal_ranklist.add("" + divide + "3rd");
                    }
                    ordinal = ordinal + 1;
                    divide = 0;
                } else {
                    if (aa == 1) {
                        ordinal_ranklist.add("1st");

                    } else if (aa == 2) {
                        ordinal_ranklist.add("2nd");
                    } else if (aa == 3) {
                        ordinal_ranklist.add("3rd");
                    } else {
                        ordinal_ranklist.add("" + aa + "th");
                    }


                }
            }
        } catch (Exception e) {

        }
        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
//                search_btn.setVisibility(View.VISIBLE);
//                search.setVisibility(View.VISIBLE);
//                InputMethodManager inputManager = (InputMethodManager)
//                        getSystemService(Context.INPUT_METHOD_SERVICE);
//                try {
//                    Incoming_outgoing_calls.gps_sett = 0;
//                    Incoming_outgoing_calls.permission_deny = 0;
//                    inputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(),
//                            InputMethodManager.HIDE_NOT_ALWAYS);
//                } catch (Exception e) {
//                }
//                if (search_value == 1) {
//
//                    search_edit.setVisibility(View.GONE);
//                    search_value = 0;
//                    adapter = new best_adapter(Bestfrdz_list.this, originallist, (ArrayList<String>) list_number, (ArrayList) list_name);
//                    list_item.setAdapter(adapter);
//                    list_item.setVisibility(View.VISIBLE);
//                    list_item1.setVisibility(View.GONE);
//                    search_edit.setText("");
//
//                } else {
//                    Intent intent = new Intent(Bestfrdz_list.this, Incoming_outgoing_calls.class);
//                    startActivity(intent);
//                    finish();
//                }
            }
        });


        // if number is not save  then its set No Name
        try {
            for (int i = 0; i < list_name.size(); i++) {
                arraylist_mood.add("Neutral");
                if (list_name.get(i).equals("No Name")) {
                    list.add(Bestfrdz_list.list_number.get(i));
                } else {
                    list.add(list_name.get(i));
                }
            }
            originallist = new ArrayList<>(list);
        } catch (Exception e) {

        }


        for (int i = 0; i < list_name.size(); i++) {
            array_installed.add("0");
        }


        //search particuar number from list
        if (isNetworkAvailable() == false) {
            for (int i = 0; i < list_name.size(); i++) {
                try {
                String s1 = String.valueOf(list_name.get(i));
                String stri = s1.substring(0, 1);
                TextDrawable drawable = TextDrawable.builder()
                        .beginConfig()
                                // height in px
                        .endConfig()
                        .buildRound(stri, Color.parseColor("#1665c1"));
                url_image = "https://scontent.xx.fbcdn.net/v/t1.0-1/s200x200/375045_466002050142972_671155216_n.jpg?oh=af79baa2785248de3358ab36b949affd&oe=57EBF53A";
                WorldPopulation wp = new WorldPopulation(array_installed.get(i), originallist.get(i), list_name.get(i), list_number.get(i), ordinal_ranklist.get(i), drawable,url_image,arraylist_mood.get(i));
                // Binds all strings into an array
                world.add(wp);
                } catch (Exception e) {
Log.d("noconnection",e.toString());

                }

            }
            try {
                adapter111 = new productsearchfilter(Bestfrdz_list.this, world);
                list_item1.setAdapter(adapter111);
            } catch (Exception e) {

            }
        } else {

        }


        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                best_adapter.list.clear();
                search_value = 1;
                search_edit.setVisibility(View.VISIBLE);
                list_item1.setVisibility(View.VISIBLE);
                list_item.setVisibility(View.GONE);
                search_btn.setVisibility(View.INVISIBLE);
                search.setVisibility(View.INVISIBLE);
            }
        });


        search_edit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String text = search_edit.getText().toString().toLowerCase(Locale.getDefault());
                try {
                    adapter111.filter(text);
                } catch (Exception e) {

                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

// if no friends ...  list empty
        try {
            a = list_name.size();
            if (a == 0) {
                no_msg.setVisibility(View.VISIBLE);
                no_msg.setText("No " + friend);
                ranktext_color.setVisibility(View.INVISIBLE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


        back_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
//                search_btn.setVisibility(View.VISIBLE);
//                search.setVisibility(View.VISIBLE);
//                InputMethodManager inputManager = (InputMethodManager)
//                        getSystemService(Context.INPUT_METHOD_SERVICE);
//
//                try {
//                    Incoming_outgoing_calls.gps_sett = 0;
//                    Incoming_outgoing_calls.permission_deny = 0;
//                    inputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(),
//                            InputMethodManager.HIDE_NOT_ALWAYS);
//                } catch (Exception e) {
//
//                }
//                if (search_value == 1) {
//                    search_edit.setVisibility(View.GONE);
//                    search_value = 0;
//                    adapter = new best_adapter(Bestfrdz_list.this, originallist, (ArrayList<String>) list_number, (ArrayList) list_name);
//                    try {
//                        list_item.setAdapter(adapter);
//                    } catch (Exception e) {
//
//                    }
//                    list_item.setVisibility(View.VISIBLE);
//                    list_item1.setVisibility(View.GONE);
//                    search_edit.setText("");
//
//                } else {
//                    Intent intent1 = new Intent(Bestfrdz_list.this, Incoming_outgoing_calls.class);
//                    startActivity(intent1);
//                    Bestfrdz_list.this.finish();
//                }
            }
        });

        // append phone number and rank
        listnumite = list_number.iterator();
        listnumite1 = rank_list.iterator();
        builder = new StringBuilder();
        while (listnumite.hasNext() && listnumite1.hasNext()) {
            str_phn = listnumite.next();
            str_rnk = listnumite1.next();
            builder = builder.append(str_phn).append("_").append(str_rnk).append(",");
        }


        try {
            adapter = new best_adapter(Bestfrdz_list.this, originallist, (ArrayList<String>) list_number, (ArrayList) list_name);
            list_item.setAdapter(adapter);

            if (isNetworkAvailable() == false) {
                alert_dialog();

            } else {
                json(friend);

            }
        } catch (Exception e) {

        }

    }

    // initial id's to view
    @Override
    public void initUI() {
        list_item = (ListView) findViewById(R.id.rankinglistitem);
        list_item1 = (ListView) findViewById(R.id.list_item1);
        back_btn = (ImageView) findViewById(R.id.back_btn);
        search_btn = (ImageView) findViewById(R.id.search_btn);
        search_edit = (EditText) findViewById(R.id.search_edit);
        back_layout = (LinearLayout) findViewById(R.id.back_layout);
        search = (LinearLayout) findViewById(R.id.search);
        friends_list_appbar = (LinearLayout) findViewById(R.id.friends_list_appbar);
        friends = (TextView) findViewById(R.id.friends);
        no_msg = (TextView) findViewById(R.id.no_msg);
        ranktext_color = (TextView) findViewById(R.id.ranktext_color);
    }

    void json(final String friend) {
        url = Base_url + "save_friends.php?type=" + URLEncoder.encode(friend) + "&user_id=" + pref_getvalue_str("user_id", "") + "&friends_list=" + URLEncoder.encode(builder.substring(0, builder.lastIndexOf(",")));
        Log.d("url_list", url);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, null, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject jsonObject) {
                try {
                    arraylist_mood.clear();
                    url_status = "1";

                    String jsonobject1 = jsonObject.getString("status");
                    if (jsonobject1.equals("true")) {
                        rank_list.clear();
                        list_number.clear();
                        array_installed.clear();

                        JSONArray jsonArray = jsonObject.getJSONArray("response");
                        for (int j = 0; j < jsonArray.length(); j++) {
                            JSONObject response = jsonArray.getJSONObject(j);
                            String type = response.getString("type");
                            // profile_pic = response.getString("profile_pic");
                            if (type.equals(friend)) {
                                JSONArray numbers = response.getJSONArray("numbers");
                                for (int i = 0; i < numbers.length(); i++) {
//                                    HashMap map2 = new HashMap();
                                    JSONObject num_ins = numbers.getJSONObject(i);
                                    String number = num_ins.getString("phone");
                                    int installed = num_ins.getInt("installed");
                                    String rank = num_ins.getString("rank");
                                    rank_list.add(Integer.valueOf(rank));
                                    list_number.add(number);
                                    profile_pic = num_ins.getString("profile_pic");
                                 //   profile_pic = num_ins.getString("profile_pic");
                                    mood = num_ins.getString("mood");
                                    profile_image.add(profile_pic);
                                    arraylist_mood.add(mood);
                                    Log.d("profile_internet",profile_pic);
                                    array_installed.add("" + installed);
                                }
                            }
                        }
                    }
                    Set<String> set = new HashSet<String>();
                    set.addAll(array_installed);
                    pref_setvalue_strset("array_installed", set);
                    pref_setvalue_str("url_status", url_status);
//shared_edit.putString("array_installed",array_installed.toString());
                    pref_setvalue_str("array_rank", array_rank.toString());
                    pref_setvalue_str("array_number", array_number.toString());
                    adapter = new best_adapter(Bestfrdz_list.this, originallist, (ArrayList<String>) list_number, (ArrayList) list_name);
                    list_item.setAdapter(adapter);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                // search list set
                for (int i = 0; i < list_name.size(); i++) {
                    String s1 = String.valueOf(list_name.get(i));
                    Log.d("S1error", s1);
                    String stri = s1.substring(0, 1);
                    TextDrawable drawable = TextDrawable.builder()
                            .beginConfig()
                                    // height in px
                            .endConfig()
                            .buildRound(stri, Color.parseColor("#1665c1"));
                    try{
                        url_image = "https://scontent.xx.fbcdn.net/v/t1.0-1/s200x200/375045_466002050142972_671155216_n.jpg?oh=af79baa2785248de3358ab36b949affd&oe=57EBF53A";
                        WorldPopulation wp = new WorldPopulation(array_installed.get(i), originallist.get(i), list_name.get(i), list_number.get(i), ordinal_ranklist.get(i), drawable,url_image,arraylist_mood.get(i));   // Binds all strings into an array
                        world.add(wp);}
                    catch (Exception e){

                    }
                }
                try {
                    adapter111 = new productsearchfilter(Bestfrdz_list.this, world);
                    list_item1.setAdapter(adapter111);
                } catch (Exception e) {

                }
                Log.d("listapi", jsonObject.toString());
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {

            }
        });
        AppController.getInstance().addToRequestQueue(jsonObjectRequest);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {

        search_btn.setVisibility(View.VISIBLE);
        search.setVisibility(View.VISIBLE);
        // super.onBackPressed();
        InputMethodManager inputManager = (InputMethodManager)
                getSystemService(Context.INPUT_METHOD_SERVICE);
        try {
            Incoming_outgoing_calls.gps_sett = 0;
            Incoming_outgoing_calls.permission_deny = 0;
            inputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(),
                    InputMethodManager.HIDE_NOT_ALWAYS);
        } catch (Exception e) {

        }
        if (search_value == 1) {
            search_edit.setVisibility(View.GONE);
            adapter = new best_adapter(Bestfrdz_list.this, originallist, (ArrayList<String>) list_number, (ArrayList) list_name);
            list_item.setAdapter(adapter);
//    adapter.notifyDataSetChanged();
            search_value = 0;
            search_edit.setText("");
            list_item.setVisibility(View.VISIBLE);
            list_item1.setVisibility(View.GONE);

        }
        else if(dialogflag == 1){
            dialogflag = 0;
            dialog.dismiss();

        }
        else {
            Intent intent = new Intent(Bestfrdz_list.this, Incoming_outgoing_calls.class);
            startActivity(intent);
            finish();
        }

    }
    /***
     * Checks that application runs first time and write flag at SharedPreferences
     * @return true if 1st time
     */
    public boolean isFirstTimebest()
    {

            SharedPreferences preferences = getPreferences(MODE_PRIVATE);
            boolean ranBeforebest = preferences.getBoolean("RanBeforebest", false);
            if (!ranBeforebest) {
                // first time
                SharedPreferences.Editor editor = preferences.edit();
                editor.putBoolean("RanBeforebest", true);
                editor.commit();
            }
            return !ranBeforebest;

    }
    public boolean isFirstTimeignored()
    {

        SharedPreferences preferences = getPreferences(MODE_PRIVATE);
        boolean ranBeforeignored = preferences.getBoolean("RanBeforeignored", false);
        if (!ranBeforeignored) {
            // first time
            SharedPreferences.Editor editor = preferences.edit();
            editor.putBoolean("RanBeforeignored", true);
            editor.commit();
        }
        return !ranBeforeignored;

    }
    public boolean isFirstTimeannoy()
    {

        SharedPreferences preferences = getPreferences(MODE_PRIVATE);
        boolean ranBeforeannoy = preferences.getBoolean("RanBeforeannoy", false);
        if (!ranBeforeannoy) {
            // first time
            SharedPreferences.Editor editor = preferences.edit();
            editor.putBoolean("RanBeforeannoy", true);
            editor.commit();
        }
        return !ranBeforeannoy;

    }
    public boolean isFirstTimelousy()
    {

        SharedPreferences preferences = getPreferences(MODE_PRIVATE);
        boolean ranBeforelousy = preferences.getBoolean("RanBeforelousy", false);
        if (!ranBeforelousy) {
            // first time
            SharedPreferences.Editor editor = preferences.edit();
            editor.putBoolean("RanBeforelousy", true);
            editor.commit();
        }
        return !ranBeforelousy;

    }
    public class MyCounter extends CountDownTimer
    {

        public MyCounter(long millisInFuture, long countDownInterval)
        {
            super(millisInFuture, countDownInterval);
        }

        @Override
        public void onFinish()
        {
            Log.i("Main","Timer Completed");
            timertext.setText("Timer Completed.");
        }

        @Override
        public void onTick(long millisUntilFinished)
        {
            timertext.setText("After 20 seconds, this dialog will be closed automatically! " + (millisUntilFinished/1000) + " " + " seconds remaining.");
        }

    }

    class best_adapter extends BaseAdapter {
        int position;
        Activity activity;
        // int divide=0,ordinal=0;
        ArrayList<String> arraylist;
        //      ArrayList<String> array_rank;
        ArrayList<String> array_number;
        ArrayList<String> array_name;
        //   ArrayList<String> array_installed=new ArrayList();
        ArrayList list;
        ImageView internet_profile;
        public List<String> getList() {
            return list_name;

        }

        public best_adapter(Activity activity, ArrayList<String> arraylist, ArrayList<String> array_number, ArrayList array_name) {
            this.arraylist = new ArrayList();
//            this.array_rank=new ArrayList();
            this.array_number = new ArrayList();
            this.array_name = new ArrayList();
            list = new ArrayList();
            divide = 0;
            ordinal = 0;
            this.activity = activity;
            this.arraylist = arraylist;
            // this.array_rank=array_rank;
            this.array_number = array_number;
            this.array_name = array_name;
            //  this.array_installed=array_installed;
            // Log.d("posytionnlisty", String.valueOf(arraylist));
        }

        @Override
        public int getCount() {
            return arraylist.size();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
         //   this.position=position;
            LayoutInflater inflater = LayoutInflater.from(activity);
            convertView = inflater.inflate(R.layout.row_best, null);
            TextView textView = (TextView) convertView.findViewById(R.id.name);
            TextView rank = (TextView) convertView.findViewById(R.id.rank);
            TextView invite_view = (TextView) convertView.findViewById(R.id.invite_view);
            ImageView menuimage = (ImageView) convertView.findViewById(R.id.menuimage);
             internet_profile = (ImageView) convertView.findViewById(R.id.internet_profile);
            ImageView emotions = (ImageView) convertView.findViewById(R.id.emotions);
            //ImageView user_profile = (ImageView) convertView.findViewById(R.id.user_profile);
            LinearLayout rank_layout = (LinearLayout) convertView.findViewById(R.id.rank_layout);
            rank_layout.setBackgroundColor(getResources().getColor(color));
            if (getList().get(position).equals("No Name")) {
                textView.setText(array_number.get(position));

                list.add(Bestfrdz_list.list_number.get(position));
                // menuimage.setImageDrawable(R.drawable.user);

                //***image

                if (array_installed.get(position).equals("0")) {
                    menuimage.setImageResource(R.drawable.user);
                } else {
                    menuimage.setVisibility(View.GONE);
                    internet_profile.setVisibility(View.VISIBLE);

                    imageLoader = ImageLoader.getInstance();
                    // ImageView imageView = imageViewReference.get();
                    imageLoader.init(ImageLoaderConfiguration.createDefault(Bestfrdz_list.this));
                    displayImageOptions = new DisplayImageOptions.Builder().showStubImage(R.drawable.user).showImageForEmptyUri(R.drawable.user).cacheOnDisc().cacheInMemory().build();
                    //    url_image = "https://scontent.xx.fbcdn.net/v/t1.0-1/s200x200/375045_466002050142972_671155216_n.jpg?oh=af79baa2785248de3358ab36b949affd&oe=57EBF53A";
                    imageLoader.displayImage(profile_image.get(position), internet_profile, displayImageOptions);
                    //    new ImageDownloaderTask(internet_profile,position);
                    Log.d("prooo",profile_pic);
                }
                // rank.setText("" + array_rank.get(position));
                rank.setText(ordinal_ranklist.get(position));
                rank.setTextColor(getResources().getColor(textrank_color));
            } else {


                String s1 = String.valueOf(getList().get(position));
                String stri = s1.substring(0, 1);
                //      if (Bestfrdz_list.list_number.get(position).equals("9888424527")) {
                    if (array_installed.get(position).equals("0")) {
                        TextDrawable drawable = TextDrawable.builder()
                                .beginConfig()
                                        // height in px
                                .endConfig()
                                .buildRound(stri, Color.parseColor("#1665c1"));
                        menuimage.setImageDrawable(drawable);

                    } else {
                        menuimage.setVisibility(View.GONE);
                        internet_profile.setVisibility(View.VISIBLE);

                        imageLoader = ImageLoader.getInstance();
                       // ImageView imageView = imageViewReference.get();
                        imageLoader.init(ImageLoaderConfiguration.createDefault(Bestfrdz_list.this));
                        displayImageOptions = new DisplayImageOptions.Builder().showStubImage(R.drawable.user).showImageForEmptyUri(R.drawable.user).cacheOnDisc().cacheInMemory().build();
                        //    url_image = "https://scontent.xx.fbcdn.net/v/t1.0-1/s200x200/375045_466002050142972_671155216_n.jpg?oh=af79baa2785248de3358ab36b949affd&oe=57EBF53A";
                        imageLoader.displayImage(profile_image.get(position), internet_profile, displayImageOptions);
                    //    new ImageDownloaderTask(internet_profile,position);
                        Log.d("prooo",profile_pic);
                    }
                textView.setText(getList().get(position));
                rank.setText(ordinal_ranklist.get(position));
                //  myViewHolder.rank.setText(rank_list.get(position));
                list.add(getList().get(position));
                // rank.setText(""+array_rank.get(position));
                rank.setTextColor(getResources().getColor(textrank_color));
            }

            try {
                if (array_installed.get(position).equals("1")) {
                    invite_view.setVisibility(View.INVISIBLE);
//                    menuimage.setVisibility(View.GONE);
//                    internet_profile.setVisibility(View.VISIBLE);
//                    imageLoader = ImageLoader.getInstance();
//                    imageLoader.init(ImageLoaderConfiguration.createDefault(Bestfrdz_list.this));
//                    displayImageOptions = new DisplayImageOptions.Builder().showStubImage(R.drawable.user).showImageForEmptyUri(R.drawable.user).cacheOnDisc().cacheInMemory().build();
//                    url_image = "https://scontent.xx.fbcdn.net/v/t1.0-1/s200x200/375045_466002050142972_671155216_n.jpg?oh=af79baa2785248de3358ab36b949affd&oe=57EBF53A";
//                    imageLoader.displayImage(profile_image.get(position), internet_profile, displayImageOptions);
                } else {
                    invite_view.setVisibility(View.VISIBLE);

                }
                invite_view.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(Bestfrdz_list.this, Send_invite.class);
                        intent.putExtra("phone_num", array_number.get(position));
                        intent.putExtra("name", array_name);
                        intent.putExtra("number", array_number);
                        intent.putExtra("send_nam", array_name.get(position));
                        intent.putExtra("frd_ranking", "2");
                        //  intent.putExtra("friends",igr_frdz);

                        intent.putExtra("friends", friend);
                        activity.startActivity(intent);
                        finish();
//                 json_invite(position);

                    }
                });
            } catch (Exception e) {

            }


            try {
                if(arraylist_mood.get(position).equals("Excellent")){
                    emotions.setImageResource(R.drawable.excellent);
                }
                else  if(arraylist_mood.get(position).equals("Good")){
                    emotions.setImageResource(R.drawable.good);
                }
                else  if(arraylist_mood.get(position).equals("Average")){
                    emotions.setImageResource(R.drawable.average);
                }
                else  if(arraylist_mood.get(position).equals("Sad")){
                    emotions.setImageResource(R.drawable.sad);
                }
                else  if(arraylist_mood.get(position).equals("Frustrated")){
                    emotions.setImageResource(R.drawable.frustrated);
                }
                else  if(arraylist_mood.get(position).equals("Neutral")){
                    emotions.setImageResource(R.drawable.min);
                }
                else {
                    emotions.setImageResource(R.drawable.min);
                }



            } catch (Exception e) {
                e.printStackTrace();
            }
            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //     Log.d("", "" + parent.getItemAtPosition(position));
                    Intent intent = new Intent(Bestfrdz_list.this, Best_friendz.class);
                    intent.putExtra("name", getList().get(position));
                    intent.putExtra("number", list_number.get(position));
                    intent.putExtra("friends", friend);
                    intent.putExtra("invite", array_installed.get(position));
                    startActivity(intent);

                }
            });
            return convertView;
        }



    class ImageDownloaderTask extends AsyncTask<String, Void, String> {
        private final WeakReference<ImageView> imageViewReference;
int position;
        public ImageDownloaderTask(ImageView imageView, int position) {
            imageViewReference = new WeakReference<ImageView>(imageView);
            this.position=position;
        }

        @Override
        protected String doInBackground(String... strings) {
            return profile_image.get(position);
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            imageLoader = ImageLoader.getInstance();
            ImageView imageView = imageViewReference.get();
            imageLoader.init(ImageLoaderConfiguration.createDefault(Bestfrdz_list.this));
            displayImageOptions = new DisplayImageOptions.Builder().showStubImage(R.drawable.user).showImageForEmptyUri(R.drawable.user).cacheOnDisc().cacheInMemory().build();
        //    url_image = "https://scontent.xx.fbcdn.net/v/t1.0-1/s200x200/375045_466002050142972_671155216_n.jpg?oh=af79baa2785248de3358ab36b949affd&oe=57EBF53A";
            imageLoader.displayImage(profile_image.get(position), imageView, displayImageOptions);
        }

//        @Override
//        protected void onPostExecute(Bitmap bitmap) {
//            if (isCancelled()) {
//                bitmap = null;
//            }
//
//            if (imageViewReference != null) {
//                ImageView imageView = imageViewReference.get();
//                if (imageView != null) {
//                    if (bitmap != null) {
//                        imageView.setImageBitmap(bitmap);
//                    } else {
//                        Drawable placeholder = imageView.getContext().getResources().getDrawable(R.drawable.placeholder);
//                        imageView.setImageDrawable(placeholder);
//                    }
//                }
//            }
//        }




    }



    }
}


