package com.buddygap.main.this_monthlist;

import android.Manifest;
import android.app.Activity;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.telephony.SmsManager;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.buddygap.main.R;
import com.buddygap.main.friend_menu.E_cards;
import com.buddygap.main.home.Incoming_outgoing_calls;
import com.buddygap.main.home.cur_month_out;
import com.buddygap.main.iconstant.IConstant;
import com.buddygap.main.navigation_menu.Friend_Ranking;
import com.buddygap.main.utils.AppController;
import com.buddygap.main.utils.BaseActivity;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by insonix on 26/5/16.
 */
public class Send_invite extends BaseActivity implements IConstant {
    String url_invite,phonenumber,friend,name,invite,this_monthcheck;
    ProgressDialog progressDialog;
    LinearLayout back_layout;
    //SharedPreferences shared_lang,sharedPreferences;
    EditText to,message_content;
    Button btn_send;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sendinvite);
      initUI();



        try{
        Bundle extras=getIntent().getExtras();
        phonenumber=extras.getString("phone_num");
            name=extras.getString("send_nam");
        friend=getIntent().getStringExtra("friends");
        this_monthcheck=getIntent().getStringExtra("this_monthcheck");
        }
        catch (Exception e){

        }
       // to.setText(phonenumber);
        if (name.equals("No Name")){
        to.setText(phonenumber);}
        else{
            to.setText(name);
        }

//        message_content.setText("Install BuddyGap through this Link: https://play.google.com/store/apps/details?id=com.buddygap.main");
        message_content.setText("Try BuddyGap app, It is great for improving professional and  personal relationships https://play.google.com/store/apps/details?id=com.buddygap.main");
        btn_send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (weHavePermissionToReadContacts()) {
                    json_invite();
                } else {
                    requestReadContactsPermissionFirst();
                }

            }
        });
back_layout.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {
        if(getIntent().getStringExtra("frd_ranking").equals("2")){
            Intent intent=new Intent(Send_invite.this,Bestfrdz_list.class);
            intent.putExtra("friends",friend);
            intent.putExtra("name", getIntent().getStringArrayListExtra("name"));
            intent.putExtra("number", getIntent().getStringArrayListExtra("number"));
            intent.putExtra("this_monthcheck", this_monthcheck);
            startActivity(intent);


            finish();
        }
        else if(getIntent().getStringExtra("frd_ranking").equals("1")){
            Intent intent=new Intent(Send_invite.this,Friend_Ranking.class);
            intent.putExtra("rankingnumber", Incoming_outgoing_calls.arrylist_ranking);
            intent.putExtra("rankingsize", Incoming_outgoing_calls.arrylist_ranking_size);
            intent.putExtra("name_rank", cur_month_out.avg_scall);
            startActivity(intent);
            finish();

        }

        else if(getIntent().getStringExtra("frd_ranking").equals("3")){
            Intent intent=new Intent(Send_invite.this,E_cards.class);
            intent.putExtra("friends",friend);
            intent.putExtra("name", name);
            intent.putExtra("number", phonenumber);
            intent.putExtra("invite", getIntent().getStringExtra("invite"));
            intent.putExtra("this_monthcheck", this_monthcheck);
            startActivity(intent);
            finish();


        }
        else {
            finish();
        }
    }
});

    }

    @Override
    public void initUI() {
        to=(EditText)findViewById(R.id.to);
        btn_send=(Button)findViewById(R.id.btn_send);
        back_layout=(LinearLayout)findViewById(R.id.back_layout);
        message_content=(EditText)findViewById(R.id.message_content);
    }

    void json_invite(){

        url_invite=Base_url+"send_invite.php?user_id="+pref_getvalue_str("user_id", "")+"&phone="+phonenumber+"&type="+friend;
        progress_show_sending();
        JsonObjectRequest jsonObjectRequest=new JsonObjectRequest(Request.Method.POST, url_invite, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject jsonObject) {
                try {
                    String status=jsonObject.getString("status");
                    if (status.equals("true")){
//                        SmsManager smsManager = SmsManager.getDefault();
//                        smsManager.sendTextMessage(phonenumber, null, message_content.getText().toString(), null, null);

                        try {

                            String SENT = "sent";
                            String DELIVERED = "delivered";

                            Intent sentIntent = new Intent(SENT);
     /*Create Pending Intents*/
                            PendingIntent sentPI = PendingIntent.getBroadcast(
                                    getApplicationContext(), 0, sentIntent,
                                    PendingIntent.FLAG_UPDATE_CURRENT);

                            Intent deliveryIntent = new Intent(DELIVERED);

                            PendingIntent deliverPI = PendingIntent.getBroadcast(
                                    getApplicationContext(), 0, deliveryIntent,
                                    PendingIntent.FLAG_UPDATE_CURRENT);
     /* Register for SMS send action */
                            registerReceiver(new BroadcastReceiver() {

                                @Override
                                public void onReceive(Context context, Intent intent) {
                                    String result = "";

                                    switch (getResultCode()) {

                                        case Activity.RESULT_OK:
                                            result = "SMS sent successfully";
                                            break;
                                        case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
                                            result = "SMS sending failed";
                                            break;
                                        case SmsManager.RESULT_ERROR_RADIO_OFF:
                                            result = "Radio off";
                                            break;
                                        case SmsManager.RESULT_ERROR_NULL_PDU:
                                            result = "No PDU defined";
                                            break;
                                        case SmsManager.RESULT_ERROR_NO_SERVICE:
                                            result = "No service";
                                            break;
                                    }

                                    Toast.makeText(getApplicationContext(), result,
                                            Toast.LENGTH_LONG).show();
                                    try{
                            if(getIntent().getStringExtra("frd_ranking").equals("2")){
                                Intent intent1=new Intent(Send_invite.this,Bestfrdz_list.class);
                                intent1.putExtra("friends",friend);
                                intent1.putExtra("name", getIntent().getStringArrayListExtra("name"));
                                intent1.putExtra("number", getIntent().getStringArrayListExtra("number"));
                                startActivity(intent1);
                                finish();
                            }
                            else if(getIntent().getStringExtra("frd_ranking").equals("1")){
                                Intent intent1=new Intent(Send_invite.this,Friend_Ranking.class);
                                intent1.putExtra("rankingnumber",Incoming_outgoing_calls.arrylist_ranking);
                                intent1.putExtra("rankingsize",Incoming_outgoing_calls.arrylist_ranking_size);
                                intent1.putExtra("name_rank",cur_month_out.avg_scall);
                                startActivity(intent1);
                                finish();

                            }
                            else if(getIntent().getStringExtra("frd_ranking").equals("3")) {
                                Intent intent1 = new Intent(Send_invite.this, E_cards.class);
                                intent1.putExtra("friends", friend);
                                intent1.putExtra("name", name);
                                intent1.putExtra("number", phonenumber);
                                intent1.putExtra("invite", getIntent().getStringExtra("invite"));
                                intent1.putExtra("this_monthcheck", this_monthcheck);
                                startActivity(intent1);
                                finish();
                            }
                            else{
                                finish();
                            }
                        }
                        catch (Exception e){

                        }
                                    progress_dismiss();
                                }

                            }, new IntentFilter(SENT));
     /* Register for Delivery event */
                            registerReceiver(new BroadcastReceiver() {

                                @Override
                                public void onReceive(Context context, Intent intent) {
                                    Toast.makeText(getApplicationContext(), "Delivered",
                                            Toast.LENGTH_LONG).show();
                                }

                            }, new IntentFilter(DELIVERED));

      /*Send SMS*/
                            SmsManager smsManager = SmsManager.getDefault();
                            smsManager.sendTextMessage(phonenumber, null, message_content.getText().toString(), sentPI,
                                    deliverPI);

                        } catch (Exception ex) {
                            Toast.makeText(getApplicationContext(),
                                    ex.getMessage().toString(), Toast.LENGTH_LONG)
                                    .show();
                            ex.printStackTrace();
                            progress_dismiss();
                        }




//                        Toast.makeText(getApplicationContext(), "SMS sent.",
//                                Toast.LENGTH_LONG).show();
//
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    progress_dismiss();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                progress_dismiss();
            }
        });
        AppController.getInstance().addToRequestQueue(jsonObjectRequest);
    }

    @Override
    public void onBackPressed() {
        try{
        if(getIntent().getStringExtra("frd_ranking").equals("2")){
        Intent intent=new Intent(Send_invite.this,Bestfrdz_list.class);
        intent.putExtra("friends",friend);
        intent.putExtra("name", getIntent().getStringArrayListExtra("name"));
        intent.putExtra("number", getIntent().getStringArrayListExtra("number"));
            intent.putExtra("this_monthcheck", this_monthcheck);
        startActivity(intent);


        finish();
        }
        else if(getIntent().getStringExtra("frd_ranking").equals("1")){
            Intent intent=new Intent(Send_invite.this,Friend_Ranking.class);
            intent.putExtra("rankingnumber",Incoming_outgoing_calls.arrylist_ranking);
            intent.putExtra("rankingsize",Incoming_outgoing_calls.arrylist_ranking_size);
            intent.putExtra("name_rank",cur_month_out.avg_scall);
            startActivity(intent);
            finish();

        }
        else if(getIntent().getStringExtra("frd_ranking").equals("3")){
            Intent intent=new Intent(Send_invite.this,E_cards.class);
            intent.putExtra("friends",friend);
            intent.putExtra("name", name);
            intent.putExtra("number", phonenumber);
            intent.putExtra("invite", getIntent().getStringExtra("invite"));
            intent.putExtra("this_monthcheck", this_monthcheck);
            startActivity(intent);
            finish();


        }
        else{
            finish();
        }}
        catch (Exception e){

        }
    }
    private boolean weHavePermissionToReadContacts() {
        return ContextCompat.checkSelfPermission(this, Manifest.permission.SEND_SMS) == PackageManager.PERMISSION_GRANTED;
    }



    private void requestReadContactsPermissionFirst() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.SEND_SMS)) {
            Toast.makeText(this, "We need permission so you can send text message to your friends.", Toast.LENGTH_LONG).show();
            requestForResultContactsPermission();
        } else {
            requestForResultContactsPermission();
        }
    }

    private void requestForResultContactsPermission() {
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.SEND_SMS}, 123);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 123
                && grantResults.length > 0
                && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//            Toast.makeText(this, "Permission Granted", Toast.LENGTH_SHORT).show();
            json_invite();
        } else {
            Toast.makeText(this, "Permission Denied", Toast.LENGTH_SHORT).show();
        }
    }
}
