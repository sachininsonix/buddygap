package com.buddygap.main.navigation_menu;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.util.Base64;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.buddygap.main.R;
import com.buddygap.main.home.Incoming_outgoing_calls;
import com.buddygap.main.iconstant.IConstant;
import com.buddygap.main.utils.AppController;
import com.buddygap.main.utils.BaseActivity;
import com.buddygap.main.utils.UserPicture;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;


public class User_profile extends BaseActivity implements IConstant {
    EditText first_name, last_name;
    Button submit;
    LinearLayout back_btn;
    String url_profile;
    ImageView profile_img;
    Bitmap bitmap = null;
    String image = null, file;
    Uri file1;
    TextView friends;
    ByteArrayOutputStream bytes;
    LinearLayout back_layout;
    String f_name, l_name, profile_pic;
    protected static final int IMAGE_CAMERA_11 = 11;
    protected static final int IMAGE_GALLERY_22 = 22;
    protected static final int IMAGE_CAMERA_33 = 33;
    ImageLoader imageLoader;
    DisplayImageOptions displayImageOptions;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_profile);

        // initialize id's of view
        initUI();

        imageLoader = ImageLoader.getInstance();
        imageLoader.init(ImageLoaderConfiguration.createDefault(User_profile.this));
        displayImageOptions = new DisplayImageOptions.Builder().showStubImage(R.drawable.user).showImageForEmptyUri(R.drawable.user).cacheOnDisc().cacheInMemory().build();


        if (pref_getvalue_str("profile", "").equals("")) {
            friends.setText("Profile");
        } else {
            try {
                friends.setText(pref_getvalue_str("profile", ""));
            } catch (Exception e) {

            }


        }

        // validation on firstname
        if (pref_getvalue_str("FirstName", "").length() > 0)
        {
            try {
                first_name.setText(pref_getvalue_str("FirstName", ""));
                last_name.setText(pref_getvalue_str("LastName", ""));
                image = pref_getvalue_str("Image", "");
            } catch (Exception e) {

            }

             if (bitmap == null && image.equals("")) {
            } else {
                imageLoader.displayImage(image, profile_img, displayImageOptions);

            }

            first_name.setEnabled(false);
        }




        profile_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectImage();


            }
        });



        back_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Incoming_outgoing_calls.gps_sett = 0;
                String submit_value = pref_getvalue_str("submit", "");
                Log.d("submitvaluee", submit_value);
                Intent intent = new Intent(User_profile.this, Incoming_outgoing_calls.class);
                startActivity(intent);
                finish();


            }
        });



        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (isNetworkAvailable() == false) {
                    alert_dialog();


                } else {


                    try {
                        json_object();
                        InputMethodManager inputManager = (InputMethodManager)
                                getSystemService(Context.INPUT_METHOD_SERVICE);
                        inputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(),
                                InputMethodManager.HIDE_NOT_ALWAYS);
                    } catch (Exception e) {

                    }

                }

            }
        });
    }

    @Override
    public void initUI() {
        submit = (Button) findViewById(R.id.submit);

        first_name = (EditText) findViewById(R.id.first_name);
        last_name = (EditText) findViewById(R.id.last_name);
        profile_img = (ImageView) findViewById(R.id.profile_img);
        friends = (TextView) findViewById(R.id.friends);
        back_layout = (LinearLayout) findViewById(R.id.back_layout);
    }


    void json_object() {

        url_profile = Base_url + "profile.php?";

        progress_show();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url_profile, new Response.Listener<String>() {
            @Override
            public void onResponse(String s) {

                try {
                    JSONObject jsonObject = new JSONObject(s);

                    String status = jsonObject.getString("status");
                    if (status.equals("true")) {
                        JSONObject jsonObject1 = jsonObject.getJSONObject("response");

                        f_name = jsonObject1.getString("firstname");
                                    l_name = jsonObject1.getString("lastname");
                        profile_pic = jsonObject1.getString("profile_pic");
                        pref_setvalue_str("FirstName", f_name);
                        pref_setvalue_str("LastName", l_name);
                        pref_setvalue_str("Image", profile_pic);
                        Toast.makeText(User_profile.this,"Profile updated successfully",Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(User_profile.this, Incoming_outgoing_calls.class);
                        startActivity(intent);
                        finish();

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

//                progress_dismiss();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                progress_dismiss();
                volleyError.printStackTrace();
                Log.d("Error","error1"+volleyError);

            }


        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> params = new HashMap<>();
                bytes = new ByteArrayOutputStream();
                if (bitmap == null) {

                } else {

                    bitmap.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
                    byte data1[] = bytes.toByteArray();
                    file = Base64.encodeToString(data1, Base64.DEFAULT);
//                    bitmap.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream);
//


                }
                params.put("user_id", pref_getvalue_str("user_id", ""));
                params.put("firstname", pref_getvalue_str("FirstName", ""));
                params.put("lastname", last_name.getText().toString().trim());


                if (file == null) {

                } else {
                    params.put("profile_name", "" + new Date().getTime() + ".png");
                    params.put("profile_pic", file);
                }

                return params;

            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(stringRequest);
    }


    private void selectImage() {


        final CharSequence[] options = {"Take Photo", "Choose from Gallery", "Cancel"};


        AlertDialog.Builder builder = new AlertDialog.Builder(User_profile.this);

        builder.setTitle("Profile Picture!");

        builder.setItems(options, new DialogInterface.OnClickListener() {

            @Override

            public void onClick(DialogInterface dialog, int item) {

                if (options[item].equals("Take Photo"))

                {

                    if (weHavePermissionToReadContacts()) {
//            readTheContacts();

                        if (Build.VERSION.SDK_INT >= 23) {
                            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

                            startActivityForResult(intent, IMAGE_CAMERA_11);
                        } else {
                            Intent takePicture = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                            startActivityForResult(takePicture, IMAGE_CAMERA_33);
                        }

                    } else {
                        requestReadContactsPermissionFirst();
                    }


                } else if (options[item].equals("Choose from Gallery"))

                {

                    Intent intent = new Intent(Intent.ACTION_PICK);
                    intent.setType("image/*");
                    startActivityForResult(intent, IMAGE_GALLERY_22);


                } else if (options[item].equals("Cancel")) {

                    dialog.dismiss();

                }

            }

        });

        builder.show();

    }


    @Override
    public void onBackPressed() {
        // super.onBackPressed();
        Incoming_outgoing_calls.gps_sett = 0;
        Intent intent = new Intent(User_profile.this, Incoming_outgoing_calls.class);

        startActivity(intent);
        finish();

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case IMAGE_CAMERA_11:
                if (data != null) {
//                    Uri uri = data.getData();
                    try {

                        onCaptureImageResult(data);
                    } catch (Exception e) {

                    }
                } else {

                }
                break;
            case IMAGE_GALLERY_22:
                if (data != null) {
                    Uri uri = data.getData();
                    try {

                            bitmap = new UserPicture(uri, getContentResolver()).getBitmap();
                            profile_img.setImageBitmap(bitmap);


                    } catch (Exception e) {

                    }
                } else {

                }
                break;
            case IMAGE_CAMERA_33:
                if (data != null) {
                    Uri uri = data.getData();
                    try {

                            bitmap = new UserPicture(uri, getContentResolver()).getBitmap();
                            profile_img.setImageBitmap(bitmap);

                    } catch (Exception e) {

                    }
                } else {

                }
                break;
        }
    }
//

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    private boolean weHavePermissionToReadContacts() {
        return ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED&&ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
             ;

    }



    private void requestReadContactsPermissionFirst() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.CAMERA)&&ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
            ) {
            Toast.makeText(this, "We need permission so you can access your camera.", Toast.LENGTH_LONG).show();
            requestForResultContactsPermission();
        } else {
            requestForResultContactsPermission();
        }
    }

    private void requestForResultContactsPermission() {
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE}, 123);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 123
                && grantResults.length > 0
                && grantResults[0] == PackageManager.PERMISSION_GRANTED&& grantResults[1] == PackageManager.PERMISSION_GRANTED) {
//            Toast.makeText(this, "Permission Granted", Toast.LENGTH_SHORT).show();
//            Intent takePicture = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//            startActivityForResult(takePicture, IMAGE_CAMERA_11);
            if (Build.VERSION.SDK_INT >= 23) {
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

                    startActivityForResult(intent, IMAGE_CAMERA_11);
//                }

            } else {

                Intent takePicture = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(takePicture, IMAGE_CAMERA_33);
            }
        } else {
            Toast.makeText(this, "Permission Denied", Toast.LENGTH_SHORT).show();
        }
    }
    private void onCaptureImageResult(Intent data) {
         bitmap = (Bitmap) data.getExtras().get("data");
        Log.d("thumbnail","thumbnail"+bitmap);
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
        byte data1[] = bytes.toByteArray();
        file = Base64.encodeToString(data1, Base64.DEFAULT);
        File destination = new File(Environment.getExternalStorageDirectory(),
                System.currentTimeMillis() + ".jpg");
        FileOutputStream fo;
        try {
            destination.createNewFile();
            fo = new FileOutputStream(destination);
            fo.write(bytes.toByteArray());
            fo.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        profile_img.setImageBitmap(bitmap);

    }
}