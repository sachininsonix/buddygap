package com.buddygap.main.navigation_menu;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.buddygap.main.BuildConfig;
import com.buddygap.main.home.Incoming_outgoing_calls;
import com.buddygap.main.iconstant.IConstant;
import com.buddygap.main.utils.BaseActivity;


import com.buddygap.main.R;

public class About_us extends BaseActivity implements IConstant {
LinearLayout back_layout;
    TextView aboutus,version_number,official_website,new_version,aboutustext;
      String url;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);
         url = "http://buddygap.com/";
           initUI();
        if (pref_getvalue_str("aboutus", "").equals("")){
            aboutus.setText("About us");
        }
        else
        aboutus.setText(pref_getvalue_str("aboutus",""));

//        Get Version Code and Version Name and inflate on Textview

        String versionName = BuildConfig.VERSION_NAME;
        aboutustext.setMovementMethod(new ScrollingMovementMethod());
        version_number.setText("V : "+versionName);

//      Onclick official Website link go to Website Page
        official_website.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Uri uri = Uri.parse(url);
                Intent browserIntent = new Intent(Intent.ACTION_VIEW);
                browserIntent.setDataAndType(uri, "text/html");
                browserIntent.addCategory(Intent.CATEGORY_BROWSABLE);
               startActivity(browserIntent);
            }
        });


        back_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Incoming_outgoing_calls.gps_sett = 0;
                Intent intent=new Intent(About_us.this,Incoming_outgoing_calls.class);
                startActivity(intent);
                About_us.this.finish();

            }
        });
    }

    @Override
    public void initUI() {
        back_layout=(LinearLayout)findViewById(R.id.back_layout);
        aboutus=(TextView)findViewById(R.id.aboutus);
        version_number=(TextView)findViewById(R.id.version_number);
        official_website=(TextView)findViewById(R.id.official_website);
        new_version=(TextView)findViewById(R.id.new_version);
        aboutustext=(TextView)findViewById(R.id.aboutustext);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return false;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
      //  super.onBackPressed();
        Incoming_outgoing_calls.gps_sett = 0;
        Intent intent=new Intent(About_us.this,Incoming_outgoing_calls.class);
        startActivity(intent);
        finish();;
    }
}
