package com.buddygap.main.navigation_menu;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.buddygap.main.R;
import com.buddygap.main.friend_menu.Best_friendz;
import com.buddygap.main.home.Incoming_outgoing_calls;
import com.buddygap.main.home.cur_month_out;
import com.buddygap.main.iconstant.IConstant;
import com.buddygap.main.searchadapter.friandrankingsearchfilter;
import com.buddygap.main.this_monthlist.Send_invite;
import com.buddygap.main.utils.AppController;
import com.buddygap.main.utils.BaseActivity;
import com.buddygap.main.utils.Friend_Population;
import com.buddygap.main.utils.TextDrawable;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

public class Friend_Ranking extends BaseActivity implements IConstant {
List number_list,no_offrd,name_list;
    ListView ranking_list,list_item1;
    int search_value=0;
    LinearLayout back_layout,search_img;
    static String frd_rank="0";
    TextView friends,no_msg;
    String url_ranking;
    StringBuilder builder;
    EditText search_edit;
    Iterator<String> listnumite,listnumite1;
    ArrayList rank_list;
    ArrayList<String> profile_image,arraylist_mood,originallist,array_emotions;
    ImageLoader imageLoader;
    DisplayImageOptions displayImageOptions;
    ArrayList<Friend_Population> world = new ArrayList<Friend_Population>();
    ArrayList<String> array_installed,friend_list,view_rank,view_list;
    ArrayList<Integer> array_textcolor,array_backgroundcolor;
    String  str_phn,str_rnk,frienddd;
    ArrayList<String> list;
    ArrayList<Boolean> visibility_view;
    String url_image;
       int divide=0,ordinal=0;
    friandrankingsearchfilter adapter111;
      @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_friend__ranking);
        initUI();

        Intent intent =getIntent();
          number_list=new ArrayList();
        array_installed=new ArrayList();
          visibility_view=new ArrayList();
          array_emotions=new ArrayList();
          friend_list=new ArrayList();
          view_rank=new ArrayList();
          view_list=new ArrayList();
        no_offrd=new ArrayList();
          array_textcolor=new ArrayList();
          array_backgroundcolor=new ArrayList();
        name_list=new ArrayList();
          list=new ArrayList();
        rank_list=new ArrayList();
          arraylist_mood=new ArrayList();
          profile_image=new ArrayList();
          try{
              number_list=intent.getStringArrayListExtra("rankingnumber");
              no_offrd=intent.getStringArrayListExtra("rankingsize");
              name_list=intent.getStringArrayListExtra("name_rank");}
          catch (Exception e){

          }

          try {
              for (int i = 0; i < name_list.size(); i++) {
                  if (name_list.get(i).equals("No Name")) {
                      list.add((String) number_list.get(i));
                  } else {
                      list.add((String) name_list.get(i));
                  }
              }
              originallist = new ArrayList<>(list);
          } catch (Exception e) {

          }



        if(number_list.size()==0){
            no_msg.setVisibility(View.VISIBLE);
            ranking_list.setVisibility(View.GONE);
        }


        for(int i=0;i<number_list.size();i++){
            int p=i+1;

            if((p-1)%10==0 && p>20)
            {
                divide=(p-1)/10;
                ordinal=0;
            }
            else  if((p-2)%10==0 && p>20)
            {
                divide=(p-1)/10;
                ordinal=1;
            }
            else  if((p-3)%10==0 && p>20)
            {
                divide=(p-1)/10;
                ordinal=2;
            }
            if(ordinal<3 && p>20  && p>divide*10 && divide!=0) {
                ordinal=ordinal+1;
                if (ordinal == 1) {
            //        rank.setText("" + divide + "1st");
                    rank_list.add(""+divide+"1st");
                } else if (ordinal == 2) {

                    rank_list.add(""+divide+"2nd");
          //          rank.setText("" + divide + "2nd");
                } else if (ordinal == 3) {

                    rank_list.add(""+divide+"3rd");
        //            rank.setText("" + divide + "3rd");
                }
                divide=0;

            }
            else{
                // ordinal=0;
                if(p==1){
      //              rank.setText("1st");
                    rank_list.add("1st");
                }

                else if(p==2){

                    rank_list.add("2nd");
    //                rank.setText("2nd");
                }
                else if(p==3){

                    rank_list.add("3rd");
  //                  rank.setText("3rd");
                }
                else{
                    rank_list.add(""+p+"th");
                }
//                    rank.setText(""+p+"th");}


        }}

for(int i=0;i<number_list.size();i++){
    arraylist_mood.add("Neutral");
    array_installed.add("0");
    int position=i;
    if(cur_month_out.list8.size()>position){


       // frienddd=Incoming_outgoing_calls.best_frdz;
        array_backgroundcolor.add(R.color.best_background_color);
        array_textcolor.add(R.color.best_rank);

//        holder.view_frdranking.setVisibility(View.VISIBLE);
//        holder.view_list.setText("Lousy  Friends");
//        friend=Incoming_outgoing_calls.lousy_frdz;
//        holder. view_list.setBackgroundColor(Color.parseColor("#36474f"));
//        holder.rank_view.setBackgroundColor(Color.parseColor("#232e33"));}

        if (position==0){
            visibility_view.add(true);
            friend_list.add("Best Friends");
            view_list.add("#ff9f00");
            view_rank.add("#d18200");
        }
        else{
            visibility_view.add(false);
            friend_list.add("Best Friends");
            view_list.add("#ff9f00");
            view_rank.add("#d18200");
        }

    }
    else if((cur_month_out.list7.size()+cur_month_out.list8.size())>position)
    {

       // frienddd=Incoming_outgoing_calls.igr_frdz;
        array_backgroundcolor.add(R.color.ignore_background_color);
        array_textcolor.add(R.color.igr_rank);

        if (cur_month_out.list8.size()==position){
            visibility_view.add(true);
            friend_list.add("Ignored Friends");
            view_list.add("#01C853");
            view_rank.add("#00A445");
        }
        else{
            visibility_view.add(false);
            friend_list.add("Ignored Friends");
            view_list.add("#01C853");
            view_rank.add("#00A445");
        }
    }
    else if((cur_month_out.list7.size()+cur_month_out.list8.size()+cur_month_out.list6.size())>position)
    {

      //  frienddd=Incoming_outgoing_calls.annoy_frdz;
        array_backgroundcolor.add(R.color.annoy_background_color);
        array_textcolor.add(R.color.ann_rank);
if ((cur_month_out.list7.size()+cur_month_out.list8.size())==position){
    visibility_view.add(true);
    friend_list.add("Annoyed Friends");
    view_list.add("#dd2c00");
    view_rank.add("#af2300");

}
else{
    visibility_view.add(false);
    friend_list.add("Annoyed Friends");
    view_list.add("#dd2c00");
    view_rank.add("#af2300");
}
    }
    else
    //if((cur_month_out.list7.size()+cur_month_out.list8.size()+cur_month_out.list6.size()+cur_month_out.list9.size())>position)
    {
        if ((cur_month_out.list7.size()+cur_month_out.list8.size()+cur_month_out.list6.size())==position){
            visibility_view.add(true);
            friend_list.add("Lousy Friends");
            view_list.add("#36474f");
            view_rank.add("#232e33");
        }
        else{
            visibility_view.add(false);
            friend_list.add("Lousy Friends");
            view_list.add("#36474f");
            view_rank.add("#232e33");
        }
        //view_list.setVisibility(View.GONE);

      //  frienddd=Incoming_outgoing_calls.lousy_frdz;
        array_backgroundcolor.add(R.color.lousy_background_color);
        array_textcolor.add(R.color.lousy_rank);
    }





}


          if (isNetworkAvailable() == false) {
              for (int i = 0; i < number_list.size(); i++) {
                  try {
                  String s1 = String.valueOf(name_list.get(i));
                  String stri = s1.substring(0, 1);
                  TextDrawable drawable = TextDrawable.builder()
                          .beginConfig()
                                  // height in px
                          .endConfig()
                          .buildRound(stri, Color.parseColor("#1665c1"));
                  url_image = "https://scontent.xx.fbcdn.net/v/t1.0-1/s200x200/375045_466002050142972_671155216_n.jpg?oh=af79baa2785248de3358ab36b949affd&oe=57EBF53A";
                  Friend_Population wp=new Friend_Population(array_installed.get(i),originallist.get(i).toString(),name_list.get(i).toString(),number_list.get(i).toString(),rank_list.get(i).toString(),drawable,url_image,array_textcolor.get(i),array_backgroundcolor.get(i),visibility_view.get(i),view_rank.get(i),view_list.get(i),friend_list.get(i),arraylist_mood.get(i));
                  // WorldPopulation wp = new WorldPopulation(array_installed.get(i), originallist.get(i), name_list.get(i), number_list.get(i), rank_list.get(i), drawable,url_image);
                  // Binds all strings into an array
                  world.add(wp);
                  } catch (Exception e) {

                  }
              }
              try {
                  adapter111 = new friandrankingsearchfilter(Friend_Ranking.this, world);
                  list_item1.setAdapter(adapter111);
              } catch (Exception e) {

              }
          } else {
             /* for (int i = 0; i < number_list.size(); i++) {
                  String s1 = String.valueOf(number_list.get(i));
                  String stri = s1.substring(0, 1);
                  TextDrawable drawable = TextDrawable.builder()
                          .beginConfig()
                                  // height in px
                          .endConfig()
                          .buildRound(stri, Color.parseColor("#1665c1"));
                  url_image = "https://scontent.xx.fbcdn.net/v/t1.0-1/s200x200/375045_466002050142972_671155216_n.jpg?oh=af79baa2785248de3358ab36b949affd&oe=57EBF53A";
                  WorldPopulation wp=new WorldPopulation(array_installed.get(i),originallist.get(i).toString(),name_list.get(i).toString(),number_list.get(i).toString(),rank_list.get(i).toString(),drawable,url_image);
                  // WorldPopulation wp = new WorldPopulation(array_installed.get(i), originallist.get(i), name_list.get(i), number_list.get(i), rank_list.get(i), drawable,url_image);
                  // Binds all strings into an array
                  world.add(wp);
              }
              try {
                  adapter111 = new friandrankingsearchfilter(Friend_Ranking.this, world);
                  list_item1.setAdapter(adapter111);
              } catch (Exception e) {

              }*/

          }
          try{
        if(pref_getvalue_str("friendrank", "").equals("")){
            friends.setText("Friend Ranking");
        }
        else
        friends.setText(pref_getvalue_str("friendrank", ""));

          ranking_adapter adapter=new ranking_adapter(Friend_Ranking.this,number_list,rank_list);
        ranking_list.setAdapter(adapter);}
          catch (Exception e){

          }

          ranking_list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
              @Override
              public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

          if(cur_month_out.list8.size()>position){


              frienddd=Incoming_outgoing_calls.best_frdz;
              array_backgroundcolor.add(R.color.best_background_color);
              array_textcolor.add(R.color.best_rank);

          }
          else if((cur_month_out.list7.size()+cur_month_out.list8.size())>position)
          {

              frienddd=Incoming_outgoing_calls.igr_frdz;
              array_backgroundcolor.add(R.color.ignore_background_color);
              array_textcolor.add(R.color.igr_rank);

          }
          else if((cur_month_out.list7.size()+cur_month_out.list8.size()+cur_month_out.list6.size())>position)
          {

              frienddd=Incoming_outgoing_calls.annoy_frdz;
              array_backgroundcolor.add(R.color.annoy_background_color);
              array_textcolor.add(R.color.ann_rank);

          }
          else
          //if((cur_month_out.list7.size()+cur_month_out.list8.size()+cur_month_out.list6.size()+cur_month_out.list9.size())>position)
          {
              //view_list.setVisibility(View.GONE);

              frienddd=Incoming_outgoing_calls.lousy_frdz;
              array_backgroundcolor.add(R.color.lousy_background_color);
              array_textcolor.add(R.color.lousy_rank);
          }


                  Friend_Ranking.frd_rank = "1";

                  Intent intent = new Intent(Friend_Ranking.this, Best_friendz.class);
                  int p = position + 1;
                 // intent.putExtra("rank", "" + p);
                  intent.putExtra("number", (String) number_list.get(position));
                  intent.putExtra("friends", frienddd);
                  intent.putExtra("name", (String) name_list.get(position));
                  intent.putExtra("invite", (String) array_installed.get(position));
                  startActivity(intent);
              }
          });

//          adapter111 = new friandrankingsearchfilter(Friend_Ranking.this, world);
//          list_item1.setAdapter(adapter111);
          search_img.setOnClickListener(new View.OnClickListener() {
              @Override
              public void onClick(View v) {
//                best_adapter.list.clear();
                  search_value = 1;
                  search_edit.setVisibility(View.VISIBLE);
                  list_item1.setVisibility(View.VISIBLE);
                  ranking_list.setVisibility(View.GONE);
                //  search_btn.setVisibility(View.INVISIBLE);
                  search_img.setVisibility(View.INVISIBLE);
              }
          });

back_layout.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {

        Incoming_outgoing_calls.gps_sett = 0;
        if (search_value == 1) {
            search_edit.setVisibility(View.GONE);
            ranking_adapter adapter=new ranking_adapter(Friend_Ranking.this,number_list,rank_list);
            ranking_list.setAdapter(adapter);
//    adapter.notifyDataSetChanged();
            search_value = 0;
            search_edit.setText("");
            ranking_list.setVisibility(View.VISIBLE);
            list_item1.setVisibility(View.GONE);
            search_img.setVisibility(View.VISIBLE);
        } else {

            Intent intent = new Intent(Friend_Ranking.this, Incoming_outgoing_calls.class);
            startActivity(intent);
            finish();
        }
//        Intent intent=new Intent(Friend_Ranking.this,Incoming_outgoing_calls.class);
//        startActivity(intent);
//        Friend_Ranking.this.finish();

    }
});
        listnumite=number_list.iterator();

        listnumite1=rank_list.iterator();
        builder=new StringBuilder();
        while (listnumite.hasNext()&&listnumite1.hasNext()){
            str_phn=listnumite.next();
            str_rnk=listnumite1.next();
            builder=builder.append(str_phn).append("_").append(str_rnk).append(",");
        }
try {
    if (isNetworkAvailable()==false) {
        alert_dialog();

    } else {
        json_ranking();
    }
}
    catch (Exception e){

    }
          search_edit.addTextChangedListener(new TextWatcher() {
              @Override
              public void beforeTextChanged(CharSequence s, int start, int count, int after) {

              }

              @Override
              public void onTextChanged(CharSequence s, int start, int before, int count) {
                  String text = search_edit.getText().toString().toLowerCase(Locale.getDefault());
                  try {
                      adapter111.filter(text);
                  } catch (Exception e) {

                  }
              }

              @Override
              public void afterTextChanged(Editable s) {

              }
          });


    }

    @Override
    public void initUI() {
        back_layout=(LinearLayout)findViewById(R.id.back_layout);
        search_img=(LinearLayout)findViewById(R.id.search_img);
        friends=(TextView)findViewById(R.id.friends);
        no_msg=(TextView)findViewById(R.id.no_msg);
        ranking_list=(ListView)findViewById(R.id.ranking_list);
        list_item1=(ListView)findViewById(R.id.list_item1);
        search_edit=(EditText)findViewById(R.id.search_edit);
    }

    void json_ranking(){
        url_ranking=Base_url+"show_app_frnds.php?user_id="+pref_getvalue_str("user_id","")+"&friends_list="+ URLEncoder.encode(builder.substring(0,builder.lastIndexOf(",")));
       Log.d("urlranking",url_ranking);
        JsonObjectRequest jsonObjectRequest=new JsonObjectRequest(Request.Method.POST, url_ranking, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject jsonObject) {
                try {
                    rank_list.clear();
                    array_installed.clear();
                    arraylist_mood.clear();
                        JSONArray jsonArray=jsonObject.getJSONArray("response");

                    for (int i=0;i<jsonArray.length();i++) {
                        JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                        String number=jsonObject1.getString("phone");
                        String rank=jsonObject1.getString("rank");
                        String installed=jsonObject1.getString("installed");
                        String mood=jsonObject1.getString("mood");
                        String profile_pic=jsonObject1.getString("profile_pic");
                        rank_list.add(rank);
                        profile_image.add(profile_pic);
                       // number_list.add(number);
                        array_installed.add(""+installed);
                        arraylist_mood.add(mood);

                    }
                    ranking_adapter adapter=new ranking_adapter(Friend_Ranking.this,number_list,rank_list);
                    ranking_list.setAdapter(adapter);

                    for (int i = 0; i < number_list.size(); i++) {
                        String s1 = String.valueOf(name_list.get(i));
                        String stri = s1.substring(0, 1);
                        TextDrawable drawable = TextDrawable.builder()
                                .beginConfig()
                                        // height in px
                                .endConfig()
                                .buildRound(stri, Color.parseColor("#1665c1"));
                        url_image = "https://scontent.xx.fbcdn.net/v/t1.0-1/s200x200/375045_466002050142972_671155216_n.jpg?oh=af79baa2785248de3358ab36b949affd&oe=57EBF53A";
                        Friend_Population wp=new Friend_Population(array_installed.get(i),originallist.get(i).toString(),name_list.get(i).toString(),number_list.get(i).toString(),rank_list.get(i).toString(),drawable,url_image,array_textcolor.get(i),array_backgroundcolor.get(i), visibility_view.get(i), view_rank.get(i), view_list.get(i), friend_list.get(i), arraylist_mood.get(i));
                        // WorldPopulation wp = new WorldPopulation(array_installed.get(i), originallist.get(i), name_list.get(i), number_list.get(i), rank_list.get(i), drawable,url_image);
                        // Binds all strings into an array
                        world.add(wp);
                    }
                    try {
                        adapter111 = new friandrankingsearchfilter(Friend_Ranking.this, world);
                        list_item1.setAdapter(adapter111);
                    } catch (Exception e) {

                    }



                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {

            }
        });
        AppController.getInstance().addToRequestQueue(jsonObjectRequest);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
//        Intent intent=new Intent(Friend_Ranking.this,Incoming_outgoing_calls.class);
//        startActivity(intent);
        Incoming_outgoing_calls.gps_sett = 0;
        if (search_value == 1) {
            search_edit.setVisibility(View.GONE);
            ranking_adapter adapter=new ranking_adapter(Friend_Ranking.this,number_list,rank_list);
            ranking_list.setAdapter(adapter);
//    adapter.notifyDataSetChanged();
            search_value = 0;
            search_edit.setText("");
            search_img.setVisibility(View.VISIBLE);
            ranking_list.setVisibility(View.VISIBLE);
            list_item1.setVisibility(View.GONE);

        } else {

            Intent intent = new Intent(Friend_Ranking.this, Incoming_outgoing_calls.class);
            startActivity(intent);
            finish();
        }


      // finish();
    }

    class ranking_adapter extends BaseAdapter{
        int p=0,divide=0;
         String friend;
ArrayList number_list;
        ArrayList<String> rank_list;
Activity activity;
    int i=0,ordinal=0;
    public ranking_adapter(Activity activity, List number_list,ArrayList rank_list) {
        this.activity=activity;
        this.rank_list=rank_list;
        this.number_list= (ArrayList) number_list;


    }

    @Override
    public int getCount() {
        return number_list.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = LayoutInflater.from(activity);

        convertView = inflater.inflate(R.layout.ranking_single, null);
       TextView number = (TextView) convertView.findViewById(R.id.phn_number);
            TextView rank = (TextView) convertView.findViewById(R.id.rank);
            TextView rank_view = (TextView) convertView.findViewById(R.id.rank_view);
            TextView name = (TextView) convertView.findViewById(R.id.name);
            TextView invite_view = (TextView) convertView.findViewById(R.id.invite_view);
            TextView view_list = (TextView) convertView.findViewById(R.id.view_list);
            ImageView imageView=(ImageView)convertView.findViewById(R.id.menuimage);
            ImageView emotions=(ImageView)convertView.findViewById(R.id.emotions);
            ImageView internet_profile=(ImageView)convertView.findViewById(R.id.internet_profile);
            LinearLayout view_frdranking=(LinearLayout)convertView.findViewById(R.id.view_frdranking);
            LinearLayout rank_layout=(LinearLayout)convertView.findViewById(R.id.rank_layout);
            LinearLayout rank_layout1=(LinearLayout)convertView.findViewById(R.id.rank_layout1);

            LinearLayout ranking_layout = (LinearLayout) convertView.findViewById(R.id.ranking_layout);

        try{

            if(arraylist_mood.get(position).equals("Excellent")){
                emotions.setImageResource(R.drawable.excellent);
            }
            else  if(arraylist_mood.get(position).equals("Good")){
                emotions.setImageResource(R.drawable.good);
            }
            else  if(arraylist_mood.get(position).equals("Average")){
                emotions.setImageResource(R.drawable.average);
            }
            else  if(arraylist_mood.get(position).equals("Sad")){
                emotions.setImageResource(R.drawable.sad);
            }
            else  if(arraylist_mood.get(position).equals("Frustrated")){
                emotions.setImageResource(R.drawable.frustrated);
            }
            else {
                emotions.setImageResource(R.drawable.min);
            }
            if(array_installed.get(position).equals("1")){
                invite_view.setVisibility(View.GONE);
            }
            else {
                invite_view.setVisibility(View.VISIBLE);
            }
            invite_view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String a="1";
                    Intent intent=new Intent(Friend_Ranking.this,Send_invite.class);
                    intent.putExtra("phone_num",number_list.get(position).toString());
                    intent.putExtra("name", ""+name_list);
                    intent.putExtra("number", number_list);
                    intent.putExtra("frd_ranking",a);
                    intent.putExtra("send_nam", ""+name_list.get(position));
                    //  intent.putExtra("friends",igr_frdz);
Log.d("namelist", name_list.toString());
                    intent.putExtra("friends",friend);
                    activity.startActivity(intent);
                    finish();

                }
            });


            if (name_list.get(position).equals("No Name")) {
                if (array_installed.get(position).equals("1")) {

                    imageView.setVisibility(View.GONE);
                    internet_profile.setVisibility(View.VISIBLE);

                    imageLoader = ImageLoader.getInstance();
                    // ImageView imageView = imageViewReference.get();
                    imageLoader.init(ImageLoaderConfiguration.createDefault(Friend_Ranking.this));
                    displayImageOptions = new DisplayImageOptions.Builder().showStubImage(R.drawable.user).showImageForEmptyUri(R.drawable.user).cacheOnDisc().cacheInMemory().build();
                    //    url_image = "https://scontent.xx.fbcdn.net/v/t1.0-1/s200x200/375045_466002050142972_671155216_n.jpg?oh=af79baa2785248de3358ab36b949affd&oe=57EBF53A";
                    imageLoader.displayImage(profile_image.get(position), internet_profile, displayImageOptions);
                    //    new ImageDownloaderTask(internet_profile,position);
                    //  Log.d("prooo",profile_pic);
                } else {
                    imageView.setImageResource(R.drawable.user);
                }

            }
            else{
                String s1 =String.valueOf(name_list.get(position));
                String stri=s1.substring(0,1);
//    TextDrawable drawable = TextDrawable.builder()
//            .beginConfig()
//                    // height in px
//            .endConfig()
//            .buildRound(stri, Color.parseColor("#1665c1"));
//    imageView.setImageDrawable(drawable);

                if (array_installed.get(position).equals("0")) {
                    TextDrawable drawable = TextDrawable.builder()
                            .beginConfig()
                                    // height in px
                            .endConfig()
                            .buildRound(stri, Color.parseColor("#1665c1"));
                    imageView.setImageDrawable(drawable);

                } else {
                    imageView.setVisibility(View.GONE);
                    internet_profile.setVisibility(View.VISIBLE);

                    imageLoader = ImageLoader.getInstance();
                    // ImageView imageView = imageViewReference.get();
                    imageLoader.init(ImageLoaderConfiguration.createDefault(Friend_Ranking.this));
                    displayImageOptions = new DisplayImageOptions.Builder().showStubImage(R.drawable.user).showImageForEmptyUri(R.drawable.user).cacheOnDisc().cacheInMemory().build();
                    //    url_image = "https://scontent.xx.fbcdn.net/v/t1.0-1/s200x200/375045_466002050142972_671155216_n.jpg?oh=af79baa2785248de3358ab36b949affd&oe=57EBF53A";
                    imageLoader.displayImage(profile_image.get(position), internet_profile, displayImageOptions);
                    //    new ImageDownloaderTask(internet_profile,position);
                    // Log.d("prooo",profile_pic);
                }
            }


        }
        catch (Exception e)
        {

        }
        rank.setText(""+rank_list.get(position));
Log.d("number_list", String.valueOf(number_list.size()));

        if (position == 0) {
            view_frdranking.setVisibility(View.VISIBLE);
            view_list.setText("Best Friends");
            view_list.setBackgroundColor(Color.parseColor("#ff9f00"));
            rank_view.setBackgroundColor(Color.parseColor("#d18200"));






        }
        if (position == cur_month_out.list8.size()) {
            view_frdranking.setVisibility(View.VISIBLE);
            view_list.setText("Ignored  Friends");
            view_list.setBackgroundColor(Color.parseColor("#01C853"));
            rank_view.setBackgroundColor(Color.parseColor("#00A445"));
        }
        if (position == cur_month_out.list7.size()+cur_month_out.list8.size()){
            view_frdranking.setVisibility(View.VISIBLE);
            view_list.setText("Annoyed  Friends");
            view_list.setBackgroundColor(Color.parseColor("#dd2c00"));
            rank_view.setBackgroundColor(Color.parseColor("#af2300"));
        }
        if (position == cur_month_out.list6.size()+cur_month_out.list8.size()+ cur_month_out.list7.size()){
            view_frdranking.setVisibility(View.VISIBLE);
            view_list.setText("Lousy  Friends");
            view_list.setBackgroundColor(Color.parseColor("#36474f"));
            rank_view.setBackgroundColor(Color.parseColor("#232e33"));
        }
        p=position+1;



            if(cur_month_out.list8.size()>position){
          //  view_list.setVisibility(View.GONE);
            number.setText((String) number_list.get(position));
            p=position+1;

                rank.setTextColor(Color.parseColor("#ff9f00"));
                rank_layout.setBackgroundColor(Color.parseColor("#ffefd5"));
            friend=Incoming_outgoing_calls.best_frdz;
            name.setText((String)name_list.get(position));
            ranking_layout.setBackgroundColor(Color.parseColor("#ffffff"));
        }
        else if((cur_month_out.list7.size()+cur_month_out.list8.size())>position)
        {
            //view_list.setVisibility(View.GONE);
            number.setText((String) number_list.get(position));
            p=position+1;


            rank.setTextColor(Color.parseColor("#01C853"));
            rank_layout.setBackgroundColor(Color.parseColor("#Ceffe2"));
            friend=Incoming_outgoing_calls.igr_frdz;
            name.setText((String)name_list.get(position));

            ranking_layout.setBackgroundColor(Color.parseColor("#ffffff"));
        }
        else if((cur_month_out.list7.size()+cur_month_out.list8.size()+cur_month_out.list6.size())>position)
        {
           // view_list.setVisibility(View.GONE);
            number.setText((String) number_list.get(position));
            p=position+1;
                      rank.setTextColor(Color.parseColor("#dd2c00"));
            rank_layout.setBackgroundColor(Color.parseColor("#fec7b9"));
            name.setText((String)name_list.get(position));
            friend=Incoming_outgoing_calls.annoy_frdz;
            ranking_layout.setBackgroundColor(Color.parseColor("#ffffff"));
        }
        else
        //if((cur_month_out.list7.size()+cur_month_out.list8.size()+cur_month_out.list6.size()+cur_month_out.list9.size())>position)
        {
            //view_list.setVisibility(View.GONE);
            number.setText((String) number_list.get(position));
            p=position+1;
            rank.setTextColor(Color.parseColor("#36474f"));
            rank_layout.setBackgroundColor(Color.parseColor("#7891a1"));
            name.setText((String)name_list.get(position));
            friend=Incoming_outgoing_calls.lousy_frdz;
            ranking_layout.setBackgroundColor(Color.parseColor("#ffffff"));
        }


   /*     convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Friend_Ranking.frd_rank = "1";

                Intent intent = new Intent(activity, Best_friendz.class);
                int p = position + 1;
                intent.putExtra("rank", "" + p);
                intent.putExtra("number", (String) number_list.get(position));
                intent.putExtra("friends", friend);
                intent.putExtra("name", (String) name_list.get(position));
                startActivity(intent);
            }
        });*/
        return convertView;
    }
}
}