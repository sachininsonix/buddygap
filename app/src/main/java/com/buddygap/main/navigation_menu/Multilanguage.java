package com.buddygap.main.navigation_menu;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.buddygap.main.R;
import com.buddygap.main.home.Incoming_outgoing_calls;
import com.buddygap.main.iconstant.IConstant;
import com.buddygap.main.utils.BaseActivity;
import com.buddygap.main.utils.Notification_feel;

import java.util.Locale;

public class Multilanguage extends BaseActivity implements IConstant {
   
    int pos=0,temp=0;

Spinner language;
    LinearLayout back_layout;
    TextView lang_text,chooselang,choosefeels;
    String lang[]={"English","Portuguese","Arabic","Catalan","Chinese","French","Italian","Spanish","Korean","Indonesian","Japanese","Turkish"};
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_multilanguage);
       
        initUI();
        try{
        ArrayAdapter arrayAdapter=new ArrayAdapter(Multilanguage.this, R.layout.support_simple_spinner_dropdown_item,lang);
        language.setAdapter(arrayAdapter);}
        catch (Exception e){
            
        }
        /*if(pref_getvalue_str("position","")==null){
            ArrayAdapter arrayAdapter=new ArrayAdapter(Multilanguage.this,R.layout.support_simple_spinner_dropdown_item,lang);
            language.setAdapter(arrayAdapter);
        }
     else{
           // language.setSelection();
        }*/

        back_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Multilanguage.this, Incoming_outgoing_calls.class);
                startActivity(intent);
                finish();

            }
        });


        choosefeels.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Multilanguage.this, Notification_feel.class);
                intent.putExtra("frommulti","multilanguage");
                startActivity(intent);
                finish();

            }
        });
        

 if (pref_getvalue_str("position1", "").equals("1")) {
    String languageToLoad  = "pt";
    Locale locale = new Locale(languageToLoad);
    Locale.setDefault(locale);
    Configuration config = new Configuration();
    config.locale = locale;
    getBaseContext().getResources().updateConfiguration(config,
            getBaseContext().getResources().getDisplayMetrics());
     try {
         lang_text.setText(pref_getvalue_str("language", ""));
         chooselang.setText(pref_getvalue_str("chooselang", ""));
         choosefeels.setText(pref_getvalue_str("feels", ""));
         language.setSelection(sharedPreferences.getInt("position", 0));

     }
     catch (Exception e){

     }
       pref_setvalue_str("bestfrd", "melhor amigo");
    pref_setvalue_str("igrfrd", "amigo igonered");
    pref_setvalue_str("annoyfrd", "amigo irritado");
    pref_setvalue_str("lousyfrd", "amigo ruim");
    pref_setvalue_str("language", "la langue");
    pref_setvalue_str("chooselang", "Choisissez la langue");
    pref_setvalue_str("profile", "perfil");
    pref_setvalue_str("activity", "atividade");
    pref_setvalue_str("logout", "sair");
    pref_setvalue_str("feedback", "comentários");
    pref_setvalue_str("settings", "configurações");
    pref_setvalue_str("friendrank", "amigo Classificação");
    pref_setvalue_str("aboutus", "sobre nós");

    pref_setvalue_str("friend", "amigo");
    pref_setvalue_str("thismonth", " este mês");
    pref_setvalue_str("lastmonth", "mês passado");
     pref_setvalue_str("gettogather", "vamos nos encontrar");
     pref_setvalue_str("call", "ligar");
     pref_setvalue_str("sms", "sms");
     pref_setvalue_str("reminder", "lembrete");
     pref_setvalue_str("e_cards", "cartões");
     pref_setvalue_str("feels", "Sinta-se do Dia");
     pref_setvalue_str("feels_today", "Como foi seu dia hoje");
     pref_setvalue_str("excelent", "excelente");
     pref_setvalue_str("good", "bom");
     pref_setvalue_str("average", "média");
     pref_setvalue_str("sad", "triste");
     pref_setvalue_str("frustrated", "frustrado");
     }
else if (pref_getvalue_str("position1", "").equals("2")) {

    String languageToLoad  = "ar";
    Locale locale = new Locale(languageToLoad);
    Locale.setDefault(locale);
    Configuration config = new Configuration();
    config.locale = locale;
    getBaseContext().getResources().updateConfiguration(config,
            getBaseContext().getResources().getDisplayMetrics());
     try{
    lang_text.setText(pref_getvalue_str("language", ""));
    chooselang.setText(pref_getvalue_str("chooselang", ""));
         choosefeels.setText(pref_getvalue_str("feels", ""));
    language.setSelection(sharedPreferences.getInt("position", 0));}
     catch (Exception e){

     }

   pref_setvalue_str("SpinnerItem", language.getSelectedItem().toString());

    pref_setvalue_str("bestfrd", "افضل صديق");
    pref_setvalue_str("igrfrd", "تجنب صديق");
    pref_setvalue_str("annoyfrd", "صديق ازعاج");
    pref_setvalue_str("lousyfrd", "صديق رديء");
    pref_setvalue_str("language", "لغة");
    pref_setvalue_str("chooselang", "اختيار اللغة");
    pref_setvalue_str("profile", "الملف الشخصي");
    pref_setvalue_str("activity", "نشاط");
    pref_setvalue_str("logout", "خروج");
    pref_setvalue_str("feedback", "ردود الفعل");
    pref_setvalue_str("settings", "إعدادات");
    pref_setvalue_str("friendrank", " هل نبقى اصدقاء نشاط");
    pref_setvalue_str("aboutus", "معلومات عنا");
//   pref_setvalue_int("position",pos);
//    pref_setvalue_str("position1",""+position);
    pref_setvalue_str("friend", "هل نبقى اصدقاء");
    pref_setvalue_str("thismonth", " هذا الشهر");
    pref_setvalue_str("lastmonth", "الشهر الماضي");
     pref_setvalue_str("gettogather", "يتيح تلبية");
     pref_setvalue_str("call", "مكالمة");
     pref_setvalue_str("sms", "sms");
     pref_setvalue_str("reminder", "تذكير");
     pref_setvalue_str("e_cards", "بطاقات ترحيبية");
     pref_setvalue_str("feels","تشعر اليوم");
     pref_setvalue_str("feels_today","كيف كان يومك");
     pref_setvalue_str("excelent", "ممتاز");
     pref_setvalue_str("good", " خير");
     pref_setvalue_str("average", "متوسط");
     pref_setvalue_str("sad", "حزين");
     pref_setvalue_str("frustrated", "محبط");



}
else if (pref_getvalue_str("position1", "").equals("3")) {

    String languageToLoad  = "ca";
    Locale locale = new Locale(languageToLoad);
    Locale.setDefault(locale);
    Configuration config = new Configuration();
    config.locale = locale;
    getBaseContext().getResources().updateConfiguration(config,
            getBaseContext().getResources().getDisplayMetrics());
     try{
    lang_text.setText(pref_getvalue_str("language", ""));
    chooselang.setText(pref_getvalue_str("chooselang", ""));
         choosefeels.setText(pref_getvalue_str("feels", ""));
    language.setSelection(sharedPreferences.getInt("position", 0));}
     catch (Exception e){

     }
    pref_setvalue_str("SpinnerItem", language.getSelectedItem().toString());
    pref_setvalue_str("bestfrd", " millor amic");
    pref_setvalue_str("igrfrd", "amic ignorat");
    pref_setvalue_str("annoyfrd", "amic molest");
    pref_setvalue_str("lousyfrd", "pèssim amic");
    pref_setvalue_str("language", "llenguatge");
    pref_setvalue_str("chooselang", "triar l'idioma");
    pref_setvalue_str("profile", "perfil");
     pref_setvalue_str("logout", "tancar sessió");
    pref_setvalue_str("activity", "activitat");
    pref_setvalue_str("feedback", "realimentació");
    pref_setvalue_str("settings", "ajustos");
    pref_setvalue_str("friendrank", "amic rang");
    pref_setvalue_str("aboutus", "sobre nosaltres");
//   pref_setvalue_int("position", pos);
//    pref_setvalue_str("position1",""+position);
    pref_setvalue_str("friend", "amics");
    pref_setvalue_str("thismonth", " aquest mes");
    pref_setvalue_str("lastmonth", " el mes passat");
     pref_setvalue_str("gettogather", "quedem");
     pref_setvalue_str("call", "anomenada");
     pref_setvalue_str("sms", "sms");
     pref_setvalue_str("reminder", "recordatori");
     pref_setvalue_str("e_cards", " targetes de felicitació");
     pref_setvalue_str("feels", "Sentir del Dia");
     pref_setvalue_str("feels_today", "Com t'ha anat el dia");
     pref_setvalue_str("excelent", "excel·lent");
     pref_setvalue_str("good", "bo");
     pref_setvalue_str("average", "mitjana");
     pref_setvalue_str("sad", "trist");
     pref_setvalue_str("frustrated", "frustrat");

    




}
else if (pref_getvalue_str("position1", "").equals("4")) {


    String languageToLoad  = "zh";
    Locale locale = new Locale(languageToLoad);
    Locale.setDefault(locale);
    Configuration config = new Configuration();
    config.locale = locale;
    getBaseContext().getResources().updateConfiguration(config,
            getBaseContext().getResources().getDisplayMetrics());
     try {
         lang_text.setText(pref_getvalue_str("language", ""));
         chooselang.setText(pref_getvalue_str("chooselang", ""));
         choosefeels.setText(pref_getvalue_str("feels", ""));
         language.setSelection(sharedPreferences.getInt("position", 0));
     }
     catch (Exception e){

     }
    pref_setvalue_str("SpinnerItem", language.getSelectedItem().toString());

    pref_setvalue_str("bestfrd", "最好的朋友");
    pref_setvalue_str("igrfrd", "忽略朋友");
    pref_setvalue_str("annoyfrd", "恼火的朋友");
    pref_setvalue_str("lousyfrd", "糟糕的朋友");
    pref_setvalue_str("language", "语言");
    pref_setvalue_str("chooselang", "选择语言");
    pref_setvalue_str("profile", "轮廓");
     pref_setvalue_str("logout", "登出");
    pref_setvalue_str("activity", "活动");
    pref_setvalue_str("feedback", "反馈");
    pref_setvalue_str("settings", "设置");
    pref_setvalue_str("friendrank", "友 秩");
    pref_setvalue_str("aboutus", "关于我们");
//   pref_setvalue_int("position", pos);
//    pref_setvalue_str("position1",""+position);
    pref_setvalue_str("friend", "友");
    pref_setvalue_str("thismonth", "这个月");
    pref_setvalue_str("lastmonth", "上个月");
     pref_setvalue_str("gettogather", "咱们见面吧");
     pref_setvalue_str("call", "呼叫");
     pref_setvalue_str("sms", "sms");
     pref_setvalue_str("reminder", "提醒");
     pref_setvalue_str("e_cards", "问候卡");
     pref_setvalue_str("feels", "感受日");
     pref_setvalue_str("feels_today", "今天过得怎么样");
     pref_setvalue_str("excelent", "优");
     pref_setvalue_str("good", "良好");
     pref_setvalue_str("average", "平均");
     pref_setvalue_str("sad", "伤心");
     pref_setvalue_str("frustrated", "失意");




    



}

        else if (pref_getvalue_str("position1", "").equals("5"))
{
    String languageToLoad  = "fr";
    Locale locale = new Locale(languageToLoad);
    Locale.setDefault(locale);
    Configuration config = new Configuration();
    config.locale = locale;
    getBaseContext().getResources().updateConfiguration(config,
            getBaseContext().getResources().getDisplayMetrics());
    try{
    pref_getvalue_str("SpinnerItem", "");
        lang_text.setText(pref_getvalue_str("language", ""));
    chooselang.setText(pref_getvalue_str("chooselang", ""));
        choosefeels.setText(pref_getvalue_str("feels", ""));
    }
    catch (Exception e){

    }
    pref_setvalue_str("bestfrd", "meilleur ami");
    pref_setvalue_str("igrfrd", "ami ignoré");
    pref_setvalue_str("annoyfrd", "ami ennuyé");
    pref_setvalue_str("lousyfrd", "ami moche");
    pref_setvalue_str("language", "la langue");
    pref_setvalue_str("chooselang", "Choisissez la langue");
    pref_setvalue_str("profile", "Profil");
     pref_setvalue_str("logout", "se déconnecter");
    pref_setvalue_str("activity", "activité");
    pref_setvalue_str("feedback", "retour d\'information");
    pref_setvalue_str("settings", "paramètres");
    pref_setvalue_str("friendrank", "copains Rang");
    pref_setvalue_str("aboutus", "à propos de nous");
    pref_setvalue_str("friend", "ami");
    pref_setvalue_str("thismonth", "ce mois-ci");
    pref_setvalue_str("lastmonth", "le mois dernier");

    pref_setvalue_str("gettogather", "on se retrouve");
    pref_setvalue_str("call", "appel");
    pref_setvalue_str("sms", "sms");
    pref_setvalue_str("reminder", "rappel");
    pref_setvalue_str("e_cards", "cartes de voeux");
    pref_setvalue_str("feels", "Sentez-vous de la journée");
    pref_setvalue_str("feels_today", "Comment s'est passée ta journée");
    pref_setvalue_str("excelent", "excellent");
    pref_setvalue_str("good", "bon");
    pref_setvalue_str("average", "moyenne");
    pref_setvalue_str("sad", "triste");
    pref_setvalue_str("frustrated", "frustré");

    language.setSelection(sharedPreferences.getInt("position", 0));
   // pref_setvalue_str("position","5");
    


}
else if (pref_getvalue_str("position1", "").equals("6")) {

    String languageToLoad  = "it";
    Locale locale = new Locale(languageToLoad);
    Locale.setDefault(locale);
    Configuration config = new Configuration();
    config.locale = locale;
    getBaseContext().getResources().updateConfiguration(config,
            getBaseContext().getResources().getDisplayMetrics());
     try{
    pref_getvalue_str("SpinnerItem", "");
         lang_text.setText(pref_getvalue_str("language", ""));
    chooselang.setText(pref_getvalue_str("chooselang", ""));
         choosefeels.setText(pref_getvalue_str("feels", ""));
    language.setSelection(sharedPreferences.getInt("position", 0));
     }
     catch (Exception e){

     }
    pref_setvalue_str("thismonth", "questo mese");
    pref_setvalue_str("lastmonth", "lo scorso mese");
    pref_setvalue_str("bestfrd", "migliore amico");
    pref_setvalue_str("igrfrd", "ignorato amico");
    pref_setvalue_str("annoyfrd", "infastidito amico");
    pref_setvalue_str("lousyfrd", "schifoso amico");
    pref_setvalue_str("language", "Lingua");
    pref_setvalue_str("chooselang", "scegli la lingua");
    pref_setvalue_str("profile", "profilo");
     pref_setvalue_str("logout", "Disconnettersi");
    pref_setvalue_str("activity", "attività");
    pref_setvalue_str("feedback", "risposta");
    pref_setvalue_str("settings", "impostazioni");
    pref_setvalue_str("friendrank", "Amici rango");
    pref_setvalue_str("aboutus", "Riguardo a noi");
//   pref_setvalue_int("position",pos);
//    pref_setvalue_str("position1",""+position);
    pref_setvalue_str("friend", "amico");
     pref_setvalue_str("gettogather", "Incontriamoci");
     pref_setvalue_str("call", "chiamata");
     pref_setvalue_str("sms", "sms");
     pref_setvalue_str("reminder", "promemoria");
     pref_setvalue_str("e_cards", " biglietti d\'auguri");
     pref_setvalue_str("feels", "Sentire del giorno");
     pref_setvalue_str("feels_today", "Com'è andata oggi");
     pref_setvalue_str("excelent", "eccellente");
     pref_setvalue_str("good", "buono");
     pref_setvalue_str("average", "media");
     pref_setvalue_str("sad", "triste");
     pref_setvalue_str("frustrated", "Frustrato");

}
else if (pref_getvalue_str("position1", "").equals("7")) {
        String languageToLoad  = "es";
        Locale locale = new Locale(languageToLoad);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        getBaseContext().getResources().updateConfiguration(config,
                getBaseContext().getResources().getDisplayMetrics());
     try{
    pref_getvalue_str("SpinnerItem", "");
         lang_text.setText(pref_getvalue_str("language", ""));
    chooselang.setText(pref_getvalue_str("chooselang", ""));
         choosefeels.setText(pref_getvalue_str("feels", ""));
    language.setSelection(sharedPreferences.getInt("position", 0));
     }
     catch (Exception e){

     }
        pref_setvalue_str("SpinnerItem", language.getSelectedItem().toString());

        pref_setvalue_str("bestfrd", " mejor amiga");
        pref_setvalue_str("igrfrd", "amigo ignorado");
        pref_setvalue_str("annoyfrd", "amigo molesto");
        pref_setvalue_str("lousyfrd", "pésimo amigo");
        pref_setvalue_str("language", "lengua");
        pref_setvalue_str("chooselang", "elige lengua");

        pref_setvalue_str("profile", "perfil");
     pref_setvalue_str("logout", "cerrar sesión");
        pref_setvalue_str("activity", "actividad");
        pref_setvalue_str("feedback", "realimentación");
        pref_setvalue_str("settings", "ajustes");
        pref_setvalue_str("friendrank", "amigos Rango");
        pref_setvalue_str("aboutus", "sobre nosotros");
//
//   pref_setvalue_int("position", pos);
//        pref_setvalue_str("position1",""+position);
        pref_setvalue_str("friend", "Amigos");
        pref_setvalue_str("thismonth", "este mes");
        pref_setvalue_str("lastmonth", "el mes pasado");

     pref_setvalue_str("gettogather", " vamos a quedar");
     pref_setvalue_str("call", "llamada");
     pref_setvalue_str("sms", "sms");
     pref_setvalue_str("reminder", "recordatorio");
     pref_setvalue_str("e_cards", "tarjetas de felicitación");
     pref_setvalue_str("feels", "Sentir del Día");
     pref_setvalue_str("feels_today", "Como estuvo hoy tu día");
     pref_setvalue_str("excelent", "excelente");
     pref_setvalue_str("good", "bueno");
     pref_setvalue_str("average", "promedio");
     pref_setvalue_str("sad", "triste");
     pref_setvalue_str("frustrated", "frustrado");

}
else if (pref_getvalue_str("position1", "").equals("8")) {


        String languageToLoad  = "ko";
        Locale locale = new Locale(languageToLoad);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        getBaseContext().getResources().updateConfiguration(config,
                getBaseContext().getResources().getDisplayMetrics());
     try{
    pref_getvalue_str("SpinnerItem", "");
         lang_text.setText(pref_getvalue_str("language", ""));
    chooselang.setText(pref_getvalue_str("chooselang", ""));
         choosefeels.setText(pref_getvalue_str("feels", ""));
    language.setSelection(sharedPreferences.getInt("position", 0));
     }
     catch (Exception e){

     }
        pref_setvalue_str("bestfrd", "가장 친한 친구 ");
        pref_setvalue_str("igrfrd", "무시 친구");
        pref_setvalue_str("annoyfrd", "짜증 친구");
        pref_setvalue_str("lousyfrd", "형편없는 친구");
        pref_setvalue_str("language", "언어");
        pref_setvalue_str("chooselang", "언어를 선택");

        pref_setvalue_str("profile", "윤곽");

        pref_setvalue_str("activity", "활동");
        pref_setvalue_str("feedback", "피드백");
     pref_setvalue_str("logout", "로그 아웃");
        pref_setvalue_str("settings", "설정");
        pref_setvalue_str("friendrank", "친구 계급");
    pref_setvalue_str("aboutus", "우리에 대해");
//       pref_setvalue_int("position", pos);
//        pref_setvalue_str("position1",""+position);
        pref_setvalue_str("friend", "친구");
        pref_setvalue_str("thismonth", "이번 달");
        pref_setvalue_str("lastmonth", "지난 달");
     pref_setvalue_str("gettogather", "만나자");
     pref_setvalue_str("call", "요구");
     pref_setvalue_str("sms", "sms");
     pref_setvalue_str("reminder", "조언");
     pref_setvalue_str("e_cards", "인사말 카드");
     pref_setvalue_str("feels", "오늘의 기분");
     pref_setvalue_str("feels_today", "오늘은 어땠 니");
     pref_setvalue_str("excelent", "Excellent");
     pref_setvalue_str("good", "좋은");
     pref_setvalue_str("average", "평균");
     pref_setvalue_str("sad", "슬픈");
     pref_setvalue_str("frustrated", "실망한");

    }else if (pref_getvalue_str("position1", "").equals("9")) {

        String languageToLoad  = "id";
        Locale locale = new Locale(languageToLoad);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        getBaseContext().getResources().updateConfiguration(config,
        getBaseContext().getResources().getDisplayMetrics());
     try{
    pref_getvalue_str("SpinnerItem", "");
         lang_text.setText(pref_getvalue_str("language", ""));
    chooselang.setText(pref_getvalue_str("chooselang", ""));
         choosefeels.setText(pref_getvalue_str("feels", ""));
    language.setSelection(sharedPreferences.getInt("position", 0));
     }
     catch (Exception e){

     }
        pref_setvalue_str("bestfrd", " sahabat");
        pref_setvalue_str("igrfrd", "teman diabaikan");
        pref_setvalue_str("annoyfrd", "teman terganggu");
        pref_setvalue_str("lousyfrd", "teman buruk");
        pref_setvalue_str("language", "bahasa");
        pref_setvalue_str("chooselang", "Pilih bahasa");

        pref_setvalue_str("profile", "Profil");
     pref_setvalue_str("logout", "keluar");
        pref_setvalue_str("activity", "aktivitas");
        pref_setvalue_str("feedback", "umpan balik");
        pref_setvalue_str("settings", "pengaturan");
        pref_setvalue_str("friendrank", "teman Pangkat");
        pref_setvalue_str("aboutus", "tentang kami");
//       pref_setvalue_int("position", pos);
//        pref_setvalue_str("position1",""+position);
        pref_setvalue_str("friend", "temans");
        pref_setvalue_str("thismonth", "bulan ini");
        pref_setvalue_str("lastmonth", "bulan lalu");
     pref_setvalue_str("gettogather", "memungkinkan memenuhi");
     pref_setvalue_str("call", "panggilan");
     pref_setvalue_str("sms", "sms");
     pref_setvalue_str("reminder", "peringatan");
     pref_setvalue_str("e_cards", "kartu ucapan");
     pref_setvalue_str("feels", "Rasakan Hari");
     pref_setvalue_str("feels_today", "Bagaimana harimu hari ini");
     pref_setvalue_str("excelent", "unggul");
     pref_setvalue_str("good", "baik");
     pref_setvalue_str("average", "rata-rata");
     pref_setvalue_str("sad", "sedih");
     pref_setvalue_str("frustrated", "frustrasi");

        



}else if (pref_getvalue_str("position1", "").equals("10")) {

        String languageToLoad  = "ja";
        Locale locale = new Locale(languageToLoad);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        getBaseContext().getResources().updateConfiguration(config,
                getBaseContext().getResources().getDisplayMetrics());
     try{
    pref_getvalue_str("SpinnerItem", "");
         lang_text.setText(pref_getvalue_str("language", ""));
    chooselang.setText(pref_getvalue_str("chooselang", ""));
         choosefeels.setText(pref_getvalue_str("feels", ""));
    language.setSelection(sharedPreferences.getInt("position", 0));
     }
     catch (Exception e){

     }
        pref_setvalue_str("bestfrd", " 親友");
        pref_setvalue_str("igrfrd", "無視友人");
        pref_setvalue_str("annoyfrd", "イライラ友人");
        pref_setvalue_str("lousyfrd", "お粗末な友人");
        pref_setvalue_str("language", "言語");
        pref_setvalue_str("chooselang", "言語を選択");

        pref_setvalue_str("profile", "プロフィール");
     pref_setvalue_str("logout", "ログアウト");
        pref_setvalue_str("activity", "アクティビティ");
        pref_setvalue_str("feedback", "フィードバック");
        pref_setvalue_str("settings", "設定");
        pref_setvalue_str("friendrank", "フレンズ ランク");
        pref_setvalue_str("aboutus", "私たちに関しては");
//       pref_setvalue_int("position", pos);
//        pref_setvalue_str("position1",""+position);
        pref_setvalue_str("friend", "フレンズ");
        pref_setvalue_str("thismonth", "今月");
        pref_setvalue_str("lastmonth", "先月");
     pref_setvalue_str("gettogather", "会いましょう");
     pref_setvalue_str("call", "コー\u200B\u200Bル");
     pref_setvalue_str("sms", "sms");
     pref_setvalue_str("reminder", "リマインダー");
     pref_setvalue_str("e_cards", "グリーティングカード");
     pref_setvalue_str("feels", "その日のフィール");
     pref_setvalue_str("feels_today", "今日はどうだった");
     pref_setvalue_str("excelent", "優れました");
     pref_setvalue_str("good", "グッド");
     pref_setvalue_str("average", "平均");
     pref_setvalue_str("sad", "悲しいです");
     pref_setvalue_str("frustrated", "欲求不満の");


        



}else if (pref_getvalue_str("position1", "").equals("11")) {



            String languageToLoad  = "tr";
            Locale locale = new Locale(languageToLoad);
            Locale.setDefault(locale);
            Configuration config = new Configuration();
            config.locale = locale;
            getBaseContext().getResources().updateConfiguration(config,
                    getBaseContext().getResources().getDisplayMetrics());
     try{
    pref_getvalue_str("SpinnerItem", "");
         lang_text.setText(pref_getvalue_str("language", ""));
    chooselang.setText(pref_getvalue_str("chooselang", ""));
         choosefeels.setText(pref_getvalue_str("feels", ""));
    language.setSelection(sharedPreferences.getInt("position", 0));
                       pref_setvalue_str("bestfrd", " en iyi arkadaş");
     }
     catch (Exception e){

     }
            pref_setvalue_str("igrfrd", " igonered arkadaş");
            pref_setvalue_str("annoyfrd", "rahatsız arkadaş");
            pref_setvalue_str("lousyfrd", " berbat bir arkadaş");
            pref_setvalue_str("language", "dil");
            pref_setvalue_str("chooselang", "dil seçiniz");

            pref_setvalue_str("profile", "profil");
     pref_setvalue_str("logout", "çıkış Yap");
            pref_setvalue_str("activity", "etkinlik");
            pref_setvalue_str("feedback", " geri bildirim");
            pref_setvalue_str("settings", "ayarlar");
            pref_setvalue_str("friendrank", "arkadaş rütbe");
            pref_setvalue_str("aboutus", "Hakkımızda");
//           pref_setvalue_int("position", pos);
//            pref_setvalue_str("position1",""+position);
            pref_setvalue_str("friend", "arkadaş");
            pref_setvalue_str("thismonth", " bu ay");
            pref_setvalue_str("lastmonth", "geçen  ay");
     pref_setvalue_str("gettogather", "buluşalım");
     pref_setvalue_str("call", "arama");
     pref_setvalue_str("sms", "SMS");
     pref_setvalue_str("reminder", "hatırlatma");
     pref_setvalue_str("e_cards", "tebrik kartı");
     pref_setvalue_str("feels", "Günün hissedin");
     pref_setvalue_str("feels_today", "günün nasıl bugün");
     pref_setvalue_str("excelent", "mükemmel");
     pref_setvalue_str("good", "iyi");
     pref_setvalue_str("average", "ortalama");
     pref_setvalue_str("sad", "üzgün");
     pref_setvalue_str("frustrated", "Frustrated");




}


        else{
            String languageToLoad  = "en";
            Locale locale = new Locale(languageToLoad);
            Locale.setDefault(locale);
            Configuration config = new Configuration();
            config.locale = locale;
            getBaseContext().getResources().updateConfiguration(config,
                    getBaseContext().getResources().getDisplayMetrics());
try{
            lang_text.setText(pref_getvalue_str("language", ""));
            chooselang.setText(pref_getvalue_str("chooselang", ""));
            choosefeels.setText(pref_getvalue_str("feels", ""));
}
catch (Exception e){

}
            pref_setvalue_str("bestfrd", "Best Friends");
            pref_setvalue_str("igrfrd", "Ignored Friends");
            pref_setvalue_str("annoyfrd", "Annoyed Friends");
            pref_setvalue_str("lousyfrd", "Lousy Friends");
            pref_setvalue_str("language", "Language");
            pref_setvalue_str("chooselang", "Choose Language");
            pref_setvalue_str("profile", "Profile");
            pref_setvalue_str("activity", "Activity");
            pref_setvalue_str("feedback", "Feedback");
     pref_setvalue_str("logout", "Logout");
            pref_setvalue_str("settings", "Settings");
            pref_setvalue_str("friendrank", "Friend Rank");
            pref_setvalue_str("aboutus", "About us");
            pref_setvalue_str("friend", "Friends");
            pref_setvalue_str("thismonth", "This Month");
            pref_setvalue_str("lastmonth", "Last Month");
     pref_setvalue_str("gettogather", "Let's Meet");
     pref_setvalue_str("call", "Call");
     pref_setvalue_str("sms", "SMS");
     pref_setvalue_str("reminder", "Reminder");
     pref_setvalue_str("e_cards", "E-Cards");
     pref_setvalue_str("feels", "Feel of the Day");
     pref_setvalue_str("feels_today", "How was your day Today");
     pref_setvalue_str("excelent", "Excellent");
     pref_setvalue_str("good", "Good");
     pref_setvalue_str("average", "Average");
     pref_setvalue_str("sad", "Sad");
     pref_setvalue_str("frustrated", "Frustrated");

            language.setSelection(sharedPreferences.getInt("position",0));
            //   pref_setvalue_str("position","0");


        }

//        lang_text.setText(pref_getvalue_str("language",""));
//        chooselang.setText(pref_getvalue_str("chooselang",""));
//language.setSelection(Integer.parseInt(pref_getvalue_str("SpinnerItem","")));
        language.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
               //
                pos=position;

                if(pos==0){

                    String languageToLoad  = "en";
                    Locale locale = new Locale(languageToLoad);
                    Locale.setDefault(locale);
                    Configuration config = new Configuration();
                    config.locale = locale;
                    getBaseContext().getResources().updateConfiguration(config,
                            getBaseContext().getResources().getDisplayMetrics());
                    pref_setvalue_str("SpinnerItem", language.getSelectedItem().toString());
pref_setvalue_str("bestfrd", "Best Friends");
pref_setvalue_str("igrfrd", "Ignored Friends");
pref_setvalue_str("annoyfrd", "Annoyed Friends");
pref_setvalue_str("lousyfrd", "Lousy Friends");
pref_setvalue_str("language", "Language");
pref_setvalue_str("chooselang", "Choose Language");
                    lang_text.setText("Language");
                    chooselang.setText("Choose Language");
                    choosefeels.setText("Feel of the Day");

                    pref_setvalue_str("logout", "Logout");
                    pref_setvalue_str("profile", "Profile");
                    pref_setvalue_str("activity", "Activity");
                    pref_setvalue_str("feedback", "Feedback");
                    pref_setvalue_str("settings", "Settings");
                    pref_setvalue_str("friendrank", "Friend Rank");
                    pref_setvalue_str("aboutus", "About us");
                   pref_setvalue_int("position", pos);
                    pref_setvalue_str("thismonth", "This Month");
                    pref_setvalue_str("lastmonth", "Last Month");
                    pref_setvalue_str("position1", "" + position);
                    pref_setvalue_str("friend", "Friends");
                    pref_setvalue_str("gettogather", "Let's Meet");
                    pref_setvalue_str("call", "Call");
                    pref_setvalue_str("sms", "SMS");
                    pref_setvalue_str("reminder", "Reminder");
                    pref_setvalue_str("e_cards", "E-Cards");
                    pref_setvalue_str("feels", "Feel of the Day");
                    pref_setvalue_str("feels_today", "How was your day Today");
                    pref_setvalue_str("excelent", "Excellent");
                    pref_setvalue_str("good", "Good");
                    pref_setvalue_str("average", "Average");
                    pref_setvalue_str("sad", "Sad");
                    pref_setvalue_str("frustrated", "Frustrated");


                }
                else if (pos==1){
                    String languageToLoad  = "pt";
                    Locale locale = new Locale(languageToLoad);
                    Locale.setDefault(locale);
                    Configuration config = new Configuration();
                    config.locale = locale;
                    getBaseContext().getResources().updateConfiguration(config,
                            getBaseContext().getResources().getDisplayMetrics());
                    lang_text.setText("língua");
                    pref_setvalue_str("SpinnerItem", language.getSelectedItem().toString());
                    chooselang.setText("escolha o idioma");
                    choosefeels.setText("Sinta-se do Dia");
                    pref_setvalue_str("bestfrd", "melhor amigo");
                    pref_setvalue_str("igrfrd", "amigo igonered");
                    pref_setvalue_str("annoyfrd", "amigo irritado");
                    pref_setvalue_str("lousyfrd", "amigo ruim");
                    pref_setvalue_str("logout", "sair");
                    pref_setvalue_str("language", "la langue");
                    pref_setvalue_str("chooselang", "Choisissez la langue");
                    pref_setvalue_str("profile", "perfil");
                    pref_setvalue_str("activity", "atividade");
                    pref_setvalue_str("feedback", "comentários");
                    pref_setvalue_str("settings", "configurações");
                    pref_setvalue_str("friendrank", "amigo Classificação");
                    pref_setvalue_str("aboutus", "sobre nós");
                   pref_setvalue_int("position", pos);
                    pref_setvalue_str("position1", "" + position);
                    pref_setvalue_str("friend", "amigo");
                    pref_setvalue_str("thismonth", " este mês");
                    pref_setvalue_str("lastmonth", "mês passado");
                    pref_setvalue_str("gettogather", "vamos nos encontrar");
                    pref_setvalue_str("call", "ligar");
                    pref_setvalue_str("sms", "sms");
                    pref_setvalue_str("reminder", "lembrete");
                    pref_setvalue_str("e_cards", "cartões");

                    pref_setvalue_str("gettogather", "vamos nos encontrar");
                    pref_setvalue_str("call", "ligar");
                    pref_setvalue_str("sms", "sms");
                    pref_setvalue_str("reminder", "lembrete");
                    pref_setvalue_str("e_cards", "cartões");
                    pref_setvalue_str("feels", "Sinta-se do Dia");
                    pref_setvalue_str("feels_today", "Como foi seu dia hoje");
                    pref_setvalue_str("excelent", "excelente");
                    pref_setvalue_str("good", "bom");
                    pref_setvalue_str("average", "média");
                    pref_setvalue_str("sad", "triste");
                    pref_setvalue_str("frustrated", "frustrado");



                }
                else if (pos==2){
                    String languageToLoad  = "ar";
                    Locale locale = new Locale(languageToLoad);
                    Locale.setDefault(locale);
                    Configuration config = new Configuration();
                    config.locale = locale;
                    getBaseContext().getResources().updateConfiguration(config,
                            getBaseContext().getResources().getDisplayMetrics());
                    lang_text.setText("لغة");
                    pref_setvalue_str("SpinnerItem", language.getSelectedItem().toString());
                    chooselang.setText("اختيار اللغة");
                    choosefeels.setText("تشعر اليوم");
                    pref_setvalue_str("bestfrd", "افضل صديق");
                    pref_setvalue_str("igrfrd", "تجنب صديق");
                    pref_setvalue_str("annoyfrd", "صديق ازعاج");
                    pref_setvalue_str("lousyfrd", "صديق رديء");
                    pref_setvalue_str("language", "لغة");
                    pref_setvalue_str("chooselang", "اختيار اللغة");
                    pref_setvalue_str("profile", "الملف الشخصي");
                    pref_setvalue_str("logout", "خروج");
                    pref_setvalue_str("activity", "نشاط");
                    pref_setvalue_str("feedback", "ردود الفعل");
                    pref_setvalue_str("settings", "إعدادات");
                    pref_setvalue_str("friendrank", " هل نبقى اصدقاء نشاط");
                    pref_setvalue_str("aboutus", "معلومات عنا");
//   pref_setvalue_int("position",pos);
//    pref_setvalue_str("position1",""+position);
                    pref_setvalue_str("friend", "هل نبقى اصدقاء");
                    pref_setvalue_str("thismonth", " هذا الشهر");
                    pref_setvalue_str("lastmonth", "الشهر الماضي");
                    pref_setvalue_str("gettogather", "يتيح تلبية");
                    pref_setvalue_str("call", "مكالمة");
                    pref_setvalue_str("sms", "sms");
                    pref_setvalue_str("reminder", "تذكير");
                    pref_setvalue_str("e_cards", "بطاقات ترحيبية");
                    pref_setvalue_str("feels","تشعر اليوم");
                    pref_setvalue_str("feels_today","كيف كان يومك");
                    pref_setvalue_str("excelent", "ممتاز");
                    pref_setvalue_str("good", " خير");
                    pref_setvalue_str("average", "متوسط");
                    pref_setvalue_str("sad", "حزين");
                    pref_setvalue_str("frustrated", "محبط");



                   pref_setvalue_int("position", pos);
                    pref_setvalue_str("position1", "" + position);

                    



                }
                else if (pos==3){
                    String languageToLoad  = "ca";
                    Locale locale = new Locale(languageToLoad);
                    Locale.setDefault(locale);
                    Configuration config = new Configuration();
                    config.locale = locale;
                    getBaseContext().getResources().updateConfiguration(config,
                            getBaseContext().getResources().getDisplayMetrics());
                    lang_text.setText("llenguatge");
                    pref_setvalue_str("SpinnerItem", language.getSelectedItem().toString());
                    chooselang.setText("triar l'idioma");
                    choosefeels.setText("Sentir del Dia");
                    pref_setvalue_str("bestfrd", " millor amic");
                    pref_setvalue_str("igrfrd", "amic ignorat");
                    pref_setvalue_str("annoyfrd", "amic molest");
                    pref_setvalue_str("lousyfrd", "pèssim amic");
                    pref_setvalue_str("language", "llenguatge");
                    pref_setvalue_str("chooselang", "triar l'idioma");

                    pref_setvalue_str("profile", "perfil");
                    pref_setvalue_str("logout", "tancar sessió");
                    pref_setvalue_str("activity", "activitat");
                    pref_setvalue_str("feedback", "realimentació");
                    pref_setvalue_str("settings", "ajustos");
                    pref_setvalue_str("friendrank", "amic rang");

                    pref_setvalue_str("aboutus", "sobre nosaltres");
                   pref_setvalue_int("position", pos);
                    pref_setvalue_str("position1", "" + position);
                    pref_setvalue_str("friend", "amics");
                    pref_setvalue_str("thismonth", " aquest mes");
                    pref_setvalue_str("lastmonth", " el mes passat");

                    pref_setvalue_str("gettogather", "quedem");
                    pref_setvalue_str("call", "anomenada");
                    pref_setvalue_str("sms", "sms");
                    pref_setvalue_str("reminder", "recordatori");
                    pref_setvalue_str("e_cards", " targetes de felicitació");
                    pref_setvalue_str("feels", "Sentir del Dia");
                    pref_setvalue_str("feels_today", "Com t'ha anat el dia");
                    pref_setvalue_str("excelent", "excel·lent");
                    pref_setvalue_str("good", "bo");
                    pref_setvalue_str("average", "mitjana");
                    pref_setvalue_str("sad", "trist");
                    pref_setvalue_str("frustrated", "frustrat");
                    



                }


                else if (pos==4){
                    String languageToLoad  = "zh";
                    Locale locale = new Locale(languageToLoad);
                    Locale.setDefault(locale);
                    Configuration config = new Configuration();
                    config.locale = locale;
                    getBaseContext().getResources().updateConfiguration(config,
                            getBaseContext().getResources().getDisplayMetrics());
                    pref_setvalue_str("bestfrd", "最好的朋友");
                    pref_setvalue_str("igrfrd", "忽略朋友");
                    pref_setvalue_str("annoyfrd", "恼火的朋友");
                    pref_setvalue_str("lousyfrd", "糟糕的朋友");
                    pref_setvalue_str("language", "语言");
                    pref_setvalue_str("chooselang", "选择语言");
                    pref_setvalue_str("profile", "轮廓");
                    pref_setvalue_str("activity", "活动");
                    pref_setvalue_str("feedback", "反馈");
                    pref_setvalue_str("settings", "设置");
                    pref_setvalue_str("logout", "登出");
                    pref_setvalue_str("friendrank", "友 秩");
                    pref_setvalue_str("aboutus", "关于我们");
//   pref_setvalue_int("position", pos);
//    pref_setvalue_str("position1",""+position);
                    pref_setvalue_str("friend", "友");
                    pref_setvalue_str("thismonth", "这个月");
                    pref_setvalue_str("lastmonth", "上个月");
                    lang_text.setText("语言");
                    pref_setvalue_str("SpinnerItem", language.getSelectedItem().toString());
                    chooselang.setText("选择语言");
                    choosefeels.setText("感受日");
                    pref_setvalue_str("gettogather", "咱们见面吧");
                    pref_setvalue_str("call", "呼叫");
                    pref_setvalue_str("sms", "sms");
                    pref_setvalue_str("reminder", "提醒");
                    pref_setvalue_str("e_cards", "问候卡");
                    pref_setvalue_str("feels", "感受日");
                    pref_setvalue_str("feels_today", "今天过得怎么样");
                    pref_setvalue_str("excelent", "优");
                    pref_setvalue_str("good", "良好");
                    pref_setvalue_str("average", "平均");
                    pref_setvalue_str("sad", "伤心");
                    pref_setvalue_str("frustrated", "失意");

                   pref_setvalue_int("position", pos);
                   pref_setvalue_str("position1", "" + position);

                    



                }
                else if (pos==5){
                    String languageToLoad  = "fr";
                    Locale locale = new Locale(languageToLoad);
                    Locale.setDefault(locale);
                    Configuration config = new Configuration();
                    config.locale = locale;
                    getBaseContext().getResources().updateConfiguration(config,
                            getBaseContext().getResources().getDisplayMetrics());
                    lang_text.setText("la langue");
                    pref_setvalue_str("SpinnerItem", language.getSelectedItem().toString());
                    chooselang.setText("Choisissez la langue");
                    choosefeels.setText("Sentez-vous de la journée");
                    pref_setvalue_str("bestfrd", "meilleur ami");
                    pref_setvalue_str("igrfrd", "ami ignoré");
                    pref_setvalue_str("annoyfrd", "ami ennuyé");
                    pref_setvalue_str("lousyfrd", "ami moche");
                    pref_setvalue_str("language", "la langue");
                    pref_setvalue_str("chooselang", "Choisissez la langue");
                    pref_setvalue_str("profile", "Profil");
                    pref_setvalue_str("logout", "se déconnecter");
                    pref_setvalue_str("activity", "activité");
                    pref_setvalue_str("feedback", "retour d\'information");
                    pref_setvalue_str("settings", "paramètres");
                    pref_setvalue_str("friendrank", "copains Rang");
                    pref_setvalue_str("aboutus", "à propos de nous");
                   pref_setvalue_int("position", pos);
                    pref_setvalue_str("position1", "" + position);
                    pref_setvalue_str("friend", "ami");
                    pref_setvalue_str("thismonth", "ce mois-ci");
                    pref_setvalue_str("lastmonth", "le mois dernier");
                    pref_setvalue_str("gettogather", "on se retrouve");
                    pref_setvalue_str("call", "appel");
                    pref_setvalue_str("sms", "sms");
                    pref_setvalue_str("reminder", "rappel");
                    pref_setvalue_str("e_cards", "cartes de voeux");
                    pref_setvalue_str("feels", "Sentez-vous de la journée");
                    pref_setvalue_str("feels_today", "Comment s'est passée ta journée");
                    pref_setvalue_str("excelent", "excellent");
                    pref_setvalue_str("good", "bon");
                    pref_setvalue_str("average", "moyenne");
                    pref_setvalue_str("sad", "triste");
                    pref_setvalue_str("frustrated", "frustré");

                    



                }
                else if (pos==6){

                    String languageToLoad  = "it";
                    Locale locale = new Locale(languageToLoad);
                    Locale.setDefault(locale);
                    Configuration config = new Configuration();
                    config.locale = locale;
                    getBaseContext().getResources().updateConfiguration(config,
                            getBaseContext().getResources().getDisplayMetrics());
                    lang_text.setText("Lingua");
                    chooselang.setText("scegli la lingua");
                    choosefeels.setText("Sentire del giorno");
                    pref_setvalue_str("thismonth", "questo mese");
                    pref_setvalue_str("lastmonth", "lo scorso mese");
                    pref_setvalue_str("bestfrd", "migliore amico");
                    pref_setvalue_str("igrfrd", "ignorato amico");
                    pref_setvalue_str("annoyfrd", "infastidito amico");
                    pref_setvalue_str("lousyfrd", "schifoso amico");
                    pref_setvalue_str("language", "Lingua");
                    pref_setvalue_str("chooselang", "scegli la lingua");
                    pref_setvalue_str("profile", "profilo");
                    pref_setvalue_str("logout", "Disconnettersi");
                    pref_setvalue_str("activity", "attività");
                    pref_setvalue_str("feedback", "risposta");
                    pref_setvalue_str("settings", "impostazioni");
                    pref_setvalue_str("friendrank", "Amici rango");
                    pref_setvalue_str("aboutus", "Riguardo a noi");
                   pref_setvalue_int("position", pos);
                    pref_setvalue_str("position1", "" + position);
                    pref_setvalue_str("friend", "amico");
                    pref_setvalue_str("gettogather", "Incontriamoci");
                    pref_setvalue_str("call", "chiamata");
                    pref_setvalue_str("sms", "sms");
                    pref_setvalue_str("reminder", "promemoria");
                    pref_setvalue_str("e_cards", " biglietti d\'auguri");
                    pref_setvalue_str("feels", "Sentire del giorno");
                    pref_setvalue_str("feels_today", "Com'è andata oggi");
                    pref_setvalue_str("excelent", "eccellente");
                    pref_setvalue_str("good", "buono");
                    pref_setvalue_str("average", "media");
                    pref_setvalue_str("sad", "triste");
                    pref_setvalue_str("frustrated", "Frustrato");

                    

                }
                else if (pos==7){
                    //spanish
                    String languageToLoad  = "es";
                    Locale locale = new Locale(languageToLoad);
                    Locale.setDefault(locale);
                    Configuration config = new Configuration();
                    config.locale = locale;
                    getBaseContext().getResources().updateConfiguration(config,
                            getBaseContext().getResources().getDisplayMetrics());
                    lang_text.setText("lengua");
                    pref_setvalue_str("SpinnerItem", language.getSelectedItem().toString());
                    chooselang.setText("elige lengua");
                    choosefeels.setText("Sentir del Día");
                    pref_setvalue_str("bestfrd", " mejor amiga");
                    pref_setvalue_str("igrfrd", "amigo ignorado");
                    pref_setvalue_str("annoyfrd", "amigo molesto");
                    pref_setvalue_str("lousyfrd", "pésimo amigo");
                    pref_setvalue_str("language", "lengua");
                    pref_setvalue_str("chooselang", "elige lengua");

                    pref_setvalue_str("profile", "perfil");
                    pref_setvalue_str("logout", "cerrar sesión");
                    pref_setvalue_str("activity", "actividad");
                    pref_setvalue_str("feedback", "realimentación");
                    pref_setvalue_str("settings", "ajustes");
                    pref_setvalue_str("friendrank", "amigos Rango");
                    pref_setvalue_str("aboutus", "sobre nosotros");
                   pref_setvalue_int("position", pos);
                    pref_setvalue_str("position1", "" + position);
                    pref_setvalue_str("friend", "Amigos");
                    pref_setvalue_str("thismonth", "este mes");
                    pref_setvalue_str("lastmonth", "el mes pasado");
                    pref_setvalue_str("gettogather", " vamos a quedar");
                    pref_setvalue_str("call", "llamada");
                    pref_setvalue_str("sms", "sms");
                    pref_setvalue_str("reminder", "recordatorio");
                    pref_setvalue_str("e_cards", "tarjetas de felicitación");
                    pref_setvalue_str("feels", "Sentir del Día");
                    pref_setvalue_str("feels_today", "Como estuvo hoy tu día");
                    pref_setvalue_str("excelent", "excelente");
                    pref_setvalue_str("good", "bueno");
                    pref_setvalue_str("average", "promedio");
                    pref_setvalue_str("sad", "triste");
                    pref_setvalue_str("frustrated", "frustrado");



                }
                else if (pos==8){
                    //spanish
                    String languageToLoad  = "ko";
                    Locale locale = new Locale(languageToLoad);
                    Locale.setDefault(locale);
                    Configuration config = new Configuration();
                    config.locale = locale;
                    getBaseContext().getResources().updateConfiguration(config,
                            getBaseContext().getResources().getDisplayMetrics());
                    pref_setvalue_str("bestfrd", "가장 친한 친구 ");
                    pref_setvalue_str("igrfrd", "무시 친구");
                    pref_setvalue_str("annoyfrd", "짜증 친구");
                    pref_setvalue_str("lousyfrd", "형편없는 친구");
                    pref_setvalue_str("language", "언어");
                    pref_setvalue_str("chooselang", "언어를 선택");

                    pref_setvalue_str("profile", "윤곽");
                    pref_setvalue_str("logout", "로그 아웃");

                    pref_setvalue_str("activity", "활동");
                    pref_setvalue_str("feedback", "피드백");
                    pref_setvalue_str("settings", "설정");
                    pref_setvalue_str("friendrank", "친구 계급");
                    pref_setvalue_str("aboutus", "우리에 대해");
//       pref_setvalue_int("position", pos);
//        pref_setvalue_str("position1",""+position);
                    pref_setvalue_str("friend", "친구");
                    pref_setvalue_str("thismonth", "이번 달");
                    pref_setvalue_str("lastmonth", "지난 달");
                    lang_text.setText("언어");
                    pref_setvalue_str("SpinnerItem", language.getSelectedItem().toString());
                    chooselang.setText("언어를 선택");
                    choosefeels.setText("오늘의 기분");
                    pref_setvalue_str("gettogather", "만나자");
                    pref_setvalue_str("call", "요구");
                    pref_setvalue_str("sms", "sms");
                    pref_setvalue_str("reminder", "조언");
                    pref_setvalue_str("e_cards", "인사말 카드");

                    pref_setvalue_str("feels", "오늘의 기분");
                    pref_setvalue_str("feels_today", "오늘은 어땠 니");
                    pref_setvalue_str("excelent", "Excellent");
                    pref_setvalue_str("good", "좋은");
                    pref_setvalue_str("average", "평균");
                    pref_setvalue_str("sad", "슬픈");
                    pref_setvalue_str("frustrated", "실망한");
                   pref_setvalue_int("position", pos);
                    pref_setvalue_str("position1", "" + position);
                                       



                }
                else if (pos==9){
                    //spanish
                    String languageToLoad  = "id";
                    Locale locale = new Locale(languageToLoad);
                    Locale.setDefault(locale);
                    Configuration config = new Configuration();
                    config.locale = locale;
                    getBaseContext().getResources().updateConfiguration(config,
                            getBaseContext().getResources().getDisplayMetrics());
                    lang_text.setText("bahasa");
                    pref_setvalue_str("SpinnerItem", language.getSelectedItem().toString());
                    chooselang.setText("Pilih bahasa");
                    choosefeels.setText("Rasakan Hari");
                    pref_setvalue_str("bestfrd", " sahabat");
                    pref_setvalue_str("igrfrd", "teman diabaikan");
                    pref_setvalue_str("annoyfrd", "teman terganggu");
                    pref_setvalue_str("lousyfrd", "teman buruk");
                    pref_setvalue_str("logout", "keluar");
                    pref_setvalue_str("language", "bahasa");
                    pref_setvalue_str("chooselang", "Pilih bahasa");
                    pref_setvalue_str("profile", "Profil");
                    pref_setvalue_str("activity", "aktivitas");
                    pref_setvalue_str("feedback", "umpan balik");
                    pref_setvalue_str("settings", "pengaturan");
                    pref_setvalue_str("friendrank", "teman Pangkat");
                    pref_setvalue_str("aboutus", "tentang kami");
                   pref_setvalue_int("position", pos);
                    pref_setvalue_str("position1", "" + position);
                    pref_setvalue_str("friend", "temans");
                    pref_setvalue_str("thismonth", "bulan ini");
                    pref_setvalue_str("lastmonth", "bulan lalu");
                    pref_setvalue_str("gettogather", "memungkinkan memenuhi");
                    pref_setvalue_str("call", "panggilan");
                    pref_setvalue_str("sms", "sms");
                    pref_setvalue_str("reminder", "peringatan");
                    pref_setvalue_str("e_cards", "kartu ucapan");
                    pref_setvalue_str("feels", "Rasakan Hari");
                    pref_setvalue_str("feels_today", "Bagaimana harimu hari ini");
                    pref_setvalue_str("excelent", "unggul");
                    pref_setvalue_str("good", "baik");
                    pref_setvalue_str("average", "rata-rata");
                    pref_setvalue_str("sad", "sedih");
                    pref_setvalue_str("frustrated", "frustrasi");



                }
                else if (pos==10){
                    //spanish
                    String languageToLoad  = "ja";
                    Locale locale = new Locale(languageToLoad);
                    Locale.setDefault(locale);
                    Configuration config = new Configuration();
                    config.locale = locale;
                    getBaseContext().getResources().updateConfiguration(config,
                            getBaseContext().getResources().getDisplayMetrics());
                    pref_setvalue_str("bestfrd", " 親友");
                    pref_setvalue_str("igrfrd", "無視友人");
                    pref_setvalue_str("annoyfrd", "イライラ友人");
                    pref_setvalue_str("lousyfrd", "お粗末な友人");
                    pref_setvalue_str("language", "言語");
                    pref_setvalue_str("chooselang", "言語を選択");
                    pref_setvalue_str("logout", "ログアウト");
                    pref_setvalue_str("profile", "プロフィール");
                    pref_setvalue_str("activity", "アクティビティ");
                    pref_setvalue_str("feedback", "フィードバック");
                    pref_setvalue_str("settings", "設定");
                    pref_setvalue_str("friendrank", "フレンズ ランク");
                    pref_setvalue_str("aboutus", "私たちに関しては");
//       pref_setvalue_int("position", pos);
//        pref_setvalue_str("position1",""+position);
                    pref_setvalue_str("friend", "フレンズ");
                    pref_setvalue_str("thismonth", "今月");
                    pref_setvalue_str("lastmonth", "先月");
                    lang_text.setText("言語");
                    pref_setvalue_str("SpinnerItem", language.getSelectedItem().toString());
                    chooselang.setText("言語を選択");
                    choosefeels.setText("その日のフィール");
                    pref_setvalue_str("gettogather", "会いましょう");
                    pref_setvalue_str("call", "コー\u200B\u200Bル");
                    pref_setvalue_str("sms", "sms");
                    pref_setvalue_str("reminder", "リマインダー");
                    pref_setvalue_str("e_cards", "グリーティングカード");
                    pref_setvalue_str("feels", "その日のフィール");
                    pref_setvalue_str("feels_today", "今日はどうだった");
                    pref_setvalue_str("excelent", "優れました");
                    pref_setvalue_str("good", "グッド");
                    pref_setvalue_str("average", "平均");
                    pref_setvalue_str("sad", "悲しいです");
                    pref_setvalue_str("frustrated", "欲求不満の");


                   pref_setvalue_int("position", pos);
                    pref_setvalue_str("position1", "" + position);

                    



                }
                else if (pos==11){
                    //spanish
                    String languageToLoad  = "tr";
                    Locale locale = new Locale(languageToLoad);
                    Locale.setDefault(locale);
                    Configuration config = new Configuration();
                    config.locale = locale;
                    getBaseContext().getResources().updateConfiguration(config,
                            getBaseContext().getResources().getDisplayMetrics());
                    lang_text.setText("dil");
                    pref_setvalue_str("SpinnerItem", language.getSelectedItem().toString());
                    chooselang.setText("dil seçiniz");
                    choosefeels.setText("Günün hissedin");
                    pref_setvalue_str("bestfrd", " en iyi arkadaş");
                    pref_setvalue_str("igrfrd", " igonered arkadaş");
                    pref_setvalue_str("annoyfrd", "rahatsız arkadaş");
                    pref_setvalue_str("lousyfrd", " berbat bir arkadaş");
                    pref_setvalue_str("language", "dil");
                    pref_setvalue_str("chooselang", "dil seçiniz");

                    pref_setvalue_str("profile", "profil");
                    pref_setvalue_str("logout", "çıkış Yap");
                    pref_setvalue_str("activity", "etkinlik");
                    pref_setvalue_str("feedback", " geri bildirim");
                    pref_setvalue_str("settings", "ayarlar");
                    pref_setvalue_str("friendrank", "arkadaş rütbe");
                    pref_setvalue_str("aboutus", "Hakkımızda");
                   pref_setvalue_int("position", pos);
                    pref_setvalue_str("position1", "" + position);
                    pref_setvalue_str("friend", "arkadaş");
                    pref_setvalue_str("thismonth", " bu ay");
                    pref_setvalue_str("lastmonth", "geçen  ay");
                    pref_setvalue_str("gettogather", "buluşalım");
                    pref_setvalue_str("call", "arama");
                    pref_setvalue_str("sms", "SMS");
                    pref_setvalue_str("reminder", "hatırlatma");
                    pref_setvalue_str("e_cards", "tebrik kartı");
                    pref_setvalue_str("feels", "Günün hissedin");
                    pref_setvalue_str("feels_today", "günün nasıl bugün");
                    pref_setvalue_str("excelent", "mükemmel");
                    pref_setvalue_str("good", "iyi");
                    pref_setvalue_str("average", "ortalama");
                    pref_setvalue_str("sad", "üzgün");
                    pref_setvalue_str("frustrated", "hayal kırıklığına");
                }
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }
    @Override
    public void initUI() {
        language=(Spinner)findViewById(R.id.language);
        back_layout=(LinearLayout)findViewById(R.id.back_layout);
        lang_text=(TextView)findViewById(R.id.lang_text);
        chooselang=(TextView)findViewById(R.id.chooselang);
        choosefeels=(TextView)findViewById(R.id.choosefeels);
    }
    @Override
    public void onBackPressed() {
        Intent intent=new Intent(Multilanguage.this,Incoming_outgoing_calls.class);
        startActivity(intent);
        Multilanguage.this.finish();
       // finish();

    }



}
