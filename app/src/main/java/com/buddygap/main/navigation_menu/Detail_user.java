package com.buddygap.main.navigation_menu;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import com.buddygap.main.R;

public class Detail_user extends BaseAdapter {
Context c1;
    boolean flag;
    ArrayList<String> arraylist,arraylist1,arraylist2;
    public Detail_user(Activity activity_info, ArrayList<String> arraylist, ArrayList<String> arraylist1, ArrayList<String> arraylist2, boolean flag) {
        c1=activity_info;
        this.arraylist=arraylist;
        this.arraylist1=arraylist1;
        this.arraylist2=arraylist2;
        this.flag=flag;

    }

    @Override
    public int getCount() {
        return arraylist.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater=LayoutInflater.from(c1);
        convertView=inflater.inflate(R.layout.single_row_today,null);
        ImageView going_img=(ImageView)convertView.findViewById(R.id.going_img);
        LinearLayout layout_watch=(LinearLayout)convertView.findViewById(R.id.layout_watch);
        TextView text1=(TextView)convertView.findViewById(R.id.dur);
        TextView text2=(TextView)convertView.findViewById(R.id.date);
           TextView no_calls=(TextView)convertView.findViewById(R.id.noofcalls);

       // going_img.setVisibility(View.INVISIBLE);
        if (flag==true){
          //  going_img.setVisibility(View.VISIBLE);
            going_img.setImageResource(R.drawable.incoming);
            layout_watch.setBackgroundResource(R.drawable.activity_date);
        }
        else{
           // going_img.setVisibility(View.VISIBLE);
            going_img.setImageResource(R.drawable.outgoing);
            layout_watch.setBackgroundResource(R.drawable.outgoing_date);
        }

        text2.setText(arraylist.get(position));
        int duration= Integer.parseInt(arraylist1.get(position));
        int min=0,sec=0,hr=0,rem=0;
        String min_name="mins",hr_name="hrs",sec_name="secs";
        if(duration>60 && duration<3600){
            min=duration/60;
            sec=duration%60;
            if(min==1)
                min_name="min";

            if(sec==1)
                sec_name="sec";
            text1.setText("Duration: "+min+" "+min_name+"  "+sec+" "+sec_name);

        }
        else if (duration<60)
        {

            if(sec==1)
                sec_name="sec";
            text1.setText("Duration: "+duration+" "+sec_name);
        }
        else {
            hr=duration/3600;
            rem=duration%60;
            if (rem>60){
                min=rem/60;
                sec=rem%60;
                if(min==1)
                    min_name="min";
                 if (hr==1)
                    hr_name="hr";
                 if(sec==1)
                    sec_name="sec";
                text1.setText("Duration: "+hr+" "+hr_name+"  "+min+" "+min_name+"  "+sec+" "+sec_name);
                            }
            else {
                if(min==1)
                    min_name="min";
                if (hr==1)
                    hr_name="hr";

                text1.setText("Duration: "+hr+" "+hr_name+"  "+rem+" "+min_name);

            }

        }
       // text1.setText("Duration:"+arraylist1.get(position)+"sec");
        no_calls.setText("Number of calls: "+arraylist2.get(position));
        return convertView;
    }
}
