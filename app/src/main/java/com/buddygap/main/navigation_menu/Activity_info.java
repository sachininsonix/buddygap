package com.buddygap.main.navigation_menu;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.buddygap.main.home.Incoming_outgoing_calls;
import com.buddygap.main.iconstant.IConstant;
import com.buddygap.main.utils.BaseActivity;

import com.buddygap.main.R;

public class Activity_info extends BaseActivity implements IConstant {
Button user_out,user_incom;
    ListView listView;
    LinearLayout back_layout;
    View outgoing_view,incoming_view;
    TextView activity;
    boolean flag;

   // int color;
  //  ArrayList<String> arraylist_incom_today_date,arralylist_nofcalls_perday_incom,arraylist_incom_today_dur;
  //  SharedPreferences sharedPreferences;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_activity_info);
        initUI();
        flag=true;
        try{
        Detail_user detail_user=new Detail_user(Activity_info.this, Incoming_outgoing_calls.arraylist_incom_today_date,Incoming_outgoing_calls.arraylist_incom_today_dur,Incoming_outgoing_calls.arralylist_nofcalls_perday_incom,flag);
        listView.setAdapter(detail_user);
        if (pref_getvalue_str("activity", "").equals("")){
            activity.setText("Activity");
        }
        else
        activity.setText(pref_getvalue_str("activity",""));}
        catch (Exception e){

        }
        user_out.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                                incoming_view.setVisibility(View.GONE);
                outgoing_view.setVisibility(View.VISIBLE);
                flag=false;
                user_out.setTextColor(Color.parseColor("#ffffff"));
                user_incom.setTextColor(Color.parseColor("#78ffffff"));
                try{
                Detail_user detail_user=new Detail_user(Activity_info.this,Incoming_outgoing_calls.arraylist_today_date,Incoming_outgoing_calls.arraylist_today_dur,Incoming_outgoing_calls.arralylist_nofcalls_perday,flag);
                listView.setAdapter(detail_user);}
                catch (Exception e){

                }
            }
        });

        user_incom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                user_out.setTextColor(Color.parseColor("#78ffffff"));
                user_incom.setTextColor(Color.parseColor("#ffffff"));
                incoming_view.setVisibility(View.VISIBLE);
                outgoing_view.setVisibility(View.GONE);
                flag=true;
               /* user_out.setBackgroundResource(R.color.backoffrd_color);
                user_incom.setBackgroundResource(R.color.splash_color);*/
                try{
                Detail_user detail_user = new Detail_user(Activity_info.this, Incoming_outgoing_calls.arraylist_incom_today_date, Incoming_outgoing_calls.arraylist_incom_today_dur, Incoming_outgoing_calls.arralylist_nofcalls_perday_incom,flag);
                listView.setAdapter(detail_user);}
                catch (Exception e){

                }
            }
        });
        Log.d("incoming_list_outt", String.valueOf(Incoming_outgoing_calls.arraylist_incom_today_date));
back_layout.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {
        Incoming_outgoing_calls.gps_sett = 0;
        Intent intent = new Intent(Activity_info.this, Incoming_outgoing_calls.class);
  startActivity(intent);
        finish();

    }
});
    }

    @Override
    public void initUI() {
        back_layout=(LinearLayout)findViewById(R.id.back_layout);
        user_out=(Button)findViewById(R.id.user_out);
        user_incom=(Button)findViewById(R.id.user_incom);
        listView=(ListView)findViewById(R.id.list_item);
        outgoing_view=(View)findViewById(R.id.outgoing_view);
        incoming_view=(View)findViewById(R.id.incoming_view);
        activity=(TextView)findViewById(R.id.activity);
    }


    @Override
    public void onBackPressed() {
       // super.onBackPressed();
        Incoming_outgoing_calls.gps_sett = 0;
        Intent intent=new Intent(Activity_info.this,Incoming_outgoing_calls.class);
        startActivity(intent);
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
