package com.buddygap.main.login;

import android.content.Intent;
import android.content.IntentSender;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.buddygap.main.iconstant.IConstant;
import com.buddygap.main.utils.AppController;
import com.buddygap.main.utils.BaseActivity;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.plus.Plus;
import com.google.android.gms.plus.model.people.Person;

import org.json.JSONException;
import org.json.JSONObject;

import com.buddygap.main.R;

public class Facebook_login extends BaseActivity implements IConstant,GoogleApiClient.OnConnectionFailedListener,GoogleApiClient.ConnectionCallbacks{

    CallbackManager callbackManager;

    LoginButton login_fb;
    ImageView signInButton;
    TextView mStatusTextView;

    String user_idfb,name_fb,lastname_fb;
    GoogleApiClient  mGoogleApiClient;

    private boolean mIntentInProgress;
    String yes="";
    private boolean mSignInClicked;

    private ConnectionResult mConnectionResult;
    int RC_SIGN_IN=1;
    @Override
    public void initUI() {
        login_fb=(LoginButton)findViewById(R.id.login_fb);
        signInButton=(ImageView)findViewById(R.id.sign_in_button);
        mStatusTextView=(TextView)findViewById(R.id.mStatusTextView);
    }
    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        callbackManager= CallbackManager.Factory.create();
        setContentView(R.layout.activity_facebook_login);
        initUI();
        AppController.tracker().send(new HitBuilders.EventBuilder("ui", "open")
                .setLabel("Facebook_Login Activity")
                .build());

        // set fb button color and icon
        login_fb.setBackgroundColor(Color.parseColor("#1665c1"));
        login_fb.setCompoundDrawablesWithIntrinsicBounds(
                R.drawable.fbb, 0, 0, 0);
        login_fb.setText("");

        // set gmail icon
        signInButton.setBackgroundResource(R.drawable.gmail_icon);



        try{
            yes=getIntent().getExtras().getString("logout");
        }
        catch (Exception e){

        }


        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this).addApi(Plus.API, Plus.PlusOptions.builder().build())
                .addScope(Plus.SCOPE_PLUS_LOGIN).build();

// gmail login

        signInButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                signInWithGplus();
            }
        });




// facebook login
        login_fb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (isNetworkAvailable() == false) {
                    alert_dialog();
                    Log.d("alert","alert");

                }
                else{
                    login_fb.setReadPermissions("email");
                    login_fb.setReadPermissions("public_profile");
                    login_reg();
                    Log.d("alert1", "alert1");
                }
            }
        });



    }

    // Login with Facebook

    void login_reg(){
               login_fb.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {

                GraphRequest request = GraphRequest.newMeRequest(loginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(JSONObject jsonObject, GraphResponse graphResponse) {
                        try {

                            user_idfb = jsonObject.getString("id");
                                 name_fb = jsonObject.getString("first_name");
                            lastname_fb = jsonObject.getString("last_name");
                                pref_setvalue_str("FirstName", name_fb);
                            pref_setvalue_str("LastName", lastname_fb);
                            pref_setvalue_str("user_id", user_idfb);
                            pref_setvalue_str("Image", "https://graph.facebook.com/" + user_idfb + "/picture?type=large");
                            Intent intent = new Intent(Facebook_login.this, Login_process.class);
                            startActivity(intent);
                            finish();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                });
                Bundle parameters = new Bundle();
                parameters.putString("fields", "id,email,first_name,last_name,picture");
                request.setParameters(parameters);
                request.executeAsync();

            }

            @Override
            public void onCancel() {

            }

            @Override
            public void onError(FacebookException e) {

            }
        });
    }


    // Sign With gmail

    protected void onStart() {
        super.onStart();
        mGoogleApiClient.connect();
    }
    protected void onStop() {
        super.onStop();

        if (mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
        //  mGoogleApiClient=getA
    }


    // Show popup of gmail
   private void resolveSignInError() {
        if (mConnectionResult.hasResolution()) {
            try {
                mIntentInProgress = true;
                mConnectionResult.startResolutionForResult(this, RC_SIGN_IN);
            } catch (IntentSender.SendIntentException e) {
                mIntentInProgress = false;
                mGoogleApiClient.connect();
            }
        }
    }





    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        if(!connectionResult.hasResolution()){
            GooglePlayServicesUtil.getErrorDialog(connectionResult.getErrorCode(),this,0).show();
            return;
        }
        if (!mIntentInProgress) {
            // Store the ConnectionResult for later usage
            mConnectionResult = connectionResult;

            if (mSignInClicked) {
                resolveSignInError();
            }
        }


    }

    // Sign in gmail
    private void signInWithGplus() {
        //progress_show();
        if (!mGoogleApiClient.isConnecting()) {
            mSignInClicked = true;
           // progress_dismiss();
            resolveSignInError();
        }
    }

    // signout gmail
    private void signOutFromGplus() {
               if (mGoogleApiClient.isConnected()) {
            Plus.AccountApi.clearDefaultAccount(mGoogleApiClient);
            Plus.AccountApi.revokeAccessAndDisconnect(mGoogleApiClient)
                    .setResultCallback(new ResultCallback<Status>() {
                        @Override
                        public void onResult(Status s) {
                            mGoogleApiClient.connect();
                        }
                    });
            mGoogleApiClient.disconnect();
            mGoogleApiClient.connect();

        }
    }


    @Override
    public void onConnected(Bundle bundle) {

        if (yes.equals("yes")){
            yes="no";
            signOutFromGplus();

        }
        else {

            mSignInClicked = false;
            getProfileInformation();

        }

    }

    @Override
    public void onConnectionSuspended(int i) {
        mGoogleApiClient.connect();

    }




    // get gamil profile information

    private void getProfileInformation() {
        try {
            if (Plus.PeopleApi.getCurrentPerson(mGoogleApiClient) != null) {
                Person currentPerson = Plus.PeopleApi
                        .getCurrentPerson(mGoogleApiClient);
                String personName = currentPerson.getDisplayName();
                String personPhotoUrl = currentPerson.getImage().getUrl();
                String nam[]=personName.split(" ");
                String  id=currentPerson.getId();
                pref_setvalue_str("FirstName", nam[0]);
                pref_setvalue_str("LastName", nam[1]);
                pref_setvalue_str("gmail", "indu");
                pref_setvalue_str("user_id", id);
                pref_setvalue_str("Image", personPhotoUrl);

                Intent intent=new Intent(Facebook_login.this,Login_process.class);
                startActivity(intent);
                finish();

                           }
        }
        catch (Exception e){
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int responseCode, Intent data) {
        super.onActivityResult(requestCode, responseCode, data);
        Log.d("alert22", "alert22");
        if (requestCode == RC_SIGN_IN) {
            Log.d("alert22", "alert22");
            if (responseCode != RESULT_OK) {
                mSignInClicked = false;
            }

            mIntentInProgress = false;

            if (!mGoogleApiClient.isConnecting()) {
                mGoogleApiClient.connect();
            }
        }

        else{
            Log.d("alert22", "alert22");
            callbackManager.onActivityResult(requestCode,responseCode,data);}
         }



    @Override
    public void onBackPressed() {
        finish();
    }
}
