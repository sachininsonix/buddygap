package com.buddygap.main.home;

import android.Manifest;
import android.app.Activity;
import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.SystemClock;
import android.provider.CallLog;
import android.provider.ContactsContract;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SlidingPaneLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.buddygap.main.R;
import com.buddygap.main.iconstant.IConstant;
import com.buddygap.main.lastmonth_list.Last_month_incoming_call;
import com.buddygap.main.lastmonth_list.Last_month_outgoingcall;
import com.buddygap.main.lastmonth_list.Last_monthlist;
import com.buddygap.main.login.Facebook_login;
import com.buddygap.main.navigation_menu.About_us;
import com.buddygap.main.navigation_menu.Activity_info;
import com.buddygap.main.navigation_menu.Friend_Ranking;
import com.buddygap.main.navigation_menu.Multilanguage;
import com.buddygap.main.navigation_menu.User_profile;
import com.buddygap.main.this_monthlist.Bestfrdz_list;
import com.buddygap.main.utils.AppController;
import com.buddygap.main.utils.BaseActivity;
import com.buddygap.main.utils.DeviceID;
import com.buddygap.main.utils.GPSTracker;
import com.buddygap.main.utils.LocationService;
import com.buddygap.main.utils.Notification_feel;
import com.buddygap.main.utils.broadcast;
import com.facebook.CallbackManager;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginManager;
import com.facebook.share.widget.ShareDialog;
import com.google.android.gms.analytics.HitBuilders;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.URLEncoder;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
public class Incoming_outgoing_calls extends BaseActivity implements IConstant {
public static int gps_sett=1;
    StringBuilder builder;
    String str_phn, str_rnk,loginCheck,titlenote;
    public static int cur_dur_incom = 0, cur_no_calls_incom = 0, cur_dur_out = 0, cur_no_calls_out = 0, cur_out_dur, dur_day_out = 0, calls_day_incom = 0, Dur_day_incom = 0;
    public static int mdur = 0, mnum_of_calls = 0, cur_in_dur, cur_in_no_call;
    public static int idur = 0, inum_of_calls = 0, last2last30days_calls_incom = 0, last2last30days_dur_incom = 0, last2last30days_calls_out = 0, last2last30days_dur_out = 0;
    public static ArrayList<String> arraylist_missedcall, arraylist_missedcalldate, last2last30days_calls_missedlist, last2last30days_calls_missed_namelist;
    public static ArrayList<String> arraylist_lastthirtydaysincom, arraylist_lastthirtydaysout, arrayList, arrayList_incalls, arraylist_incom_today_date, arraylist_today_dur, arraylist_incom_today_dur;
    public static ArrayList<String> arrayList1, arrayList_indur, arraylist_today_date, arralylist_nofcalls_perday, arralylist_nofcalls_perday_incom;
    public static ArrayList<String> arraylist_incom_dur, arraylist_incom_phn, arraylist_out_phn, arraylist_out_dur, arrayList_incom_name, arrayList_out_name;
    public static ArrayList<String> last_arraylist_incom_dur, last_arraylist_incom_phn, last_arraylist_out_phn, last_arraylist_out_dur, last_arraylist_incom_name, last_arraylist_out_name;
    public static ArrayList<String> lastarraylist_missed_calldur, arraylist_missed_calldur, arrylist_ranking, last_arraylistranking, arraylist_incom_email, arraylist_out_email, last_Arraylist_incom_email, last_Arraylist_out_email;
    public static ArrayList arrylist_ranking_size, menulist, last_ranking_size, xtra_list;


    SlidingPaneLayout slidingPaneLayout;
    TextView first_name, last_name, best_frd, friend,feelofday,textrank,textmsg;
    ListView listView;
    LinearLayout best_layout, ignore_layout, annoy_layout, lousy_layout,rank_layout;
    ImageView back_btn;
    ImageView profile1,yourfeels;
    TextView ignored_frd, annoyed_frd, lousy_frd;
    TextView this_month_best, this_month_igr, this_month_annoy, this_month_lousy, last_month_best, last_month_igr, last_monthannoy, last_monthlousy;
    TextView this_month_text, last_month_text, current_month_ignore, last_month_ignore, current_month_annoy, last_month_annoy, current_month_lousy, last_month_lousy;
    LinearLayout back_layout,maincontainer;
    LinearLayout best_this_month, best_last_month, ignore_this_month, ignore_last_month, annoy_this_month, annoy_last_month, lousy_this_month, lousy_last_month,feels_lay;
    // SharedPreferences sharedPreferences,shared_lang;
    public static String best_frdz, igr_frdz, annoy_frdz, lousy_frdz;
    // ConnectivityManager connMgr ;
    Iterator<String> listnumite, listnumite1;
    ArrayList<String> rank_list;
    // NetworkInfo networkInfo ;
    ImageLoader imageLoader;
   public static int permission_deny=1,login_deny=1;
    DisplayImageOptions displayImageOptions;
    private boolean doubleBackToExitPressedOnce;
    GPSTracker gpsTracker;
    static String currentDate, url_allfrd;
    Double lat,lon;
    private String callname;
    @Override
    public void initUI() {
        profile1 = (ImageView) findViewById(R.id.profile1);
        slidingPaneLayout = (SlidingPaneLayout) findViewById(R.id.SlidingPanel);
        first_name = (TextView) findViewById(R.id.first_name);
        last_name = (TextView) findViewById(R.id.last_name);
        best_frd = (TextView) findViewById(R.id.best_frd);
        ignored_frd = (TextView) findViewById(R.id.ignored_frd);
        annoyed_frd = (TextView) findViewById(R.id.annoyed_frd);
        lousy_frd = (TextView) findViewById(R.id.lousy_frd);
        friend = (TextView) findViewById(R.id.friend);
        this_month_best = (TextView) findViewById(R.id.this_month_best);
        this_month_igr = (TextView) findViewById(R.id.this_month_igr);
        this_month_annoy = (TextView) findViewById(R.id.this_month_annoy);
        this_month_lousy = (TextView) findViewById(R.id.this_month_lousy);
        last_month_best = (TextView) findViewById(R.id.last_month_best);
        last_month_igr = (TextView) findViewById(R.id.last_month_igr);
        last_monthannoy = (TextView) findViewById(R.id.last_monthannoy);
        last_monthlousy = (TextView) findViewById(R.id.last_monthlousy);
        listView = (ListView) findViewById(R.id.MenuList);
        back_layout = (LinearLayout) findViewById(R.id.back_layout);
        best_layout = (LinearLayout) findViewById(R.id.best_layout);
        ignore_layout = (LinearLayout) findViewById(R.id.ignore_layout);
        annoy_layout = (LinearLayout) findViewById(R.id.annoy_layout);
        lousy_layout = (LinearLayout) findViewById(R.id.lousy_layout);
        best_this_month = (LinearLayout) findViewById(R.id.best_this_month);
        best_last_month = (LinearLayout) findViewById(R.id.best_last_month);
        ignore_this_month = (LinearLayout) findViewById(R.id.ignore_this_month);
        ignore_last_month = (LinearLayout) findViewById(R.id.ignore_last_month);
        annoy_this_month = (LinearLayout) findViewById(R.id.annoy_this_month);
        annoy_last_month = (LinearLayout) findViewById(R.id.annoy_last_month);
        lousy_this_month = (LinearLayout) findViewById(R.id.lousy_this_month);
        lousy_last_month = (LinearLayout) findViewById(R.id.lousy_last_month);
        rank_layout = (LinearLayout) findViewById(R.id.rank_layout);
        feels_lay = (LinearLayout) findViewById(R.id.feels_lay);
        maincontainer = (LinearLayout) findViewById(R.id.maincontainer);
        back_btn = (ImageView) findViewById(R.id.back_btn);
        yourfeels = (ImageView) findViewById(R.id.yourfeels);
        this_month_text = (TextView) findViewById(R.id.this_month);
        last_month_text = (TextView) findViewById(R.id.last_month);
        current_month_ignore = (TextView) findViewById(R.id.current_month_ignore);
        last_month_ignore = (TextView) findViewById(R.id.last_month_ignore);
        current_month_annoy = (TextView) findViewById(R.id.current_month_annoy);
        last_month_annoy = (TextView) findViewById(R.id.last_month_annoy);
        current_month_lousy = (TextView) findViewById(R.id.current_month_lousy);
        last_month_lousy = (TextView) findViewById(R.id.last_month_lousy);
        first_name = (TextView) findViewById(R.id.first_name);
        last_name = (TextView) findViewById(R.id.last_name);
        feelofday = (TextView) findViewById(R.id.feelofday);
        textrank = (TextView) findViewById(R.id.textrank);
        textmsg = (TextView) findViewById(R.id.textmsg);
    }
    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_incoming_outgoing_calls);
            initUI();
            maincontainer.setVisibility(View.VISIBLE);
            gpsTracker = new GPSTracker(Incoming_outgoing_calls.this);
            try {
                GetMood();
                loginCheck = getIntent().getStringExtra("loginCheck");

                Handler mHandler = new Handler(getMainLooper());
                mHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            titlenote = getIntent().getStringExtra("title");
                            if (!titlenote.equals(" ")) {
                                rank_layout.setVisibility(View.VISIBLE);
                                textrank.setText(getIntent().getStringExtra("rank"));
                                textmsg.setText(getIntent().getStringExtra("title"));
//                    Toast.makeText(getApplicationContext(), getIntent().getStringExtra("title"), Toast.LENGTH_LONG).show();
                            }
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                });
            }
            catch (Exception e){
                e.printStackTrace();
            }

            //   notification_alarm();
        /*--Start Service--*/
            // Start section to start the service to update the loction on server using service after fixed time interval
/*  try{
                permission_deny= Login_process.permission_deny_login;
      Log.d("error", String.valueOf(permission_deny));
            }
            catch (Exception e){

//                permission_deny=0;
//                Log.d("deny", String.valueOf(permission_deny));
                Log.d("error", String.valueOf(e));
            }*/
            if (weHavePermissionToReadContacts()) {
//            readTheContacts();
//            gpsTracker.showSettingsAlert();
                if (gpsTracker.canGetLocation()) {
                    if (gpsTracker.canGetLocation) {
                        lat = gpsTracker.getLatitude();
                        lon = gpsTracker.getLongitude();
                        StartServiceforLocation();
                    }

                } else {

                    if (gps_sett == 1) {
                        gpsTracker.showSettingsAlert();
                        Log.d("gsss", "gsss");
                    } else {
                        Log.d("gpss", "gpsss");
                    }
                }
            } else {
                if (permission_deny == 1) {
                    requestReadContactsPermissionFirst();
                } else {

                }
            }
            //end section  Method to start the service
            AppController.tracker().send(new HitBuilders.EventBuilder("ui", "open")
                    .setLabel("Home Activity")
                    .build());
            imageLoader = ImageLoader.getInstance();
            imageLoader.init(ImageLoaderConfiguration.createDefault(Incoming_outgoing_calls.this));
            displayImageOptions = new DisplayImageOptions.Builder().showStubImage(R.drawable.user).showImageForEmptyUri(R.drawable.user).cacheOnDisc().cacheInMemory().build();

            doubleBackToExitPressedOnce = false;
            if (isNetworkAvailable() == false) {
                Toast.makeText(Incoming_outgoing_calls.this, "No Internet Available!", Toast.LENGTH_SHORT).show();
            } else {

            }
            cur_month_incom incom = new cur_month_incom(Incoming_outgoing_calls.this);
            final cur_month_out cur_month_out1 = new cur_month_out(Incoming_outgoing_calls.this);
            this_month_text.setText(cur_month_out.size_best);
            last_month_text.setText(Last_month_outgoingcall.size_best);
            current_month_ignore.setText(cur_month_out.size_ignore);
            last_month_ignore.setText(Last_month_outgoingcall.size_ignore);
            current_month_annoy.setText(cur_month_out1.size_annoy);
            last_month_annoy.setText(Last_month_outgoingcall.size_annoy);
            current_month_lousy.setText(cur_month_out1.size_lousy);
            last_month_lousy.setText(Last_month_outgoingcall.size_lousy);
            slidingPaneLayout.setSliderFadeColor(Color.TRANSPARENT);
            rank_list = new ArrayList<>();

            if (!pref_getvalue_str("bestfrd", "").equals("")) {
                menulist = new ArrayList();
                try {
                    best_frdz = pref_getvalue_str("bestfrd", "");
                    igr_frdz = pref_getvalue_str("igrfrd", "");
                    annoy_frdz = pref_getvalue_str("annoyfrd", "");
                    lousy_frdz = pref_getvalue_str("lousyfrd", "");
                    friend.setText(pref_getvalue_str("friend", ""));
                    feelofday.setText(pref_getvalue_str("feels", "")+" : ");
                    menulist.add(pref_getvalue_str("profile", ""));
                    menulist.add(pref_getvalue_str("friendrank", ""));
                    menulist.add(pref_getvalue_str("activity", ""));
                    menulist.add(pref_getvalue_str("aboutus", ""));
                    menulist.add(pref_getvalue_str("settings", ""));
                    //  menulist.add(shared_lang.getString("share", ""));
                    menulist.add(pref_getvalue_str("feedback", ""));
                    menulist.add(pref_getvalue_str("logout", ""));
                } catch (Exception e) {

                }

                this_month_best.setText(pref_getvalue_str("thismonth", ""));
                this_month_igr.setText(pref_getvalue_str("thismonth", ""));
                this_month_annoy.setText(pref_getvalue_str("thismonth", ""));
                this_month_lousy.setText(pref_getvalue_str("thismonth", ""));
                last_monthlousy.setText(pref_getvalue_str("lastmonth", ""));
                last_month_best.setText(pref_getvalue_str("lastmonth", ""));
                last_month_igr.setText(pref_getvalue_str("lastmonth", ""));
                last_monthannoy.setText(pref_getvalue_str("lastmonth", ""));
            } else {
                // String [] Menu1 = {"Profile","Friend Rank","Activity","About us","Settings","Feedback"};
                menulist = new ArrayList();
                menulist.add("Profile");
                menulist.add("Friends Ranking");
                menulist.add("Activity");
                menulist.add("About us");
                menulist.add("Settings");
                //  menulist.add("Share");
                menulist.add("Feedback");
                menulist.add("Logout");

                best_frdz = "Best Friends";
                igr_frdz = "Ignored Friends";
                annoy_frdz = "Annoyed Friends";
                lousy_frdz = "Lousy Friends";
            }

            listView.setAdapter(new MenuAdapter(Incoming_outgoing_calls.this, menulist));
            String firstName = pref_getvalue_str("FirstName", "");
            String lastName = pref_getvalue_str("LastName", "");
            best_frd.setText(best_frdz);
            annoyed_frd.setText(annoy_frdz);
            ignored_frd.setText(igr_frdz);
            lousy_frd.setText(lousy_frdz);
            if (pref_getvalue_str("Image", "").equals("")) {

            } else {

                imageLoader.displayImage(pref_getvalue_str("Image", ""), profile1, displayImageOptions);
            }

            first_name.setText(firstName);
            last_name.setText(lastName);

            best_this_month.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(Incoming_outgoing_calls.this, Bestfrdz_list.class);
                    intent.putStringArrayListExtra("name", cur_month_out.listtt1_name);
                    intent.putStringArrayListExtra("number", cur_month_out.listtt1);
                    intent.putExtra("friends", best_frdz);
                    startActivity(intent);
                    Incoming_outgoing_calls.this.finish();


                }
            });
            feels_lay.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(Incoming_outgoing_calls.this, Notification_feel.class);
                    intent.putExtra("frommulti","incoming_outcoming");
                    startActivity(intent);
                    finish();
                }
            });

            best_layout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(Incoming_outgoing_calls.this, Bestfrdz_list.class);
                    intent.putStringArrayListExtra("name", cur_month_out.listtt1_name);
                    intent.putStringArrayListExtra("number", cur_month_out.listtt1);
                    intent.putExtra("friends", best_frdz);
                    startActivity(intent);
                    finish();
                }
            });

            best_last_month.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(Incoming_outgoing_calls.this, Last_monthlist.class);
                    intent.putStringArrayListExtra("name", Last_month_outgoingcall.listtt1_name);
                    intent.putStringArrayListExtra("number", Last_month_outgoingcall.listtt1);
                    intent.putExtra("friends", best_frdz);
                    startActivity(intent);
                    Incoming_outgoing_calls.this.finish();
                }
            });

            annoy_this_month.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(Incoming_outgoing_calls.this, Bestfrdz_list.class);
                    intent.putStringArrayListExtra("name", cur_month_out.listtt2_name);
                    intent.putStringArrayListExtra("number", cur_month_out.listtt2);
                    intent.putExtra("friends", annoy_frdz);
                    startActivity(intent);
                    Incoming_outgoing_calls.this.finish();
                    // finish();
                }
            });

            annoy_layout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(Incoming_outgoing_calls.this, Bestfrdz_list.class);
// Pass data object in the bundle and populate details activity.
                    intent.putStringArrayListExtra("name", cur_month_out.listtt2_name);
                    intent.putStringArrayListExtra("number", cur_month_out.listtt2);
                    intent.putExtra("friends", annoy_frdz);

                    startActivity(intent);
                    Incoming_outgoing_calls.this.finish();
                    // finish();
                }
            });

            annoy_last_month.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(Incoming_outgoing_calls.this, Last_monthlist.class);
                    intent.putStringArrayListExtra("name", Last_month_outgoingcall.listtt2_name);
                    intent.putStringArrayListExtra("number", Last_month_outgoingcall.listtt2);
                    intent.putExtra("friends", annoy_frdz);
                    startActivity(intent);
                    Incoming_outgoing_calls.this.finish();
                    // finish();
                }
            });


            ignore_this_month.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(Incoming_outgoing_calls.this, Bestfrdz_list.class);
                    intent.putExtra("name", cur_month_out.listtt3_name);
                    intent.putExtra("number", cur_month_out.listtt3);
                    intent.putExtra("friends", igr_frdz);
                    startActivity(intent);
                    Incoming_outgoing_calls.this.finish();
                    //     finish();
                }
            });

            ignore_layout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(Incoming_outgoing_calls.this, Bestfrdz_list.class);
                    intent.putExtra("name", cur_month_out.listtt3_name);
                    intent.putExtra("number", cur_month_out.listtt3);
                    intent.putExtra("friends", igr_frdz);
                    startActivity(intent);
                    Incoming_outgoing_calls.this.finish();
                    //     finish();
                }
            });
            ignore_last_month.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(Incoming_outgoing_calls.this, Last_monthlist.class);
                    intent.putExtra("name", Last_month_outgoingcall.listtt3_name);
                    intent.putExtra("number", Last_month_outgoingcall.listtt3);
                    intent.putExtra("friends", igr_frdz);
                    startActivity(intent);
                    Incoming_outgoing_calls.this.finish();
                    //     finish();
                }
            });

            lousy_this_month.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(Incoming_outgoing_calls.this, Bestfrdz_list.class);
                    intent.putExtra("name", cur_month_out.listtt4_name);
                    intent.putExtra("number", cur_month_out.listtt4);
                    intent.putExtra("friends", lousy_frdz);
                    startActivity(intent);
                    Incoming_outgoing_calls.this.finish();
                    // finish();
                }
            });
            lousy_layout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(Incoming_outgoing_calls.this, Bestfrdz_list.class);
                    intent.putExtra("name", cur_month_out.listtt4_name);
                    intent.putExtra("number", cur_month_out.listtt4);
                    intent.putExtra("friends", lousy_frdz);
//
                    startActivity(intent);
                    Incoming_outgoing_calls.this.finish();
                    // finish();
                }
            });
            lousy_last_month.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(Incoming_outgoing_calls.this, Last_monthlist.class);
                    intent.putExtra("name", Last_month_outgoingcall.listtt4_name);
                    intent.putExtra("number", Last_month_outgoingcall.listtt4);
                    intent.putExtra("friends", lousy_frdz);

                    startActivity(intent);
                    Incoming_outgoing_calls.this.finish();
                    // finish();
                }
            });

            back_layout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    slidingPaneLayout.openPane();

                }
            });


            listnumite = arrylist_ranking.iterator();
            for (int i = 1; i <= arrylist_ranking.size(); i++) {
                rank_list.add("" + i);
            }
            listnumite1 = rank_list.iterator();
            builder = new StringBuilder();
            while (listnumite.hasNext() && listnumite1.hasNext()) {
                str_phn = listnumite.next();
                str_rnk = listnumite1.next();
                builder = builder.append(str_phn).append("_").append(str_rnk).append(",");
            }

            try {
                json_allfrdlist();
            } catch (Exception e) {
            }
    }

    @Override
    protected void onResume() {
        super.onResume();

            Handler mHandler = new Handler(getMainLooper());
            mHandler.post(new Runnable() {
                @Override
                public void run() {
                    try {

                        titlenote = getIntent().getStringExtra("title");
                        if (!titlenote.equals(" ")) {
                        rank_layout.setVisibility(View.VISIBLE);
                        textrank.setText(getIntent().getStringExtra("rank"));
                        textmsg.setText(getIntent().getStringExtra("title"));
//                    Toast.makeText(getApplicationContext(), getIntent().getStringExtra("title"), Toast.LENGTH_LONG).show();
                    }
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }
            });


    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
        //now getIntent() should always return the last received intent
    }


    //****
   /* private void createNotification(String pDate, String pTime) {
        AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(ACTION);
        PendingIntent alarmIntent = PendingIntent.getBroadcast(this, 0, intent, 0);
        alarmManager.set(AlarmManager.RTC_WAKEUP, 5*1000, alarmIntent);
    }*/

   /* public void Remind ()
    {

        Intent alarmIntent = new Intent(Incoming_outgoing_calls.this, AlarmReceiver.class);
//        alarmIntent.PutExtra ("message", message);
//        alarmIntent.PutExtra ("title", title);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(this, 0, alarmIntent, 0);
      //  PendingIntent pendingIntent = PendingIntent.GetBroadcast(Forms.Context, 0, alarmIntent, PendingIntentFlags.UpdateCurrent);
        AlarmManager alarmManager = (AlarmManager)getSystemService (Context.ALARM_SERVICE);

        //TODO: For demo set after 5 seconds.
        alarmManager.set(AlarmManager.RTC_WAKEUP, 5*1000, pendingIntent);
       // alarmManager.Set(AlarmType.ElapsedRealtime, SystemClock.ElapsedRealtime() + 5 * 1000, pendingIntent);

    }*/

    void json_allfrdlist() {

        url_allfrd = Base_url + "save_users.php?user_id=" + pref_getvalue_str("user_id", "") + "&listusers=" + URLEncoder.encode(builder.substring(0, builder.lastIndexOf(",")));
          Log.d("url_allfrd111",url_allfrd);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url_allfrd, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject jsonObject) {

                Log.d("allfrd", jsonObject.toString());


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {


            }
        });
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(jsonObjectRequest);
    }


    public void getCallDetails() {
        Date current_date = Calendar.getInstance().getTime(); // Current time
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd"); // Set your date format
        currentDate = sdf.format(current_date);

        String[] cur_date = currentDate.split("-");
        String cur_month = cur_date[1];
        String cur_month1 = cur_month;
        String Today_Date = cur_date[2];
        String Today_Date1 = cur_date[2];
        String date_thirtydays = Today_Date;
        String cur_year = cur_date[0];
        int mprev_month = Integer.parseInt(cur_date[1]) - 1;
        arrayList = new ArrayList<>();
        arrayList1 = new ArrayList<>();
        arrayList_incalls = new ArrayList<>();
        arrayList_indur = new ArrayList<>();
        arraylist_incom_dur = new ArrayList<>();
        arraylist_incom_phn = new ArrayList<>();
        arraylist_out_phn = new ArrayList<>();
        arraylist_out_dur = new ArrayList<>();
        last_arraylist_incom_dur = new ArrayList<>();
        last_arraylist_incom_phn = new ArrayList<>();
        last_arraylist_out_phn = new ArrayList<>();
        last_arraylist_out_dur = new ArrayList<>();
        arrayList_out_name = new ArrayList<>();
        arrayList_incom_name = new ArrayList<>();
        last_arraylist_incom_name = new ArrayList<>();
        last_arraylist_out_name = new ArrayList<>();
        last_Arraylist_incom_email = new ArrayList<>();
        last_Arraylist_out_email = new ArrayList<>();
        arraylist_incom_email = new ArrayList<>();
        arraylist_out_email = new ArrayList<>();
        arraylist_today_date = new ArrayList<>();
        arraylist_incom_today_date = new ArrayList<>();
        arraylist_today_dur = new ArrayList<>();
        arraylist_incom_today_dur = new ArrayList<>();
        arralylist_nofcalls_perday = new ArrayList<>();
        arraylist_lastthirtydaysincom = new ArrayList<>();
        arraylist_lastthirtydaysout = new ArrayList<>();
        arralylist_nofcalls_perday_incom = new ArrayList<>();
        arraylist_missedcalldate = new ArrayList<>();
        arraylist_missedcall = new ArrayList<>();
        last2last30days_calls_missedlist = new ArrayList<>();
        last2last30days_calls_missed_namelist = new ArrayList<>();
        arrylist_ranking = new ArrayList<>();
        last_arraylistranking = new ArrayList<>();
        arrylist_ranking_size = new ArrayList<>();
        last_ranking_size = new ArrayList<>();
        arraylist_missed_calldur = new ArrayList<>();
        lastarraylist_missed_calldur = new ArrayList<>();
        xtra_list = new ArrayList<>();
        Cursor managedCursor = managedQuery(CallLog.Calls.CONTENT_URI, null, null, null, CallLog.Calls.DATE + " DESC");
        int number = managedCursor.getColumnIndex(CallLog.Calls.NUMBER);
        int type = managedCursor.getColumnIndex(CallLog.Calls.TYPE);
        int date = managedCursor.getColumnIndex(CallLog.Calls.DATE);
        int duration = managedCursor.getColumnIndex(CallLog.Calls.DURATION);
            int name = managedCursor.getColumnIndex(CallLog.Calls.CACHED_NAME);


//int photo=managedCursor.getColumnIndex(CallLog.Calls.CACHED_PHOTO_URI);

        dur_day_out = 0;
        Dur_day_incom = 0;
        cur_dur_incom = 0;
        cur_dur_out = 0;
        cur_no_calls_out = 0;
        cur_no_calls_incom = 0;
        int no_of_calls = 0, no_of_calls_incom = 0;
        int last_thirty_days = 0;
        ArrayList arrayList_out_name1 = new ArrayList();
        String s_out = Today_Date, s_incom = Today_Date, m_out = cur_month, m_incom = cur_month;

        //    get data from call log history
        while (managedCursor.moveToNext()) {
            String phNumber = managedCursor.getString(number);
            String callType = managedCursor.getString(type);
            String callDate = managedCursor.getString(date);
            String callname = managedCursor.getString(name);
//            String contactname = managedCursor.getString(name);
//            String cleancontact = phNumber;
//            if (cleancontact.length() == 13) {
//                cleancontact = phNumber.replace("+91", "");
//            } else if (cleancontact.charAt(0) == '0') {
//                cleancontact = phNumber.replaceFirst("0", "");
//
//            } else if (cleancontact.charAt(0) == '+') {
//                cleancontact = phNumber.replace("+", "");
//
//            } else{
//                cleancontact = phNumber;
//            }
//
//            if (contactname == null) {
//
//                callname=getContactName(cleancontact);
//
//            } else
//            {
//                callname=contactname;
//            }
            Date callDayTime = new Date(Long.valueOf(callDate));
            String dat = sdf.format(callDayTime);
            String callDuration = managedCursor.getString(duration);
            if (callDuration.equals("")) {
                callDuration = "1";
            }

            String sp[] = dat.split("-");
            int dircode = Integer.parseInt(callType);
            Today_Date = s_out;
            cur_month = m_out;
            String str1 = phNumber;
            if (str1.length() == 13) {
                str1 = phNumber.replace("+91", "");
            } else if (str1.charAt(0) == '0') {
                str1 = phNumber.replaceFirst("0", "");

            } else if (str1.charAt(0) == '+') {
                str1 = phNumber.replace("+", "");


            } else
                str1 = phNumber;

            arrayList.add(str1);
            arrayList1.add(callDuration);


            // Outgoing calls

            if (dircode == CallLog.Calls.OUTGOING_TYPE) {
                int cal_dur = Integer.parseInt(callDuration);
                String Today_date = sp[2];
                String month = sp[1];
                String year = sp[0];

                // find the Total duration per day and Number of calls Per day

                if (Today_Date.equals(Today_date) && month.equals(cur_month) && cur_year.equals(year)) {
                    dur_day_out = dur_day_out + Integer.parseInt(callDuration);
                    no_of_calls = no_of_calls + 1;

                } else if (!(Today_date).equals(Today_Date) && month.equals(cur_month) && cur_year.equals(year)) {
                    if (no_of_calls >= 1) {
                        arraylist_today_dur.add(String.valueOf(dur_day_out));
                        arralylist_nofcalls_perday.add(String.valueOf(no_of_calls));
                        String ss = formatMonth(cur_month) + " " + Today_Date;
                        arraylist_today_date.add(ss);
                    }
                    Today_Date = Today_date;
                    no_of_calls = 0;
                    dur_day_out = 0;
                    dur_day_out = dur_day_out + Integer.parseInt(callDuration);
                    no_of_calls = no_of_calls + 1;


                } else if (Today_date.equals(Today_Date) && month.equals(cur_month) && !(cur_year).equals(year)) {
                    if (no_of_calls >= 1) {
                        arraylist_today_dur.add(String.valueOf(dur_day_out));
                        arralylist_nofcalls_perday.add(String.valueOf(no_of_calls));
                        String ss = formatMonth(cur_month) + " " + Today_Date;
                        //   String ss = Today_Date + "-" + cur_month + "-" + cur_year;
                        arraylist_today_date.add(ss);
                    }
                    Today_Date = Today_date;
                    cur_year = year;
                    cur_month = month;
                    dur_day_out = 0;
                    no_of_calls = 0;
                    dur_day_out = dur_day_out + Integer.parseInt(callDuration);
                    no_of_calls = no_of_calls + 1;

                } else if (!(Today_date.equals(Today_Date) && !(month.equals(cur_month)) && (cur_year).equals(year))) {
                    if (no_of_calls >= 1) {
                        arraylist_today_dur.add(String.valueOf(dur_day_out));
                        arralylist_nofcalls_perday.add(String.valueOf(no_of_calls));
                        String ss = formatMonth(cur_month) + " " + Today_Date;
                        // String ss = Today_Date + "-" + cur_month + "-" + cur_year;
                        arraylist_today_date.add(ss);
                    }
                    Today_Date = Today_date;
                    cur_month = month;
                    cur_year = year;
                    dur_day_out = 0;
                    no_of_calls = 0;
                    dur_day_out = dur_day_out + Integer.parseInt(callDuration);
                    no_of_calls = no_of_calls + 1;

                }
                s_out = Today_Date;
                m_out = cur_month;


                // Find the Last 30 days Duration , number of calls , phone number
                if (last_thirty_days <= 30) {

                    if (!(date_thirtydays).equals(Today_date)) {
                        last_thirty_days = last_thirty_days + 1;
                        date_thirtydays = Today_date;

                    }
                    String str = phNumber;
                    if (str.length() == 13) {
                        str = phNumber.replace("+91", "");
                    } else if (str.charAt(0) == '0') {

                        str = phNumber.replaceFirst("0", "");

                    } else
                        str = phNumber;

                    arraylist_out_phn.add(str);

                    if (callname == null)
                        arrayList_out_name.add("No Name");
                    else
                        arrayList_out_name.add("" + callname);
                    arraylist_out_dur.add(callDuration);
                    cur_no_calls_out = cur_no_calls_out + 1;
                    int Dur = Integer.parseInt(callDuration);
                    cur_dur_out = cur_dur_out + Dur;
                    String month11 = month;
                }

                //Find duration , number of calls and contact number of last to last 60 days
                if (last_thirty_days >= 30 && last_thirty_days <= 60) {
                    if (!(date_thirtydays).equals(Today_date)) {

                        last_thirty_days = last_thirty_days + 1;
                        date_thirtydays = Today_date;
                    }
                    arraylist_lastthirtydaysout.add(date_thirtydays);
                    String str = phNumber;
                    if (str.length() == 13) {
                        str = phNumber.replace("+91", "");
                    } else if (str.charAt(0) == '0') {
                        str = phNumber.replaceFirst("0", "");

                    } else
                        str = phNumber;
                    if (callname == null)
                        last_arraylist_out_name.add("No Name");
                    else
                        last_arraylist_out_name.add(callname);
                    last_arraylist_out_phn.add(str);
                    last_arraylist_out_dur.add(callDuration);
                    int a = Integer.parseInt(callDuration);
                    last2last30days_calls_out = last2last30days_calls_out + 1;
                    last2last30days_dur_out = last2last30days_dur_out + a;


                }
            }




            // Incoming calls

            else if (dircode == CallLog.Calls.INCOMING_TYPE) {

                arrayList_incalls.add(phNumber);
                arrayList_indur.add(callDuration);
                String month = sp[1];
                String Toady_date = sp[2];
                String year = sp[0];
                Today_Date = s_incom;
                cur_month = m_incom;

                // find the Total duration per day and Number of calls Per day

                if (Today_Date.equals(Toady_date) && month.equals(cur_month) && cur_year.equals(year)) {
                    Dur_day_incom = Dur_day_incom + Integer.parseInt(callDuration);
                    no_of_calls_incom = no_of_calls_incom + 1;

                } else if (!(Toady_date).equals(Today_Date) && month.equals(cur_month) && cur_year.equals(year)) {
                    if (no_of_calls_incom >= 1) {
                        arraylist_incom_today_dur.add(String.valueOf(Dur_day_incom));
                        arralylist_nofcalls_perday_incom.add(String.valueOf(no_of_calls_incom));
                        String ss = formatMonth(cur_month) + " " + Today_Date;
                        //  String ss = Today_Date + "-" + cur_month + "-" + cur_year;
                        arraylist_incom_today_date.add(ss);
                    }
                    Today_Date = Toady_date;
                    no_of_calls_incom = 0;
                    Dur_day_incom = 0;
                    Dur_day_incom = Dur_day_incom + Integer.parseInt(callDuration);
                    no_of_calls_incom = no_of_calls_incom + 1;


                } else if (Toady_date.equals(Today_Date) && month.equals(cur_month) && !(cur_year).equals(year)) {
                    if (no_of_calls_incom >= 1) {
                        arraylist_incom_today_dur.add(String.valueOf(Dur_day_incom));
                        arralylist_nofcalls_perday_incom.add(String.valueOf(no_of_calls_incom));
                        String ss = formatMonth(cur_month) + " " + Today_Date;
                        //  String ss = Today_Date + "-" + cur_month + "-" + cur_year;
                        arraylist_incom_today_date.add(ss);
                    }
                    Today_Date = Toady_date;
                    cur_year = year;
                    cur_month = month;
                    Dur_day_incom = 0;
                    no_of_calls_incom = 0;
                    Dur_day_incom = Dur_day_incom + Integer.parseInt(callDuration);
                    no_of_calls_incom = no_of_calls_incom + 1;

                } else {
                    if (no_of_calls_incom >= 1) {
                        arraylist_incom_today_dur.add(String.valueOf(Dur_day_incom));
                        arralylist_nofcalls_perday_incom.add(String.valueOf(no_of_calls_incom));
                        String ss = formatMonth(cur_month) + " " + Today_Date;
                        //String ss = Today_Date + "-" + cur_month + "-" + cur_year;
                        arraylist_incom_today_date.add(ss);
                    }
                    Today_Date = Toady_date;
                    cur_month = month;
                    cur_year = year;
                    Dur_day_incom = 0;
                    no_of_calls_incom = 0;
                    Dur_day_incom = Dur_day_incom + Integer.parseInt(callDuration);
                    no_of_calls_incom = no_of_calls_incom + 1;
                }
                s_incom = Today_Date;
                m_incom = cur_month;

// last 30 days data of incoming call
                if (last_thirty_days <= 30) {
                    // if(!(Today_Date1).equals(Toady_date)){
                    if (!(date_thirtydays).equals(Toady_date)) {

                        last_thirty_days = last_thirty_days + 1;
                        date_thirtydays = Toady_date;
                        Log.d("datesssin", String.valueOf(last_thirty_days));
                    }

                    String str = phNumber;
                    if (str.length() == 13) {
                        str = phNumber.replace("+91", "");
                    } else if (str.charAt(0) == '0') {
                        str = phNumber.replaceFirst("0", "");
                    } else
                        str = phNumber;
                    arraylist_incom_phn.add(str);

                    if (callname == null)
                        arrayList_incom_name.add("No Name");
                    else
                        arrayList_incom_name.add(callname);
                    arraylist_incom_dur.add(callDuration);
                    int a = Integer.parseInt(callDuration);
                    cur_dur_incom = cur_dur_incom + a;
                    cur_no_calls_incom = cur_no_calls_incom + 1;
                }

                if (last_thirty_days >= 30 && last_thirty_days <= 60) {
                    if (!(date_thirtydays).equals(Toady_date)) {

                        last_thirty_days = last_thirty_days + 1;
                        date_thirtydays = Toady_date;
                    }
                    arraylist_lastthirtydaysout.add(date_thirtydays);
                    String str = phNumber;
                    if (str.length() == 13) {
                        str = phNumber.replace("+91", "");

                    } else if (str.charAt(0) == '0') {
                        str = phNumber.replaceFirst("0", "");

                    } else
                        str = phNumber;
                    if (callname == null)
                        last_arraylist_incom_name.add("No Name");
                    else
                        last_arraylist_incom_name.add(callname);
                    last_arraylist_incom_phn.add(str);
                    Log.d("last_inc", "last_inc" + last_arraylist_incom_phn.toString());
                    last_arraylist_incom_dur.add(callDuration);
                    int a = Integer.parseInt(callDuration);
                    last2last30days_dur_incom = last2last30days_dur_incom + a;
                    last2last30days_calls_incom = last2last30days_calls_incom + 1;

                }

            }




            //Missed call
            else {

                String Toady_date = sp[2];
                String month = sp[1];

                // last 30 days data of misscall
                if (last_thirty_days <= 30) {

                    if (!(date_thirtydays).equals(Toady_date)) {
                        last_thirty_days = last_thirty_days + 1;
                        date_thirtydays = Toady_date;
                    }
                    String str = phNumber;
                    if (str.length() == 13) {
                        str = phNumber.replace("+91", "");
                    } else if (str.charAt(0) == '0') {
                        str = phNumber.replaceFirst("0", "");
                    } else
                        str = phNumber;

                    if (callname == null)
                        arrayList_incom_name.add("No Name");
                    else
                        arrayList_incom_name.add(callname);

                    arraylist_incom_dur.add("1");
                    arraylist_incom_phn.add(str);

                    arraylist_missedcall.add(str);
                    arraylist_missedcalldate.add(date_thirtydays);
                    mnum_of_calls = mnum_of_calls + 1;
                    arraylist_missed_calldur.add("1");
                }
                if (last_thirty_days >= 30 && last_thirty_days <= 60) {

                    if (!(date_thirtydays).equals(Toady_date)) {

                        last_thirty_days = last_thirty_days + 1;
                        date_thirtydays = Toady_date;
                    }
                    arraylist_lastthirtydaysout.add(date_thirtydays);
                    String str = phNumber;

                    if (str.length() == 13) {
                        str = phNumber.replace("+91", "");
                    } else if (str.charAt(0) == '0') {
                        str = phNumber.replaceFirst("0", "");
                    } else
                        str = phNumber;
                    if (callname == null)
                        last_arraylist_incom_name.add("No Name");
                    else
                        last_arraylist_incom_name.add(callname);
                    last_arraylist_incom_phn.add(str);
                    last_arraylist_incom_dur.add("1");
                                    last2last30days_calls_missedlist.add(str);
                    lastarraylist_missed_calldur.add("1");
                }


            }
        }


        arraylist_today_dur.add(String.valueOf(dur_day_out));
        arralylist_nofcalls_perday.add(String.valueOf(no_of_calls));
        String ss = formatMonth(cur_month) + " " + Today_Date;
              arraylist_today_date.add(ss);
        arraylist_incom_today_dur.add(String.valueOf(Dur_day_incom));
        arralylist_nofcalls_perday_incom.add(String.valueOf(no_of_calls_incom));
        String ss1 = formatMonth(m_incom) + " " + s_incom;
               arraylist_incom_today_date.add(ss1);


    }
    public static void showRateDialogForRate(final Context context) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context)
                .setTitle("Rate application")
                .setMessage("Please, rate the app at PlayMarket")
                .setPositiveButton("RATE", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (context != null) {
                            ////////////////////////////////
                            Uri uri = Uri.parse("market://details?id=" + context.getPackageName());
                            Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);
                            // To count with Play market backstack, After pressing back button,
                            // to taken back to our application, we need to add following flags to intent.
                            goToMarket.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY |
                                    Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET |
                                    Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
                            try {
                                context.startActivity(goToMarket);
                            } catch (ActivityNotFoundException e) {
                                context.startActivity(new Intent(Intent.ACTION_VIEW,
                                        Uri.parse("http://play.google.com/store/apps/details?id=" + context.getPackageName())));
                            }


                        }
                    }
                })
                .setNegativeButton("CANCEL", null);
        builder.show();
    }
    public  String getContactName(String phoneNumber) {
        ContentResolver cr = getContentResolver();
        Uri uri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(phoneNumber));
        Cursor cursor = cr.query(uri, new String[]{ContactsContract.PhoneLookup.DISPLAY_NAME}, null, null, null);
        if (cursor == null) {
            return null;
        }
        String contactName = null;
        if(cursor.moveToFirst()) {
            contactName = cursor.getString(cursor.getColumnIndex(ContactsContract.PhoneLookup.DISPLAY_NAME));
        }

        if(cursor != null && !cursor.isClosed()) {
            cursor.close();
        }

        return contactName;
    }




    //* alarm


    public void notification_alarm() {

//        Calendar calendar = Calendar.getInstance();
//
//        calendar.set(Calendar.HOUR_OF_DAY, 10); // For 1 PM or 2 PM
//        calendar.set(Calendar.MINUTE, 20);
//        calendar.set(Calendar.SECOND, 0);

        Date mToday = new Date();

        SimpleDateFormat sdf = new SimpleDateFormat("hh:mm:aa");
        String curTime = sdf.format(mToday);
String s1[]=curTime.split(":");
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(System.currentTimeMillis());
        calendar.set(Calendar.HOUR_OF_DAY, 11);
        calendar.set(Calendar.MINUTE, 55);
        Date date = new Date(calendar.getTimeInMillis());
        DateFormat formatter = new SimpleDateFormat("HH:mm:ss:aa");
        String dateFormatted = formatter.format(date);
        String s[]=dateFormatted.split(":");

        Log.d("shourr","hh"+s[0]+"mm"+s[1]+"aa"+s[3]+"hhh"+s1[1]+"aaa"+s1[2]);
     //   if(s[0].equals(s1[0])&&s[1].equals(s1[1])&&s[3].equals(s1[2])){

            if(s[0]=="11" && s[1]=="55"){

            Log.d("minnnshourr","hh"+s[0]+"mm"+s[1]+"aa"+s[3]+"hhh"+s1[0]+"aaa"+s1[2]);
            Intent intent = new Intent(this, broadcast.class);
            PendingIntent pendingIntent = PendingIntent.getBroadcast(this.getApplicationContext(), 234324243, intent, 0);
            AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
//        alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(),
//                AlarmManager.INTERVAL_DAY, pendingIntent);
            alarmManager.setInexactRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(),
                    24*60*60*1000, pendingIntent);

            Log.d("militime", String.valueOf(calendar.getTimeInMillis())+" cn cnm"+dateFormatted);
        }




    /* if(){

        }*/





// With setInexactRepeating(), you have to use one of the AlarmManager interval
// constants--in this case, AlarmManager.INTERVAL_DAY.

    }



   /* private void createNotification(String pDate, String pTime) {
        AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(ACTION);
        PendingIntent alarmIntent = PendingIntent.getBroadcast(this, 0, intent, 0);
        alarmManager.set(AlarmManager.RTC_WAKEUP, Calendar.getInstance().getTimeInMillis(), alarmIntent);
    }*/





    public String formatMonth(String month) {
        SimpleDateFormat monthParse = new SimpleDateFormat("MM");
        SimpleDateFormat monthDisplay = new SimpleDateFormat("MMM");
        String mm = null;
        try {
            mm = monthDisplay.format(monthParse.parse(month));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return mm;




    }


    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
//            super.onBackPressed();
            finish();
            return;
        }

        if (rank_layout.getVisibility() == View.VISIBLE) {
           rank_layout.setVisibility(View.GONE);
        }
        else {


        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 2000);
        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    class MenuAdapter extends BaseAdapter implements IConstant {
        Activity activity;
        String url;
        Context c1;
        CallbackManager callbackManager;
        ShareDialog shareDialog;
        ArrayList menulist = new ArrayList();


        public MenuAdapter(Activity activity, ArrayList menulist) {

            this.activity = activity;
            FacebookSdk.sdkInitialize(activity);
            callbackManager = CallbackManager.Factory.create();
            shareDialog = new ShareDialog(activity);

            this.menulist = menulist;

        }


        @Override
        public int getCount() {
            return menulist.size();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.menulist, null);
            ImageView menuimage = (ImageView) convertView.findViewById(R.id.menuimage);
            TextView item = (TextView) convertView.findViewById(R.id.item);
            item.setText(menulist.get(position).toString());

            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (position == 0) {
                        Intent intent = new Intent(activity, User_profile.class);
                        activity.startActivity(intent);
                        activity.finish();
                        Log.d("position0", "user profile");
                    } else if (position == 1) {
                        Intent intent = new Intent(activity, Friend_Ranking.class);
                        try {
                            intent.putStringArrayListExtra("rankingnumber", Incoming_outgoing_calls.arrylist_ranking);
                            intent.putStringArrayListExtra("rankingsize", Incoming_outgoing_calls.arrylist_ranking_size);
                            intent.putStringArrayListExtra("name_rank", cur_month_out.avg_scall);
                        } catch (Exception e) {

                        }
                        activity.startActivity(intent);
                        activity.finish();

                    } else if (position == 2) {
                        Intent intent = new Intent(activity, Activity_info.class);

                        activity.startActivity(intent);
                        activity.finish();

//
                    } else if (position == 3) {
                        Intent intent = new Intent(activity, About_us.class);
                        activity.startActivity(intent);
                        activity.finish();
                    } else if (position == 4) {
                        Intent intent = new Intent(activity, Multilanguage.class);
                        activity.startActivity(intent);
                        activity.finish();
                    }

                    else if (position == 5) {

                        try {
                            Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                                    "mailto", "ajeetboparai@gmail.com", null));
                            emailIntent.putExtra(Intent.EXTRA_SUBJECT, "BuddyGap Feedback");
                            activity.startActivity(Intent.createChooser(emailIntent, "Send email..."));
                        } catch (Exception e) {

                        }
                    } else if (position == 6) {


                        if (isNetworkAvailable() == false) {
                            alert_dialog();
                        } else {
                            AlertDialog.Builder builder = new AlertDialog.Builder(activity);

                            builder.setTitle("Confirm");
                            builder.setMessage("Are you sure you want to logout?");

                            builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {

                                public void onClick(DialogInterface dialog, int which) {
                                    // Do nothing but close the dialog

                                    logout();
                                }

                            });

                            builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {

                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                    dialog.dismiss();
                                }
                            });

                            AlertDialog alert = builder.create();
                            alert.show();

                        }


                    }


                }
            });
            return convertView;
        }




        void logout() {
            url = Base_url + "logout.php?user_id=" + pref_getvalue_str("user_id", "");
            progress_show();


            Log.d("url_logout", url);
            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, null, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {
                        String status = response.getString("status");
                        if (status.equals("true")) {
                            Log.d("logout", status);
                            LoginManager.getInstance().logOut();
                            //   signOutFromGplus();

                            //  GoogleAuthUtil.clearToken(activity);

                            sharedPreferences.edit().clear().commit();

                            Intent intent = new Intent(activity, Facebook_login.class);
                            intent.putExtra("logout", "yes");
                            activity.startActivity(intent);
                            activity.finish();

                        }
                        progress_dismiss();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    progress_dismiss();
                }
            });
            AppController.getInstance().addToRequestQueue(jsonObjectRequest);
        }

    }
    public  void GetMood(){
        //String url="http://api.openweathermap.org/data/2.5/weather?lat=-33.87&lon=151.21&APPID=ea574594b9d36ab688642d5fbeab847e";
        String url=Base_url+"get_mood.php?id="+pref_getvalue_str("user_id", "");
        //http://api.openweathermap.org/data/2.5/weather?lat=30.7105&lon=76.7033&APPID=ea574594b9d36ab688642d5fbeab847e";
        Log.e("urll", url);
        // progress_show();
        JsonObjectRequest jsonObjectRequest=new JsonObjectRequest(url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject jsonObject) {
                try {

                    String status = jsonObject.getString("status");
                    if (status.equals("true")) {

                        JSONObject response = jsonObject.getJSONObject("response");
                        String mood = response.getString("mood");
                        if (mood.equals("Excellent")) {
                            yourfeels.setImageResource(R.drawable.excellent);
                        } else if (mood.equals("Good")) {
                            yourfeels.setImageResource(R.drawable.good);
                        } else if (mood.equals("Average")) {
                            yourfeels.setImageResource(R.drawable.average);
                        } else if (mood.equals("Sad")) {
                            yourfeels.setImageResource(R.drawable.sad);
                        } else if (mood.equals("Frustrated")) {
                            yourfeels.setImageResource(R.drawable.frustrated);
                        } else if (mood.equals("Neutral")) {
                            yourfeels.setImageResource(R.drawable.min);
                        } else {
                            yourfeels.setImageResource(R.drawable.min);
                        }
                    }
                    else {
                        yourfeels.setImageResource(R.drawable.min);
                    }


                }
                catch (JSONException e) {
                    e.printStackTrace();
                }
                Log.e("msg",jsonObject.toString());
                //  progress_dismiss();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.e("msg","error");
                //    progress_dismiss();
            }
        });
        AppController.getInstance().addToRequestQueue(jsonObjectRequest);
    }

    public void  StartServiceforLocation(){
        startService(new Intent(Incoming_outgoing_calls.this, LocationService.class));
        //  startService(new Intent(Incoming_outgoing_calls.this, Wordservice.class));
//Toast.makeText(Incoming_outgoing_calls.this,"servicess",Toast.LENGTH_LONG).show();
    }

    private boolean weHavePermissionToReadContacts() {
        return ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED&&ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED;

    }



    private void requestReadContactsPermissionFirst() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_COARSE_LOCATION)&&ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_FINE_LOCATION)) {
            Toast.makeText(this, "We need permission so you can access your location.", Toast.LENGTH_LONG).show();
            requestForResultContactsPermission();
        } else {
            requestForResultContactsPermission();
        }
    }

    private void requestForResultContactsPermission() {

        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION}, 123);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 123
                && grantResults.length > 0
                && grantResults[0] == PackageManager.PERMISSION_GRANTED&& grantResults[1] == PackageManager.PERMISSION_GRANTED) {
//            Toast.makeText(this, "Permission Granted", Toast.LENGTH_SHORT).show();




            if (gpsTracker.canGetLocation()) {
                if (gpsTracker.canGetLocation) {
                    lat = gpsTracker.getLatitude();
                    lon = gpsTracker.getLongitude();

                    StartServiceforLocation();
                }

            }
            else {

                if(gps_sett==1){
                    gpsTracker.showSettingsAlert();}
                else{

                }
            }

            //  StartServiceforLocation();
        } else {
            permission_deny=0;
            Toast.makeText(this, "Permission Denied", Toast.LENGTH_SHORT).show();
        }
    }
}


class cur_month_incom {


    static HashMap map1;
    static HashMap map4;
    static float ff1;
    static ArrayList incoming_arraylist, dur_arraylist, name_list, missed_arraylist;


    public cur_month_incom(Incoming_outgoing_calls incoming_outgoing_calls) {

        map1 = new HashMap();

        incoming_outgoing_calls.getCallDetails();
        Last_month_incoming_call incoming_call = new Last_month_incoming_call();
        incoming_arraylist = new ArrayList();
        map4 = new HashMap();
        incoming_arraylist = Incoming_outgoing_calls.arraylist_incom_phn;
        dur_arraylist = new ArrayList();
        dur_arraylist = Incoming_outgoing_calls.arraylist_incom_dur;

        name_list = new ArrayList();

        missed_arraylist = Incoming_outgoing_calls.arraylist_missedcall;
        Iterator<String> dupIter = incoming_arraylist.iterator();


        String dupWord;
        int j = 0;
        Integer count;

        Map<String, Integer> map2 = new HashMap<>();
        Map<String, String> map3 = new HashMap<>();

        ArrayList<String> nonDupList = new ArrayList<String>();


        while (dupIter.hasNext()) {
            dupWord = dupIter.next();

            if (nonDupList.contains(dupWord)) {
                count = (Integer) map1.get(dupWord);
                map1.put(dupWord, (count == null) ? 1 : count + 1);
                int f = 0, h = 0, sum = 0;
                if (map2.get(dupWord) == null) {

                } else {
                    f = map2.get(dupWord);
                    h = Integer.parseInt(String.valueOf(dur_arraylist.get(j)));
                    sum = f + h;
                    map2.put(dupWord, sum);
                }

                dupIter.remove();
            } else {
                count = (Integer) map1.get(dupWord);
                map1.put(dupWord, (count == null) ? 1 : count + 1);
                map2.put(dupWord, Integer.valueOf(String.valueOf(dur_arraylist.get(j))));
                map3.put(dupWord, String.valueOf(Incoming_outgoing_calls.arrayList_incom_name.get(j)));
                               nonDupList.add(dupWord);
            }
            j++;
              }



        for (int o = 0; o < incoming_arraylist.size(); o++) {
                      name_list.add(map3.get(incoming_arraylist.get(o)));

        }

        int f3 = 0;
        int total_avg_incom = 0;
        for (int i = 0; i < incoming_arraylist.size(); i++) {

            int f1 = map2.get(incoming_arraylist.get(i));
            int f2 = (int) map1.get(incoming_arraylist.get(i));
            f3 = f1 * f2;
                        map4.put(incoming_arraylist.get(i), f3);

        }

        ArrayList<Integer> sorting = new ArrayList<>();
        for (int i = 0; i < incoming_arraylist.size(); i++) {
            sorting.add((Integer) map4.get(incoming_arraylist.get(i)));
        }


        Collections.sort(sorting);



        int a1 = (
                20 * sorting.size()) / 100;

        for (int k = 0; k < sorting.size(); k++) {

            if (k >= a1 && k <= (sorting.size() - a1 - 1)) {

                total_avg_incom = total_avg_incom + sorting.get(k);
            }
        }


        try {
            ff1 = total_avg_incom / incoming_arraylist.size();
        } catch (Exception e) {

        }

    }


}


