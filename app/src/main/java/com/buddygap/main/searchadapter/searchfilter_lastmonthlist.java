package com.buddygap.main.searchadapter;

import android.app.Activity;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.buddygap.main.R;
import com.buddygap.main.home.Incoming_outgoing_calls;
import com.buddygap.main.lastmonth_list.Last_month_outgoingcall;
import com.buddygap.main.lastmonth_list.Last_monthlist;
import com.buddygap.main.lastmonth_list.Lastmonth_menu_friendz;
import com.buddygap.main.utils.WorldPopulation;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;


public
class searchfilter_lastmonthlist extends BaseAdapter {

    // Declare Variables
    Activity mContext;
    LayoutInflater inflater;
    private List<WorldPopulation> worldpopulationlist = null;
    private ArrayList<WorldPopulation> arraylist;

    public searchfilter_lastmonthlist(Activity context,

                                      List<WorldPopulation> worldpopulationlist) {
        mContext = context;
        this.worldpopulationlist = worldpopulationlist;
        inflater = LayoutInflater.from(mContext);
        this.arraylist = new ArrayList<WorldPopulation>();
        this.arraylist.addAll(worldpopulationlist);
    }

    public class ViewHolder {
        TextView rank;
        TextView invite_view;
        TextView country;
        // TextView population;
        ImageView flag;
        LinearLayout rank_layout;
    }

    @Override
    public int getCount() {
        return worldpopulationlist.size();
    }

    @Override
    public WorldPopulation getItem(int position) {
        return worldpopulationlist.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public View getView(final int position, View view, ViewGroup parent) {
        final ViewHolder holder;
        int p;
        int color,textrank_color;
        if (Last_monthlist.friend.equals(Incoming_outgoing_calls.best_frdz)){
            p=position+1;
            color=R.color.best_rank;
            textrank_color= R.color.best_background_color;
        }
        else if(Last_monthlist.friend.equals(Incoming_outgoing_calls.igr_frdz)){
            p=position+1+ Last_month_outgoingcall.list4.size();
            color=R.color.igr_rank;
            textrank_color=R.color.ignore_background_color;
        }
        else if(Last_monthlist.friend.equals(Incoming_outgoing_calls.annoy_frdz)){
            p=position+Last_month_outgoingcall.list2.size()+1+Last_month_outgoingcall.list4.size();
            color=R.color.ann_rank;
            textrank_color=R.color.annoy_background_color;

        }
        else {

            color=R.color.lousy_rank;
            textrank_color=R.color.lousy_background_color;
        }
        if (view == null) {
            holder = new ViewHolder();
            view = inflater.inflate(R.layout.row_best
                    , null);
            // Locate the TextViews in listview_item.xml
            holder.rank = (TextView) view.findViewById(R.id.rank);
            holder.country = (TextView) view.findViewById(R.id.name);
            holder.invite_view = (TextView) view.findViewById(R.id.invite_view);
            holder.rank_layout = (LinearLayout) view.findViewById(R.id.rank_layout);
            //    holder.population = (TextView) view.findViewById(R.id.rank);
            // Locate the ImageView in listview_item.xml
            holder.flag = (ImageView) view.findViewById(R.id.menuimage);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }


        holder.rank_layout.setBackgroundColor(mContext.getResources().getColor(color));
        holder.rank.setTextColor(mContext.getResources().getColor(textrank_color));

        // Set the results into TextViews

        holder.rank.setText(worldpopulationlist.get(position).getRank());
        try {
            if (worldpopulationlist.get(position).getinstall().equals("1")) {
                holder.invite_view.setVisibility(View.INVISIBLE);
            } else {
                holder.invite_view.setVisibility(View.VISIBLE);
            }

        }
        catch (Exception e){

        }

        // Set the results into TextViews
        if(worldpopulationlist.get(position).getName().equals("No Name")){
         //   holder.rank.setText(""+worldpopulationlist.get(position).getRank());
            holder.country.setText(worldpopulationlist.get(position).getNumber());
            holder.flag.setImageResource(R.drawable.user);

        }
        else{
           // holder.rank.setText(""+worldpopulationlist.get(position).getRank());
            holder.country.setText(worldpopulationlist.get(position).getName());
            holder.flag.setImageDrawable(worldpopulationlist.get(position)
                    .getImg());
        }
//        holder.rank.setText(String.valueOf(p));
//        holder.country.setText(worldpopulationlist.get(position).getName());
//         holder.flag.setImageDrawable(worldpopulationlist.get(position)
//                .getImg());
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent=new Intent(mContext,Lastmonth_menu_friendz.class);
                intent.putExtra("name",worldpopulationlist.get(position).getName());
                intent.putExtra("number", worldpopulationlist.get(position).getNumber());
                intent.putExtra("friends",Last_monthlist.friend);
                mContext.startActivity(intent);

            }
        });
        return view;
    }

    // Filter Class
    public void filter(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        worldpopulationlist.clear();
        if (charText.length() == 0) {
            worldpopulationlist.addAll(arraylist);
        } else {
            for (WorldPopulation wp : arraylist) {
                if (wp.getName_no().toLowerCase(Locale.getDefault())
                        .contains(charText)) {
                    worldpopulationlist.add(wp);
                }
                else  if (wp.getNumber().toLowerCase(Locale.getDefault())
                        .contains(charText)) {
                    worldpopulationlist.add(wp);
                }
            }
        }
        notifyDataSetChanged();
    }

}


