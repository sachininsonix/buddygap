package com.buddygap.main.searchadapter;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.buddygap.main.R;
import com.buddygap.main.friend_menu.Best_friendz;
import com.buddygap.main.home.Incoming_outgoing_calls;
import com.buddygap.main.this_monthlist.Bestfrdz_list;
import com.buddygap.main.this_monthlist.Send_invite;
import com.buddygap.main.utils.Friend_Population;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class friandrankingsearchfilter extends BaseAdapter {

    // Declare Variables
    Activity mContext;
    LayoutInflater inflater;
    private List<Friend_Population> worldpopulationlist = null;
    private ArrayList<Friend_Population> arraylist;
    ImageLoader imageLoader;
    DisplayImageOptions displayImageOptions;
    String friend;

    public friandrankingsearchfilter(Activity context,
                                     List<Friend_Population> worldpopulationlist) {
        mContext = context;
        this.worldpopulationlist = worldpopulationlist;
        inflater = LayoutInflater.from(mContext);
        this.arraylist = new ArrayList<Friend_Population>();
        this.arraylist.addAll(worldpopulationlist);
    }

    public class ViewHolder {
        TextView rank,rank_view,phn_number;
        TextView country,view_list;
        TextView invite_view;
         ImageView flag,mood,profile_pic;
        LinearLayout rank_layout,view_frdranking;
    }

    @Override
    public int getCount() {
        return worldpopulationlist.size();
    }

    @Override
    public Friend_Population getItem(int position) {
        return worldpopulationlist.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public View getView(final int position, View view, ViewGroup parent) {
        ViewHolder holder;
        int color,textrank_color;

        if (view == null) {
            holder = new ViewHolder();
            view = inflater.inflate(R.layout.ranking_single
                    , null);
            // Locate the TextViews in listview_item.xml
            holder.rank = (TextView) view.findViewById(R.id.rank);
            holder.country = (TextView) view.findViewById(R.id.name);
            holder.rank_view = (TextView) view.findViewById(R.id.rank_view);
            holder.view_list = (TextView) view.findViewById(R.id.view_list);
            holder.phn_number = (TextView) view.findViewById(R.id.phn_number);
            holder.invite_view = (TextView) view.findViewById(R.id.invite_view);
            holder.rank_layout = (LinearLayout) view.findViewById(R.id.rank_layout);
            holder.view_frdranking = (LinearLayout) view.findViewById(R.id.view_frdranking);
            holder.flag = (ImageView) view.findViewById(R.id.menuimage);
            holder.profile_pic = (ImageView) view.findViewById(R.id.internet_profile);
            holder.mood = (ImageView) view.findViewById(R.id.emotions);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }

       /* if (position==0) {
            holder.view_frdranking.setVisibility(View.VISIBLE);
            holder.view_list.setText("Best Friends");
            friend= Incoming_outgoing_calls.best_frdz;
            holder.view_list.setBackgroundColor(Color.parseColor("#ff9f00"));
            holder. rank_view.setBackgroundColor(Color.parseColor("#d18200"));
        }
       else  if (position == cur_month_out.list8.size()) {
            holder.view_frdranking.setVisibility(View.VISIBLE);
            holder.view_list.setText("Ignored  Friends");
            friend=Incoming_outgoing_calls.igr_frdz;
            holder.view_list.setBackgroundColor(Color.parseColor("#01C853"));
            holder.rank_view.setBackgroundColor(Color.parseColor("#00A445"));
        }
        else
        if (position == (cur_month_out.list7.size()+cur_month_out.list8.size())){
            holder.view_frdranking.setVisibility(View.VISIBLE);
            holder.view_list.setText("Annoyed  Friends");
            friend=Incoming_outgoing_calls.annoy_frdz;
            holder.view_list.setBackgroundColor(Color.parseColor("#dd2c00"));
            holder.rank_view.setBackgroundColor(Color.parseColor("#af2300"));
        }
        else
        if (position == (cur_month_out.list6.size()+cur_month_out.list8.size()+ cur_month_out.list7.size())){
            holder.view_frdranking.setVisibility(View.VISIBLE);
            holder.view_list.setText("Lousy  Friends");
            friend=Incoming_outgoing_calls.lousy_frdz;
            holder. view_list.setBackgroundColor(Color.parseColor("#36474f"));
            holder.rank_view.setBackgroundColor(Color.parseColor("#232e33"));
        }*/
        if (worldpopulationlist.get(position).getaBoolean()==true){
            holder.view_frdranking.setVisibility(View.VISIBLE);
            holder.view_list.setText(worldpopulationlist.get(position).getFriend_list());
            friend= Incoming_outgoing_calls.best_frdz;
            holder.view_list.setBackgroundColor(Color.parseColor(worldpopulationlist.get(position).getViewlist()));
            holder. rank_view.setBackgroundColor(Color.parseColor(worldpopulationlist.get(position).getviewrank()));
        }
        else{
            holder.view_frdranking.setVisibility(View.GONE);
        }
      /*  if(cur_month_out.list8.size()>position){
            //  view_list.setVisibility(View.GONE);

           // color=R.color.best_rank;
            friend= Incoming_outgoing_calls.best_frdz;
//            textrank_color= R.color.best_background_color;
//            holder.rank_layout.setBackgroundColor(mContext.getResources().getColor(color));
//            holder.rank.setTextColor(mContext.getResources().getColor(textrank_color));
            if (position==0){
                holder.view_frdranking.setVisibility(View.VISIBLE);
                holder.view_list.setText("Best Friends");
                friend= Incoming_outgoing_calls.best_frdz;
                holder.view_list.setBackgroundColor(Color.parseColor("#ff9f00"));
                holder. rank_view.setBackgroundColor(Color.parseColor("#d18200"));
            }
            else{
                holder.view_frdranking.setVisibility(View.GONE);
                Log.d("visi","holder");
            }


        }

        else if((cur_month_out.list7.size()+cur_month_out.list8.size())>position)
        {


            holder.view_frdranking.setVisibility(View.GONE);
            //view_list.setVisibility(View.GONE);
           // color=R.color.igr_rank;
            friend= Incoming_outgoing_calls.igr_frdz;
//            textrank_color=R.color.ignore_background_color;
//            holder.rank_layout.setBackgroundColor(mContext.getResources().getColor(color));
//            holder.rank.setTextColor(mContext.getResources().getColor(textrank_color));
            if (position==cur_month_out.list8.size()){
                holder.view_frdranking.setVisibility(View.VISIBLE);
                holder.view_list.setText("Ignored  Friends");
                friend=Incoming_outgoing_calls.igr_frdz;
                holder.view_list.setBackgroundColor(Color.parseColor("#01C853"));
                holder.rank_view.setBackgroundColor(Color.parseColor("#00A445"));
            }
        }
        else if((cur_month_out.list7.size()+cur_month_out.list8.size()+cur_month_out.list6.size())>position)
        {
            holder.view_frdranking.setVisibility(View.GONE);
            // view_list.setVisibility(View.GONE);
         //   color=R.color.ann_rank;
            friend= Incoming_outgoing_calls.annoy_frdz;
//            textrank_color=R.color.annoy_background_color;
//            holder.rank_layout.setBackgroundColor(mContext.getResources().getColor(color));
//            holder.rank.setTextColor(mContext.getResources().getColor(textrank_color));
            if (position == (cur_month_out.list7.size()+cur_month_out.list8.size())){
                holder.view_frdranking.setVisibility(View.VISIBLE);
                holder.view_list.setText("Annoyed  Friends");
                friend=Incoming_outgoing_calls.annoy_frdz;
                holder.view_list.setBackgroundColor(Color.parseColor("#dd2c00"));
                holder.rank_view.setBackgroundColor(Color.parseColor("#af2300"));
            }
        }
        else
        //if((cur_month_out.list7.size()+cur_month_out.list8.size()+cur_month_out.list6.size()+cur_month_out.list9.size())>position)
        {
            holder.view_frdranking.setVisibility(View.GONE);

            //view_list.setVisibility(View.GONE);
           // color=R.color.lousy_rank;
            friend= Incoming_outgoing_calls.lousy_frdz;
//            textrank_color=R.color.lousy_background_color;
//            holder.rank_layout.setBackgroundColor(mContext.getResources().getColor(color));
//            holder.rank.setTextColor(mContext.getResources().getColor(textrank_color));
            if (position == (cur_month_out.list6.size()+cur_month_out.list8.size()+ cur_month_out.list7.size())){
                holder.view_frdranking.setVisibility(View.VISIBLE);
                holder.view_list.setText("Lousy  Friends");
                friend=Incoming_outgoing_calls.lousy_frdz;
                holder. view_list.setBackgroundColor(Color.parseColor("#36474f"));
                holder.rank_view.setBackgroundColor(Color.parseColor("#232e33"));}

        }

*/









try{


    if(worldpopulationlist.get(position).getMood().equals("Excellent")){
        holder.mood.setImageResource(R.drawable.excellent);
    }
    else  if(worldpopulationlist.get(position).getMood().equals("Good")){
        holder.mood.setImageResource(R.drawable.good);
    } else if(worldpopulationlist.get(position).getMood().equals("Average")){
        holder.mood.setImageResource(R.drawable.average);
    } else if(worldpopulationlist.get(position).getMood().equals("Sad")){
        holder.mood.setImageResource(R.drawable.sad);
    } else if(worldpopulationlist.get(position).getMood().equals("Frustrated")){
        holder.mood.setImageResource(R.drawable.frustrated);
    }
    else {
        holder.mood.setImageResource(R.drawable.min);
    }
        if (worldpopulationlist.get(position).getinstall().equals("1")){
            holder.invite_view.setVisibility(View.INVISIBLE);
        }
        else{
            holder.invite_view.setVisibility(View.VISIBLE);
        }
    holder.invite_view.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent = new Intent(mContext, Send_invite.class);
            intent.putExtra("phone_num", worldpopulationlist.get(position).getNumber());
            intent.putExtra("send_nam", worldpopulationlist.get(position).getName());
            intent.putExtra("name",  ""+Bestfrdz_list.list_name);
            intent.putExtra("number", Bestfrdz_list.array_number);

            intent.putExtra("frd_ranking", "4");
                       intent.putExtra("friends", Bestfrdz_list.friend);

            mContext.startActivity(intent);


        }
    });}
catch (Exception e) {
}


        holder.rank.setText(worldpopulationlist.get(position).getRank());
        holder.country.setText(worldpopulationlist.get(position).getName());

        holder.phn_number.setText(worldpopulationlist.get(position).getNumber());
        holder.rank_layout.setBackgroundColor(mContext.getResources().getColor(worldpopulationlist.get(position).getText1()));
          holder.rank.setTextColor(mContext.getResources().getColor(worldpopulationlist.get(position).getText_background()));
        if(worldpopulationlist.get(position).getName().equals("No Name")) {
            //holder.rank.setText(""+worldpopulationlist.get(position).getRank());


            holder.flag.setImageResource(R.drawable.user);



            Log.d("hiiii",worldpopulationlist.get(position).getName());
        }
        else{
            holder.flag.setImageDrawable(worldpopulationlist.get(position)
                    .getImg());

        }


        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent=new Intent(mContext,Best_friendz.class);
                intent.putExtra("name",worldpopulationlist.get(position).getName());
                intent.putExtra("number", worldpopulationlist.get(position).getNumber());
                intent.putExtra("friends",friend);
                intent.putExtra("invite", worldpopulationlist.get(position).getinstall());
                mContext.startActivity(intent);

            }
        });
        return view;
    }

    // Filter Class
    public void filter(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        worldpopulationlist.clear();
        if (charText.length() == 0) {
            worldpopulationlist.addAll(arraylist);
        } else {
            for (Friend_Population wp : arraylist) {
                if (wp.getName_no().toLowerCase(Locale.getDefault())
                        .contains(charText)) {
                    worldpopulationlist.add(wp);
                }
                else if (wp.getNumber().toLowerCase(Locale.getDefault()).contains(charText)){
                    worldpopulationlist.add(wp);

                }
            }
        }
        notifyDataSetChanged();
    }
    public  boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
}

