package com.buddygap.main.searchadapter;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.buddygap.main.R;
import com.buddygap.main.friend_menu.Best_friendz;
import com.buddygap.main.home.Incoming_outgoing_calls;
import com.buddygap.main.this_monthlist.Bestfrdz_list;
import com.buddygap.main.this_monthlist.Send_invite;
import com.buddygap.main.utils.WorldPopulation;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class productsearchfilter extends BaseAdapter {

    // Declare Variables
    Activity mContext;
    LayoutInflater inflater;
    private List<WorldPopulation> worldpopulationlist = null;
    private ArrayList<WorldPopulation> arraylist;
    ImageLoader imageLoader;
    DisplayImageOptions displayImageOptions;

    public productsearchfilter(Activity context,
                               List<WorldPopulation> worldpopulationlist) {
        mContext = context;
        this.worldpopulationlist = worldpopulationlist;
        inflater = LayoutInflater.from(mContext);
        this.arraylist = new ArrayList<WorldPopulation>();
        this.arraylist.addAll(worldpopulationlist);
    }

    public class ViewHolder {
        TextView rank;
        TextView country;
        TextView invite_view;
         ImageView flag,mood,profile_pic;
        LinearLayout rank_layout;
    }

    @Override
    public int getCount() {
        return worldpopulationlist.size();
    }

    @Override
    public WorldPopulation getItem(int position) {
        return worldpopulationlist.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public View getView(final int position, View view, ViewGroup parent) {
        final ViewHolder holder;
        int color,textrank_color;

        if (Bestfrdz_list.friend.equals(Incoming_outgoing_calls.best_frdz)){

            color=R.color.best_rank;
            textrank_color= R.color.best_background_color;

        }
        else if(Bestfrdz_list.friend.equals(Incoming_outgoing_calls.igr_frdz)){

            color=R.color.igr_rank;
            textrank_color=R.color.ignore_background_color;

        }
        else if(Bestfrdz_list.friend.equals(Incoming_outgoing_calls.annoy_frdz)){

            color=R.color.ann_rank;
            textrank_color=R.color.annoy_background_color;
        }
        else {

            color=R.color.lousy_rank;
            textrank_color=R.color.lousy_background_color;
        }


        if (view == null) {
            holder = new ViewHolder();
            view = inflater.inflate(R.layout.row_best
                    , null);
            // Locate the TextViews in listview_item.xml
            holder.rank = (TextView) view.findViewById(R.id.rank);
            holder.country = (TextView) view.findViewById(R.id.name);
            holder.invite_view = (TextView) view.findViewById(R.id.invite_view);
            holder.rank_layout = (LinearLayout) view.findViewById(R.id.rank_layout);
        //    holder.population = (TextView) view.findViewById(R.id.rank);
            // Locate the ImageView in listview_item.xml
            holder.flag = (ImageView) view.findViewById(R.id.menuimage);
            holder.profile_pic = (ImageView) view.findViewById(R.id.internet_profile);
            holder.mood = (ImageView) view.findViewById(R.id.emotions);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }
        holder.rank_layout.setBackgroundColor(mContext.getResources().getColor(color));
        holder.rank.setTextColor(mContext.getResources().getColor(textrank_color));


try{


    if(worldpopulationlist.get(position).getMood().equals("Excellent")){
        holder.mood.setImageResource(R.drawable.excellent);
    }
    else  if(worldpopulationlist.get(position).getMood().equals("Good")){
        holder.mood.setImageResource(R.drawable.good);
    }
    else  if(worldpopulationlist.get(position).getMood().equals("Average")){
        holder.mood.setImageResource(R.drawable.average);
    }
    else  if(worldpopulationlist.get(position).getMood().equals("Sad")){
        holder.mood.setImageResource(R.drawable.sad);
    }
    else  if(worldpopulationlist.get(position).getMood().equals("Frustrated")){
        holder.mood.setImageResource(R.drawable.frustrated);
    }
    else {
        holder.mood.setImageResource(R.drawable.min);
    }

        if (worldpopulationlist.get(position).getinstall().equals("1")){
            holder.invite_view.setVisibility(View.INVISIBLE);
        }
        else{
            holder.invite_view.setVisibility(View.VISIBLE);
        }
    holder.invite_view.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent = new Intent(mContext, Send_invite.class);
            intent.putExtra("phone_num", worldpopulationlist.get(position).getNumber());
            intent.putExtra("send_nam", worldpopulationlist.get(position).getName());
            intent.putExtra("name",  ""+Bestfrdz_list.list_name);
            intent.putExtra("number", Bestfrdz_list.array_number);

            intent.putExtra("frd_ranking", "4");
                       intent.putExtra("friends", Bestfrdz_list.friend);

            mContext.startActivity(intent);


        }
    });}
catch (Exception e) {
}
        holder.rank.setText(worldpopulationlist.get(position).getRank());
        if(worldpopulationlist.get(position).getName().equals("No Name")) {
            //holder.rank.setText(""+worldpopulationlist.get(position).getRank());

            holder.country.setText(worldpopulationlist.get(position).getNumber());

            holder.flag.setImageResource(R.drawable.user);

      /*      if (worldpopulationlist.get(position).getinstall().equals("0")) {
                holder.flag.setImageResource(R.drawable.user);

            } else {
                if (isNetworkAvailable()==false) {
                    holder.profile_pic.setVisibility(View.GONE);
                    holder.flag.setImageResource(R.drawable.user);
                }else{
                    holder.profile_pic.setVisibility(View.VISIBLE);

                imageLoader = ImageLoader.getInstance();
                imageLoader.init(ImageLoaderConfiguration.createDefault(mContext));
                displayImageOptions = new DisplayImageOptions.Builder().showStubImage(R.drawable.user).showImageForEmptyUri(R.drawable.user).cacheOnDisc().cacheInMemory().build();

            imageLoader.displayImage(worldpopulationlist.get(position).geturl(), holder.profile_pic, displayImageOptions);
        }
            }*/

            //holder.flag.setImageResource(R.drawable.user);

            Log.d("hiiii",worldpopulationlist.get(position).getName());
        }
        else{
           // holder.rank.setText(""+worldpopulationlist.get(position).getRank());
            holder.country.setText(worldpopulationlist.get(position).getName());
            holder.flag.setImageDrawable(worldpopulationlist.get(position)
                    .getImg());
          /*  if (worldpopulationlist.get(position).getinstall().equals("0")) {
                holder.flag.setImageDrawable(worldpopulationlist.get(position)
                        .getImg());
            } else {
                if (isNetworkAvailable()==false){
                    holder.profile_pic.setVisibility(View.GONE);
                    holder.flag.setImageDrawable(worldpopulationlist.get(position)
                            .getImg());}
                else{
                imageLoader = ImageLoader.getInstance();
                imageLoader.init(ImageLoaderConfiguration.createDefault(mContext));
                displayImageOptions = new DisplayImageOptions.Builder().showStubImage(R.drawable.user).showImageForEmptyUri(R.drawable.user).cacheOnDisc().cacheInMemory().build();


                                   holder.profile_pic.setVisibility(View.VISIBLE);

                    imageLoader.displayImage(worldpopulationlist.get(position).geturl(), holder.profile_pic, displayImageOptions);
                }
             //   imageLoader.displayImage(Bestfrdz_list.profile_image.get(position),  holder.profile_pic, displayImageOptions);

            }*/
          /*  holder.flag.setImageDrawable(worldpopulationlist.get(position)
                    .getImg());*/
        }
//        holder.rank.setText(String.valueOf(p));
//        holder.country.setText(worldpopulationlist.get(position).getName());
//         holder.flag.setImageDrawable(worldpopulationlist.get(position)
//                .getImg());

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent=new Intent(mContext,Best_friendz.class);
                intent.putExtra("name",worldpopulationlist.get(position).getName());
                intent.putExtra("number", worldpopulationlist.get(position).getNumber());
                intent.putExtra("friends",Bestfrdz_list.friend);
                intent.putExtra("invite", worldpopulationlist.get(position).getinstall());
                mContext.startActivity(intent);

            }
        });
        return view;
    }

    // Filter Class
    public void filter(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        worldpopulationlist.clear();
        if (charText.length() == 0) {
            worldpopulationlist.addAll(arraylist);
        } else {
            for (WorldPopulation wp : arraylist) {
                if (wp.getName_no().toLowerCase(Locale.getDefault())
                        .contains(charText)) {
                    worldpopulationlist.add(wp);
                }
                else if (wp.getNumber().toLowerCase(Locale.getDefault()).contains(charText)){
                    worldpopulationlist.add(wp);
                }
            }
        }
        notifyDataSetChanged();
    }
    public  boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
}

